const functions = require("firebase-functions");
// const { database } = require("firebase-functions/v1/firestore");
const admin = require('firebase-admin');
const nodemailer = require('nodemailer');
admin.initializeApp();
const database = admin.firestore();

var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: '10.to.1.magic@gmail.com',
        pass: 'rgwkiryeadewkevd',
    }
});

exports.sendEmail = functions.firestore
.document('emails/{docName}')
.onWrite( async (change, context) => {

    const query = await database.collection("emails")
    .where("emailSent", "==", false).get();

    query.forEach(async snapshot => {
        sendEmail(snapshot.data().address, snapshot.data().subject, snapshot.data().html)
        await database.doc('emails/' + snapshot.data().address).update({
            "emailSent" : true,
        })
    });

    function sendEmail(address, subject, html) {
    
        const mailOptions = {
            from: '10.to.1.magic@gmail.com',
            to: address,
            subject: subject,
            html: html,
        };
    
        return transporter.sendMail(mailOptions, (error, data)  => {
            if (error) {
                console.log(error)
                return
            }
            console.log("Sent!")
        });
    }    
});

// Checks Firestore every time there is a write to /notifications/
exports.sendNotification = functions.firestore
    .document('notifications/{docName}')
    .onWrite( async (change, context) => {

    const query = await database.collection("notifications")
    .where("notificationSent", "==", false).get();

    query.forEach(async snapshot => {
        sendNotification(snapshot.data().token, snapshot.data().title, snapshot.data().body, snapshot.data().threadID, snapshot.data().sound, snapshot.data().badge)
        await database.doc('notifications/' + snapshot.data().token).update({
            "notificationSent" : true,
        })
    });

    function sendNotification(notificationToken, title, body, threadID, sound, badgeCount) {
        const message = {
            notification : {
                title: title, 
                body: body,
            },
            apns: {
                payload: {
                    aps: {
                        badge: badgeCount,
                        sound: sound,
                        "thread-id": threadID,
                        "interruption-level": "time-sensitive",
                    }
                }
            },
            token: notificationToken,
        };

        admin.messaging().send(message).then(response => {
            return console.log("Successful Message Sent");

        }).catch(error => {
            return console.log("Error Sending Message:");
        });
    }

    return console.log('End of Function');
});