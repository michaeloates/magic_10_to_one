# magic_10_to_one

An asynchronous multiplayer supported card game based on the the traditional Wood's game "10 to 1". Built in flutter
using firebase noSQL solution as backend database.

Includes:
- Supports 4-10 players with dynamic deck that accommodates for varying players
- Extensive game features
    - Server passwords and options such as "hexing" and "blind-man game"
- Permanent cloud memory storage of the game
    - Players can quit/leave and rejoin at anytime to resume game
- asynchronous listening and loading to enhance usability. Race conditioning mitigated by usage of thread locks.
- AES encryption with SHA-256 salted hashes for passwords

## Features

### Home screen
<p float="left">
  <img src="/assets/images/home.png"  width="400" height="845">
</p>

### Profile
<p float="left">
  <img src="/assets/images/profile.png"  width="400" height="845">
</p>

### Create and view servers
<p float="left">
  <img src="/assets/images/create.png"  width="400" height="845">
  <img src="/assets/images/servers.png"  width="400" height="845">
</p>

### Select order and then play
<p float="left">
  <img src="/assets/images/order.png"  width="400" height="845">
  <img src="/assets/images/game.png"  width="400" height="845">
</p>

### In-game features and score functionality
<p float="left">
  <img src="/assets/images/pile.png"  width="400" height="845">
  <img src="/assets/images/scores.png"  width="400" height="845">
</p>
