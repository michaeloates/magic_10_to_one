import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:magic_10_to_one/Models/CardWrapper.dart';
import 'package:magic_10_to_one/Models/Game.dart';
import 'package:magic_10_to_one/enums/cards.dart';
import 'package:magic_10_to_one/game/FinalScreen.dart';
import 'package:eraser/eraser.dart';

import '../Models/GameUtilities.dart';
import '../Models/Player.dart';


class PlayScreen extends StatefulWidget {
  final String serverName;
  const PlayScreen({Key? key, required this.serverName}) : super(key: key);

  @override
  _PlayScreenState createState() => _PlayScreenState();
}

class _PlayScreenState extends State<PlayScreen> {
  bool host = false;
  Game game = Game();
  Stream<DocumentSnapshot> _serverStream = const Stream<DocumentSnapshot>.empty();
  Box profileBox = Hive.box(ProfileKeys.profile);
  String selected = "";
  TextEditingController _textFieldController = TextEditingController();
  String callValue = "";
  String tempCallValue = "";
  int tempFutureCall = -1;
  bool versionCheck = false;
  late double height;
  late double width;
  int notificationLock = -1;
  String personVoted = "";
  int minLocalVersion = 9999;
  int minServerVersion = 9999;
  DateTime sinceLastVote = DateTime.utc(1989, 11, 9);
  late Timer pressTimer;
  Timer? resetTimer;
  double callAlertThickness = 2.0;
  double cardAlertThickness = 2.0;
  double hexAlertThickness = 2.0;
  Color callAlertColor = Colors.white;
  Color cardAlertColor = Colors.white;
  Color hexAlertColor = Colors.white;

  Future<bool> makeFutureCall(BuildContext context) async {
    int futureCall = game.getFutureCall();
    String existingCallText = "";

    if (futureCall == -1) {
      existingCallText = "There is no queued call";
    } else {
      existingCallText = "Existing queued call is $futureCall";
    }

    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Queue a call'),
            content: SizedBox(
              height: 80,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextField(
                    keyboardType: TextInputType.number,
                    autocorrect: false,
                    onChanged: (value) {
                      setState(() {
                        tempFutureCall = int.parse(value);
                      });
                    },
                    controller: _textFieldController,
                    decoration: const InputDecoration(hintText: "Enter Call"),
                  ),
                  Container(height: 5,),
                  Text(
                    existingCallText,
                  )
                ],
              ),
            ),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                        foregroundColor: const Color(0xFFA10702),
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(
                            color: Color(0xFFA10702),
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        )
                    ),
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                    child: const Text('No'),
                  ),

                  TextButton(
                    style: TextButton.styleFrom(
                        foregroundColor: const Color(0xFF550527),
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(
                            color: Color(0xFF550527),
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        )
                    ),
                    onPressed: () {
                      game.clearFutureCall();
                      Navigator.pop(context, false);
                      updateCallAlert();
                      updateCardAlert();
                    },
                    child: const Text('Clear'),
                  ),

                  TextButton(
                    style: TextButton.styleFrom(
                      foregroundColor: const Color(0xFF256D1B),
                      shape: RoundedRectangleBorder(
                        side: const BorderSide(
                          color: Color(0xFF256D1B),
                          width: 2.0,
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                      )
                    ),
                    onPressed: () {
                      game.addCallToFuture(tempFutureCall);
                      Navigator.pop(context, false);
                      updateCallAlert();
                      updateCardAlert();
                    },
                    child: const Text('Yes'),
                  ),
                ],
              ),
            ],
          );
        }) ?? false;
  }

  Future<bool> makeFutureCard(BuildContext context, CardWrapper card) async {
    String existingFutureCardString = game.getFutureCardString();
    String existingFutureCardFullName = CardWrapper(cardStr: game.getFutureCardString()).getFullName();
    String proposedFutureCardFullName = card.getFullName();
    String existingCardText = "";

    if (existingFutureCardString == "n00") {
      existingCardText = "There is currently no queued card";
    } else {
      existingCardText = "Existing queued card is the $existingFutureCardFullName";
    }

    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Queue a card'),
            content: SizedBox(
              height: 90,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Are you sure you want to queue the $proposedFutureCardFullName?",
                    textAlign: TextAlign.center,
                  ),
                  Container(height: 10,),
                  Text(
                    existingCardText,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                        foregroundColor: const Color(0xFFA10702),
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(
                            color: Color(0xFFA10702),
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        )
                    ),
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                    child: const Text('No'),
                  ),

                  TextButton(
                    style: TextButton.styleFrom(
                        foregroundColor: const Color(0xFF550527),
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(
                            color: Color(0xFF550527),
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        )
                    ),
                    onPressed: () {
                      game.clearFutureCard();
                      Navigator.pop(context, false);
                    },
                    child: const Text('Clear'),
                  ),

                  TextButton(
                    style: TextButton.styleFrom(
                        foregroundColor: const Color(0xFF256D1B),
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(
                            color: Color(0xFF256D1B),
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        )
                    ),
                    onPressed: () {
                      game.addCardToFuture(card);
                      selected = "";
                      Navigator.pop(context, false);
                    },
                    child: const Text('Yes'),
                  ),
                ],
              ),

            ],
          );
        }) ?? false;
  }

  void updateCallAlert() {
    if (game.isCallQueued()) {
      callAlertColor = Colors.orange;
      callAlertThickness = 4.0;
    } else {
      callAlertColor = Colors.white;
      callAlertThickness = 2.0;
    }
  }

  void updateCardAlert() {
    if (game.isCardQueued()) {
      cardAlertColor = Colors.orange;
      cardAlertThickness = 4.0;
    } else {
      cardAlertColor = Colors.white;
      cardAlertThickness = 2.0;
    }
  }

  void updateHexAlert() {
    if (game.isHexDifferent()) {
      hexAlertColor = Colors.orange;
      hexAlertThickness = 4.0;
    } else {
      hexAlertColor = Colors.white;
      hexAlertThickness = 2.0;
    }
  }


  Text cardLedText = Text(
    "-",
    style: const TextStyle(color: Colors.black, fontSize: 25),
    textAlign: TextAlign.center,
  );


  loadGame(AsyncSnapshot<DocumentSnapshot> snapshot) async {
    await game.load(snapshot);
    updateCardLedString();

    return 1;
  }

  void updateCardLedString() {
    String cardLedString = game.currentPlaceSuit();

    if (cardLedString == "♠️" || cardLedString == "♣️" || cardLedString == "-") {
      cardLedText = Text(
        cardLedString,
        style: const TextStyle(color: Colors.black, fontSize: 25),
        textAlign: TextAlign.center,
      );
    } else {
      cardLedText = Text(
        cardLedString,
        style: const TextStyle(color: Color(0xFFcf2627), fontSize: 25),
        textAlign: TextAlign.center,
      );
    }
  }

  Widget createScore(Map<String, Map<String, int>> scoreboard) {
    // sort by player order
    int highestScore = game.getHighestScore();

    Map<String, dynamic> playersOrder = game.getPlayersOrder();
    List<Player> players = game.getPlayers();

    List<String> sortedNicknames = playersOrder.keys.toList(growable:false)
      ..sort((k1, k2) => playersOrder[k1].compareTo(playersOrder[k2]));

    List<TableRow> rows = [];
    for (String nickname in sortedNicknames) {
      int callsWon = scoreboard[nickname]!["callWon"] ?? 0;
      int call = scoreboard[nickname]!["call"] ?? 0;
      int score = scoreboard[nickname]!["score"] ?? 0;
      bool hexed = false;
      bool calling = false;
      bool twoHundredClub = false;
      bool todayIsTheirBirthday = checkBirthday(nickname) as bool;
      String displayNickname = "";
      FontWeight nicknameWeight = FontWeight.w400;
      FontWeight scoreWeight = FontWeight.w400;
      Color nicknameColor = Colors.white;
      Color scorePColor = Colors.white;
      Color scoreColor = const Color(0xFF097800);
      Color currentTurnColor = const Color(0xFFF44708);

      String callString = "";
      if (call == -1) {
        callString = "-";
      } else {
        callString = call.toString();
      }

      for (Player player in players) {
        if (nickname == player.nickname) {
          hexed = player.getHexStatus();
          twoHundredClub = player.getTwoHundredStatus();
        }
        if (nickname == game.getWhoStarted()) {
          calling = true;
        }
      }

      if (todayIsTheirBirthday) {
        displayNickname += "🎉 ";
      }

      if (twoHundredClub) {
        displayNickname += "👑 ";
      }

      if (hexed && !todayIsTheirBirthday) {

        if(!twoHundredClub) {
          displayNickname += "💀 ";
        }

        nicknameWeight = FontWeight.w800;
        nicknameColor = const Color(0xFF550527);
      }

      displayNickname += nickname;

      if (hexed) {
        displayNickname += " 💀";
      }

      if (score == highestScore) {
        scoreColor = const Color(0xFFFAA613);
        scoreWeight = FontWeight.w700;
        nicknameWeight = FontWeight.w700;
        scorePColor = const Color(0xFF5A1C00);

        if (!hexed) {
          nicknameColor = const Color(0xFF5A1C00);
          displayNickname = "-" + displayNickname + "-";
        }

      } else if (nickname == game.getCurrentPlayerTurn()) {
        scoreColor = const Color(0xFFF44708);
      }

      rows.add(TableRow(
          children: [
            Center(child: Text(displayNickname, style: TextStyle(
                fontSize: 10,
                color: nicknameColor,
                fontWeight: nicknameWeight,
                backgroundColor: scoreColor,
            ),)),
            Center(child: Text(calling ? "$callsWon / $callString 📞" : "$callsWon / $callString", style: const TextStyle(fontSize: 10, color: Colors.white),)),
            Center(child: Text("$score", style: TextStyle(fontSize: 10, color: scorePColor, fontWeight: scoreWeight, backgroundColor: scoreColor)))
          ],
          decoration: BoxDecoration(
            color: nickname == game.getCurrentPlayerTurn() ? currentTurnColor : const Color(0xFF097800),
          )
      ));
    }
    return Container(
        margin: const EdgeInsets.fromLTRB(15, 5, 5, 5),
        child: Table(
            children: rows,
          border: TableBorder.all(
            width: 0.8,
            color: Colors.white,
            borderRadius: const BorderRadius.all(Radius.circular(2.0)),
          ),
          columnWidths: const {
              0: FlexColumnWidth(4),
              1: FlexColumnWidth(2),
              3: FlexColumnWidth(2),
          },
        )
    );
  }

  List<Widget> getCards(double boxHeight, double boxWidth) {
    List<Widget> cards = [];
    List<CardWrapper> pile = game.getPile();
    double height = 120;
    double width = 80;
    double adjustment = (boxWidth - (width + (pile.length * width / 4))) / 2;

    for(int i = 0; i < pile.length; i++) {
      if (game.getCardLed().cardStr == pile[i].cardStr) {
        cards.add(Positioned(
          left: adjustment + (i * width / 4),
          child: Stack(
            children: [
              pile[i].getSpecImage(height, width),
              Container(
                width: 80,
                height: 120,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                    border: Border.all(
                        width: 2,
                        color: const Color(0xFFFA7921)
                    )
                ),
              ),

            ],
          ),
        ));
      } else {
        cards.add(Positioned(
          left: adjustment + (i * width / 4),
          child: pile[i].getSpecImage(height, width),
        ));
      }
    }
    return cards;
  }

  showCustomAlert(BuildContext context, String title, String description, String button) {
    // set up the button
    Widget okButton = TextButton(
      child: Text(button),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(description),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void pilePopup(BuildContext context) {
    Map<String, dynamic> cardToPlayer = {};
    cardToPlayer = game.getCardToPlayerPile();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    var alertDialog = AlertDialog(
      backgroundColor: Colors.transparent /* Colors.white */,
      insetPadding: const EdgeInsets.all(10),
      content: SizedBox(
        height: (height / 3),
        width: (width),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 0.0),
          child: game.getPile().isNotEmpty ? GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1,
              childAspectRatio: 1.72,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
            ),
            scrollDirection: Axis.horizontal,
            itemCount: game.getPile().length,
            itemBuilder: (BuildContext context, int index) {
              CardWrapper indivCard = game.getPile()[index];
              return Column(
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      cardToPlayer.containsKey(indivCard.cardStr) ? cardToPlayer[indivCard.cardStr] : "Unknown",
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                  Expanded(
                    flex: 9,
                    child: indivCard.getImage()
                  )
                ],
              );
            },
          ) : Container(),
        ),
      )
    );

    showDialog(
        context: context,
        builder: (BuildContext context) => alertDialog);
  }

  void previousPilePopup(BuildContext context) {
    Map<String, dynamic> cardToPlayer = {};
    cardToPlayer = game.getPreviousCardToPlayerPile();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    var alertDialog = AlertDialog(
        backgroundColor: Colors.transparent /* Colors.white */,
        insetPadding: const EdgeInsets.all(10),
        content: SizedBox(
          height: (height / 3),
          width: (width),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 0.0),
            child: game.getPreviousPile().isNotEmpty ? GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1,
                childAspectRatio: 1.72,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
              ),
              scrollDirection: Axis.horizontal,
              itemCount: game.getPreviousPile().length,
              itemBuilder: (BuildContext context, int index) {
                CardWrapper indivCard = game.getPreviousPile()[index];
                return Column(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        cardToPlayer.containsKey(indivCard.cardStr) ? cardToPlayer[indivCard.cardStr] : "Unknown",
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    Expanded(
                        flex: 9,
                        child: indivCard.getImage()
                    )
                  ],
                );
              },
            ) : Container(),
          ),
        )
    );

    showDialog(
        context: context,
        builder: (BuildContext context) => alertDialog);
  }

  void notificationMike() {
    if (game.getCurrentPlayer() == "Magician") {
      game.sendCurrentNotification();
      Fluttertoast.showToast(
        msg: "Notification Sent",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        fontSize: 16.0,
      );
    } else {
      Fluttertoast.showToast(
        msg: "Nice try bud",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        fontSize: 16.0,
      );
    }

  }

  void fullScoreboardPopup(BuildContext context) {
    // double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    var alertDialog = AlertDialog(
        backgroundColor: Colors.white /* Colors.white */,
        title: const Text("Scoreboard"),
        insetPadding: const EdgeInsets.all(5),
        content: Container(
          width: width,
          child: createFinalScoreSummary(),
        ),
    );

    showDialog(
        context: context,
        builder: (BuildContext context) => alertDialog);
  }
  

  Widget createFinalScoreSummary() {
    Widget scoreboard = Container(
        // margin: const EdgeInsets.all(3.0),
        // decoration: const BoxDecoration(
            color: Colors.white,
            //borderRadius: BorderRadius.all(Radius.circular(15.0))
        // ),
        child: LayoutBuilder(
          builder: (context, constraints) => Table(
            children: getDetailedRows(constraints),
            columnWidths: const {
              0: FlexColumnWidth(1),
              1: FlexColumnWidth(1),
              2: FlexColumnWidth(1),
            },
          ),
        )
    );

    return SingleChildScrollView(child: scoreboard);
  }

  List<TableRow> getDetailedRows(BoxConstraints constraints) {
    List<TableRow> rows = [];
    List<List<String>> scoreboard = game.getPopupScoreboard();
    int numRounds = scoreboard.length;
    int numPlayers = scoreboard[0].length;

    for(var i = 0; i < numRounds; i++) {
      List<Widget> tempRow = [];
      for(var j = 0; j < numPlayers; j++) {
        tempRow.add(
            Center(
              child: Text(
                scoreboard[i][j],
                style: const TextStyle(
                  fontSize: 15.0,
                  color: Colors.black,
                ),
              ),
            )
        );
      }
      rows.add(TableRow(
          children: tempRow
      ));
    }
    return rows;
  }

  Future sendHexNotification(String playerName) async {
    Map<String, dynamic> hexVotes = {};
    String personHexed = "";
    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    DateTime rightNow = DateTime.now();
    int secondsPassed = rightNow.difference(sinceLastVote).inSeconds;

    await ref.get().then((snapshot) {
      hexVotes = snapshot.get('hexVotes');
      personHexed = snapshot.get('personHexed') ?? "";
    });

    // Here we are going to send a notification to EVERYBODY telling them that we've just hexed someone
    if (!game.getGameComplete() && secondsPassed > 1) {
    sinceLastVote = DateTime.now();
    int voteCount = hexVotes[playerName];
    String notificationTitle = "Hex Update";
    String notificationBody = "";
    String sound = "";

    if (personHexed != "") {
    // someone was actually hexed
    notificationBody = "$playerName was hexed";
    sound = NotificationSounds.bigFail;
    } else {
    notificationBody = "$playerName has $voteCount votes to be hexed";
    sound = NotificationSounds.smallBad;
    }

    game.sendEveryoneNotification(notificationTitle, notificationBody, "hexUpdate", sound);
    }
  }

  Future<bool> _updateHexVote(String player) async {
    Map<String, dynamic> hexVotes = {};
    Map<String, dynamic> hexVoted = {};
    String personHexed = "";
    int playerCount = game.getPlayers().length;
    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);


    await ref.get().then((snapshot) {
      hexVotes = snapshot.get('hexVotes');
      hexVoted = snapshot.get('hexVoted');
      personHexed = snapshot.get('personHexed') ?? "";
    });

    // update hex voted to show I voted for this player
    hexVoted[game.getCurrentPlayer()] = player;

    // update the value for how many votes to that player
    if (hexVotes.containsKey(player)) {
      hexVotes[player] += 1;
    } else {
      hexVotes[player] = 1;
    }

    // if it is above 50% in votes, they are the hexed player
    for(String key in hexVotes.keys) {
      if ((hexVotes[key] / playerCount) > 0.5001) {
        personHexed = key;
      }
    }

    await ref.update({
        'hexVotes' : hexVotes,
        'personHexed' : personHexed,
        'hexVoted' : hexVoted
        }
    );

    if (personHexed.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  Future<void> _removeHexVote(String player) async {
    Map<String, dynamic> hexVotes = {};
    Map<String, dynamic> hexVoted = {};
    String personHexed = "";
    int playerCount = game.getPlayers().length;
    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);

    await ref.get().then((snapshot) {
      hexVotes = snapshot.get('hexVotes');
      personHexed = snapshot.get('personHexed') ?? "";
      hexVoted = snapshot.get('hexVoted');
    });

    // remove who I voted for
    hexVoted.remove(game.getCurrentPlayer());

    // remove vote count to that player
    if (hexVotes.containsKey(player)) {
      hexVotes[player] -= 1;
    }

    for(String key in hexVotes.keys) {
      if ((hexVotes[key] / playerCount) > 0.65) {
        personHexed = key;
      }
    }

    await ref.update({
      'hexVotes' : hexVotes,
      'personHexed' : personHexed,
      'hexVoted' : hexVoted
    }
    );
  }

  Future<void> _handleHex(String playr, Map<String, dynamic> hexVoted) async {
    // if the current player has already selected someone
    if (hexVoted.containsKey(game.getCurrentPlayer())) {
      // if the one clicked is one already selected by the player, remove only
      if (hexVoted[game.getCurrentPlayer()] == playr) {
        await _removeHexVote(hexVoted[game.getCurrentPlayer()]);
      } else {
        // remove the one already selected and select the new one
        await _removeHexVote(hexVoted[game.getCurrentPlayer()]);
        await _updateHexVote(playr);
      }
    } else {
      // update the new vote as the first vote ever
      await _updateHexVote(playr);
    }
  }

  Future<void> hexPopup(BuildContext context) async {
    Map<String, dynamic> hexVotes = Map<String, dynamic>.from(game.getHexVotes());
    Map<String, dynamic> hexVoted = Map<String, dynamic>.from(game.getHexVoted());
    Stream<DocumentSnapshot> newStream = const Stream<DocumentSnapshot>.empty();
    List<Player> players = game.getPlayers();
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    game.updateLocalHexInfo();

    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (context, setState) {
              // set personVoted here
              newStream = FirebaseFirestore.instance.collection('games').doc(widget.serverName).snapshots();

              return AlertDialog(
                  backgroundColor: Colors.transparent,
                  insetPadding: const EdgeInsets.all(10),
                  content: StreamBuilder(
                    stream: newStream,
                    builder: (BuildContext context,
                      AsyncSnapshot<DocumentSnapshot> snapshot) {
                      if (snapshot.hasData) {
                        return FutureBuilder(
                          future: loadGame(snapshot),
                          builder: (_, sna) {
                            if (sna.hasData) {
                              hexVotes = Map<String, dynamic>.from(game.getHexVotes());
                              hexVoted = Map<String, dynamic>.from(game.getHexVoted());
                              personVoted = hexVoted[game.getCurrentPlayer()] ?? "";
                              // if someone already above 65% in votes, don't allow anyone to continue and therefore pop
                              for(String key in hexVotes.keys) {
                                if ((hexVotes[key] / players.length) > 0.65) {
                                  Navigator.of(context).pop();
                                }
                              }
                              return SizedBox(
                                  height: (height / 3),
                                  width: (width),
                                  child: ListView.builder(
                                      itemCount: players.length,
                                      itemBuilder: (context, index) {
                                        String player = players[index].nickname;
                                        return GestureDetector(
                                          onTap: () async {
                                            await _handleHex(player, hexVoted);
                                            game.updateLocalHexInfo();
                                            // await sendHexNotification(player); Add back later
                                          },
                                          child: Card(
                                            child: ListTile(
                                              leading: !hexVotes.containsKey(player) ? Text("0 / ${players.length}") : Text("${hexVotes[player]} / ${players.length}"),
                                              title: Text(player),
                                              trailing: personVoted == player ? const Icon(Icons.arrow_drop_down_circle) : const Icon(Icons.arrow_drop_down_circle_outlined),
                                            ),
                                          ),
                                        );
                                      }
                                  )
                              );
                            } else {
                              return const CircularProgressIndicator();
                            }
                          }
                        );
                      } else {
                        return const CircularProgressIndicator();
                      }
                    },
                  )
              );
            },
          );
        });
  }

  Future<void> _displayTextInputDialog(BuildContext context, int cant) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Calling'),
            content: TextFormField (
              keyboardType: TextInputType.number,
              onChanged: (value) {
                setState(() {
                  tempCallValue = value;
                });
              },
              controller: _textFieldController,
              decoration: cant < 0 ? const InputDecoration(hintText: "Enter a call") : InputDecoration(hintText: "Enter a call, can't go $cant"),
            ),
            actions: <Widget>[

              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xFF47A025),
                  onPrimary: Colors.white,
                ),
                onPressed: () {
                  setState(() {
                    callValue = tempCallValue;
                    _textFieldController.clear();
                    tempCallValue = "";
                    Navigator.pop(context);
                  });
                },
                child: const Text("Continue"),
              ),
            ],
          );
        });
  }

  void findMinVersion() async {
    DocumentReference ref = FirebaseFirestore.instance.collection('Keys').doc('version');

    await ref.get().then((snapshot) {
      minLocalVersion = snapshot.get('minLocalVersion');
      minServerVersion = snapshot.get('minServerVersion');
    });
  }

  void resetTimerBoy() {
    if (resetTimer != null && resetTimer!.isActive) return;

    resetTimer = Timer.periodic(const Duration(seconds: 5), (timer) {
      //job
      setState(() {});
    });
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    resetTimer?.cancel();
    super.dispose();
  }

  @override
  void initState()  {
    super.initState();
    resetTimerBoy();
    findMinVersion();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    _serverStream = FirebaseFirestore.instance.collection('games').doc(widget.serverName).snapshots();
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    double toolbarSize = MediaQuery.of(context).padding.top;

    updateCallAlert();
    updateCardAlert();
    updateHexAlert();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xFF256D1B),
      body: StreamBuilder(
          stream: _serverStream,
          builder: (BuildContext context,
              AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.hasData) {
              return FutureBuilder(
                future: loadGame(snapshot),
                builder: (_, sna) {
                  if (sna.hasData) {
                    if (game.isItMyTurn() && notificationLock != game.getTurnCount()) {
                      HapticFeedback.heavyImpact();
                      notificationLock = game.getTurnCount();
                    }
                    return Column(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(00.0, 00.0, 0.0, 00.0),
                            child: Column(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(0.0, toolbarSize * 0.69, 0.0, 0.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                            flex: 6,
                                            child: game.isItMyTurn() ? Center(
                                              child: Container(
                                                padding: const EdgeInsets.fromLTRB(20, 12, 20, 15),
                                                decoration: BoxDecoration(
                                                    borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                                                    border: Border.all(color: const Color(0xFFF44708), width: 4.0),
                                                    color: const Color(0xFFFAA613)
                                                ),
                                                child: const Text(
                                                  "Your Turn",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.bold
                                                  ),
                                                ),
                                              ),
                                            ) : const SizedBox()
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: SizedBox(
                                              width: 500,
                                              height: 50,
                                              child: Center(
                                                child: Container(
                                                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                  decoration: BoxDecoration(
                                                      borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                                                      border: Border.all(color: Colors.black, width: 1.0),
                                                      color: Colors.white
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      game.getTotalCallsString() +
                                                          "/" + (calculateSizeOfHand(game.totalRounds, game.getCurrentRound())).toString(),
                                                      textAlign: TextAlign.center,
                                                      style: const TextStyle(
                                                          color: Colors.black,
                                                          fontWeight: FontWeight.bold
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                        ),

                                        Expanded(
                                            flex: 9,
                                            child: Center(
                                              child: Container(
                                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                child: InkWell(
                                                  onTap: () {
                                                    game.getPreviousPile().isNotEmpty ? fullScoreboardPopup(context) : null;
                                                    },
                                                  child: createScore(game.getScoreBoard()),
                                                ),
                                              ),
                                            )
                                        )
                                      ],
                                    ),
                                  )
                                )

                              ]
                            )
                          )
                        ),

                        // This is the whole middle row of the Play Screen
                        Expanded(
                          flex: 5,
                          child: Row(
                            children: [

                              // This is the game area
                              Expanded(
                                flex: 3,
                                child: Column(
                                  children: [

                                    // This is just to add some space above the game area
                                    Expanded(
                                        child: Container(),
                                      flex: 1,
                                    ),

                                    // This is the dark green coloured background
                                    Expanded(
                                      flex: 10,
                                      child: Padding(
                                      padding: const EdgeInsets.fromLTRB(10.0, 00.0, 0.0, 0.0),
                                      child: Container(
                                          decoration: const BoxDecoration(
                                              color: Color(0x195800FF),
                                              borderRadius: BorderRadius.all(Radius.circular(15.0))
                                          ),
                                          constraints: const BoxConstraints.expand(),
                                          child: LayoutBuilder(
                                            builder: (BuildContext context, BoxConstraints constrains) {
                                              return InkWell(
                                                onTap: () {
                                                  game.getPile().isNotEmpty ? pilePopup(context) : null;
                                                },
                                                child: Stack(
                                                    alignment: Alignment.center,
                                                    children: getCards(constrains.maxHeight, constrains.maxWidth)
                                                ),
                                              );
                                            },
                                          )
                                      ),
                                    ),
                                    )
                                  ],
                                )
                              ),
                              Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        
                                        Expanded(
                                          flex: 1, child:
                                          Container(),
                                        ),

                                        // This will be the timer.
                                        Container(
                                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                          decoration: BoxDecoration(
                                              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                                              border: Border.all(color: Colors.black, width: 1.0),
                                              color: Colors.white
                                          ),
                                          child: Center(
                                            child: Text(
                                              game.timeSinceLastTurn(),
                                              textAlign: TextAlign.center,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold
                                              ),
                                            ),
                                          ),
                                        ),


                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 4.0),
                                            child: InkWell(
                                              child: CardWrapper(cardStr: game.getTrump().getString()).getImage(),
                                              onTap: () {
                                                game.getPreviousPile().isNotEmpty ? previousPilePopup(context) : null;
                                              },
                                            ),
                                            //child: CardWrapper(cardStr: game.getTrump().getString()).getImage(),
                                          ),
                                        ),

                                        /*
                                        Expanded(
                                          flex: 2,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 4.0),
                                            child: InkWell(
                                              child: CardWrapper(cardStr: "back").getImage(),
                                              onTap: () {
                                                game.getPreviousPile().isNotEmpty ? previousPilePopup(context) : null;
                                              },
                                            ),
                                            //child: CardWrapper(cardStr: "back").getImage(),
                                          ),
                                        ),
                                         */

                                        Expanded(
                                          flex: 0,
                                          child: GestureDetector(
                                            child: Container(
                                              alignment: Alignment.center,
                                              height: 90,
                                              width: 50,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(5.0),
                                                color: Colors.white,
                                              ),
                                              child: cardLedText,
                                            ),
                                            onTapDown: (_) {
                                              pressTimer = Timer(const Duration(milliseconds: 5000), notificationMike);
                                            },
                                            onTapUp: (_){
                                              pressTimer.cancel();
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),

                        Expanded(
                          flex: 3,
                          child: game.getGameComplete() == false ? Padding(
                              padding: const EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 0.0),
                              child: game.getMyHand().isNotEmpty ? GridView.builder(
                                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 1,
                                  childAspectRatio: 1.72,
                                  mainAxisSpacing: 10,
                                  crossAxisSpacing: 10,
                                ),
                                scrollDirection: Axis.horizontal,
                                itemCount: game.getMyHand().length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          // if you selected the same one, unselected. Else, select another.
                                          selected == game.getMyHand()[index].cardStr ? selected = "" : selected = game.getMyHand()[index].cardStr;
                                        });
                                        },
                                      child: Stack(
                                        children: [
                                          game.getMyHand()[index].getSpecImage(double.infinity, double.infinity),
                                        Container(
                                          //height: (MediaQuery.of(context).size.height)-100,
                                          decoration: BoxDecoration(
                                              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                                              border : game.getMyHand()[index].cardStr == selected ? Border.all(
                                                  width: 2.5,
                                                  color: const Color(0xFFF44708)
                                              ) : null
                                          ),
                                        ),
                                        ],
                                      ),
                                    );
                                },
                              ) : Container(),
                          ) : Padding(
                              padding: const EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 0.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [

                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      //primary: const Color(0xFF47A025),
                                      onPrimary: Colors.white,
                                    ),
                                    onPressed: () {
                                      // messy but oh well
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    },
                                    child: Container(
                                      width: (width / 3),
                                      height: (height / 10),
                                      decoration: BoxDecoration(
                                          color: const Color(0xFFFAA613),
                                          border: Border.all(
                                            color: Colors.white,
                                            width: 4.0,
                                          ),
                                          borderRadius: const BorderRadius.all(Radius.circular(20))
                                      ),
                                      child: const Center(
                                        child: Text(
                                          "Home",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.white
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      //primary: const Color(0xFF47A025),
                                      onPrimary: Colors.white,
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) => FinalScreen(serverName: widget.serverName)
                                          )
                                      );
                                    },
                                    child: Container(
                                      width: (width / 3),
                                      height: (height / 10),
                                      decoration: BoxDecoration(
                                          color: const Color(0xFFA10702),
                                          border: Border.all(
                                            color: Colors.white,
                                            width: 4.0,
                                          ),
                                          borderRadius: const BorderRadius.all(Radius.circular(20))
                                      ),
                                      child: const Center(
                                        child: Text(
                                          "Scores",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.white
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                          )
                        ),
                        Expanded(
                          flex: 2,
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: ElevatedButton(
                                        onPressed: () async {
                                          // can I call now?
                                          versionCheck = await checkLatestVersion();
                                          int serverVersion = game.getServerVersion();
                                          bool serverVersionCheck = serverVersion >= minServerVersion;

                                          if (!serverVersionCheck) {
                                            Fluttertoast.showToast(
                                              msg: "Server is outdated. Please start a new game.",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              fontSize: 16.0,
                                            );
                                          } else if (!versionCheck) {
                                            Fluttertoast.showToast(
                                              msg: "App needs to be updated to continue!",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              fontSize: 16.0,
                                            );
                                          } else if (game.turnTypeCheck() == 1 || (game.turnTypeCheck() == 0) && !game.placeCardsCheck()) {

                                            if (game.turnTypeCheck() == 0) { // It's not my go, let's queue a call
                                              makeFutureCall(context);
                                            } else { // It is my go, let's make a call
                                              // check if I can't go something
                                              int cant = game.cantGo();
                                              // await calling
                                              await _displayTextInputDialog(context, cant);
                                              try {
                                                int call = int.parse(callValue);
                                                // check if call is in bounds
                                                if (cant != -1) {
                                                  if (call == cant) {
                                                    showCustomAlert(context, "Call issue", "Invalid call", "Continue");
                                                    return;
                                                  }
                                                }
                                                game.callNum(call);
                                                callValue = "";

                                                // next turn
                                                Eraser.clearAllAppNotifications();
                                                Eraser.resetBadgeCountAndRemoveNotificationsFromCenter();
                                                await game.nextTurn(false);
                                              } on FormatException {
                                                callValue = "";
                                                showCustomAlert(context, "Call issue", "Invalid call", "Continue");
                                              }
                                            }
                                          } else {
                                            Fluttertoast.showToast(
                                              msg: "Patience, young Jedi. It's not time to call.",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              fontSize: 16.0,
                                            );
                                          }
                                        },
                                        child: const Icon(
                                          Icons.campaign,
                                          color: Colors.white,
                                        ),
                                        style: ElevatedButton.styleFrom(
                                            primary: game.turnTypeCheck() == 1 ? const Color(0xFFA1C349) : Colors.grey,
                                            shape: CircleBorder(
                                                side: BorderSide(
                                                  width: callAlertThickness,
                                                  color: callAlertColor,
                                                )
                                            ),
                                            padding: const EdgeInsets.all(5.0)
                                        )
                                    ),
                                  ),
                                  SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: ElevatedButton(
                                        onPressed: () async {
                                          versionCheck = await checkLatestVersion();
                                          int serverVersion = game.getServerVersion();
                                          bool serverVersionCheck = serverVersion >= minServerVersion;

                                          if (!serverVersionCheck) {
                                            Fluttertoast.showToast(
                                              msg: "Server is outdated. Please start a new game.",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              fontSize: 16.0,
                                            );
                                          } else if (!versionCheck) {
                                            Fluttertoast.showToast(
                                              msg: "App needs to be updated to continue!",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              fontSize: 16.0,
                                            );
                                          } else {
                                            String player = game.getHexedPlayer();
                                            if (player.isEmpty) {
                                              await hexPopup(context);
                                            } else {
                                              Fluttertoast.showToast(
                                                msg: "$player is already hexed for the game",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.CENTER,
                                                timeInSecForIosWeb: 1,
                                                fontSize: 16.0,
                                              );
                                            }
                                          }
                                        },
                                        child: const Icon(
                                          Icons.auto_awesome,
                                          color: Colors.white,
                                        ),
                                        style: ElevatedButton.styleFrom(
                                            primary: game.getHexedPlayer().isEmpty ? const Color(0xFF4B4A67) : Colors.grey,
                                            shape: CircleBorder(
                                                side: BorderSide(
                                                  width: hexAlertThickness,
                                                  color: hexAlertColor,
                                                )
                                            ),
                                            padding: const EdgeInsets.all(5.0)
                                        )
                                    ),
                                  ),
                                  SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: ElevatedButton(
                                        onPressed: () async {
                                          // can I place a card?
                                          versionCheck = await checkLatestVersion();
                                          int serverVersion = game.getServerVersion();
                                          bool serverVersionCheck = serverVersion >= minServerVersion;

                                          if (!serverVersionCheck) {
                                            Fluttertoast.showToast(
                                              msg: "Server is outdated. Please start a new game.",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              fontSize: 16.0,
                                            );
                                          } else if (!versionCheck) {
                                            Fluttertoast.showToast(
                                              msg: "App needs to be updated to continue!",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              fontSize: 16.0,
                                            );
                                          } else if (game.turnTypeCheck() == 2 || game.turnTypeCheck() == 0) { // If it's my go, or someone else's go
                                            // have I selected a card?
                                            if (selected.isNotEmpty) {
                                              CardWrapper card = CardWrapper(cardStr: selected);

                                              if (game.turnTypeCheck() == 0) { // It's not my go, let's try to queue the card
                                                makeFutureCard(context, card); //
                                              } else { // It's my go! Let's try to place the card on the pile
                                                // am I allowed to select this card?
                                                if (game.canIPlace(card)) {
                                                  // attempt to place the card
                                                  bool placed = game.placeCard(card);
                                                  if (placed) {
                                                    // place card and let everyone see
                                                    selected = "";

                                                    // next turn
                                                    Eraser.clearAllAppNotifications();
                                                    Eraser.resetBadgeCountAndRemoveNotificationsFromCenter();
                                                    await game.nextTurn(false);
                                                  } else {
                                                    showCustomAlert(context, "Card issue", "An error occurred when trying to place that card.", "Continue");
                                                  }
                                                } else {
                                                  showCustomAlert(context, "Card issue", "That card can't be placed.", "Continue");
                                                }
                                              }
                                            } else {
                                              Fluttertoast.showToast(
                                                msg: "Please select a card",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.CENTER,
                                                timeInSecForIosWeb: 1,
                                                fontSize: 16.0,
                                              );
                                            }// if it's someone else's go
                                          } else {
                                          Fluttertoast.showToast(
                                          msg: "It's not your go, knucklehead",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.CENTER,
                                          timeInSecForIosWeb: 1,
                                          fontSize: 16.0,
                                          );
                                          }
                                        },
                                        child: const Icon(
                                          Icons.done,
                                          color: Colors.white,
                                        ),
                                        style: ElevatedButton.styleFrom(
                                          primary: selected.isEmpty ? Colors.grey : const Color(0xFFA10702),
                                          shape: CircleBorder(
                                            side: BorderSide(
                                              width: cardAlertThickness,
                                              color: cardAlertColor,
                                            )
                                          ),
                                          padding: const EdgeInsets.all(5.0)
                                        )
                                    ),
                                  )
                                ],
                              )
                          ),
                        )
                      ],
                    );
                  } else {
                    return const Center(child: CircularProgressIndicator());
                  }
                }
              );
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }
      )
    );
  }
}
