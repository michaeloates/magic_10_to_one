import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:magic_10_to_one/Models/CardWrapper.dart';
import 'package:magic_10_to_one/enums/cards.dart';
import 'package:magic_10_to_one/game/PlayScreen.dart';
import 'GameWidgets/MagicCardFlippable.dart';


class OrderSelection extends StatefulWidget {
  final String serverName;
  const OrderSelection({Key? key, required this.serverName}) : super(key: key);

  @override
  _OrderSelectionState createState() => _OrderSelectionState();
}

class _OrderSelectionState extends State<OrderSelection> {
  Box profileBox = Hive.box(ProfileKeys.profile);
  late Stream<DocumentSnapshot> _serverStream;
  late String? notificationToken;

  /*
  TODO: Work out how to find the first player...
  Future<void> _notifyFirstPlayer() async {
    Map<String, dynamic> notificationTokens = {};
    Map<String, dynamic> playersOrder = {};
    bool firstPlayerNotified = true;
    String firstPlayerNotificationToken = "";
    String firstPlayer = "";
    String serverName = widget.serverName;

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    await ref.get().then((snapshot) {
      notificationTokens = snapshot.get('notificationTokens');
      firstPlayerNotified = snapshot.get('firstPlayerNotified');
      playersOrder = snapshot.get('playersOrder');
    });

    if (firstPlayerNotified) {
      return;
    }

    playersOrder.forEach((key, value) {
      if (value == 0) {
        firstPlayer = key;
      }
    });

    notificationTokens.forEach((key, value) {
      if (key == firstPlayer) {
        firstPlayerNotificationToken = value;
      }
    });

    sendNotification(firstPlayerNotificationToken, "Magic 10 to 1", "Please rejoin $serverName, you're up first!", widget.serverName);

    await ref.update({
      "firstPlayerNotified" : true
    });
  }
   */

  Future<void> _handleCardUpload(String name, String cardStr) async {
    Map<String, dynamic> selectionHandUnknown = {};

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    await ref.get().then((snapshot) {
      selectionHandUnknown = snapshot.get('selectionHandUnknown');
    });


    if (!selectionHandUnknown.containsKey(name)) {
      selectionHandUnknown[name] = cardStr;
      await ref.update({
        "selectionHandUnknown" : selectionHandUnknown
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _serverStream = FirebaseFirestore.instance.collection('games').doc(widget.serverName).snapshots();

    FirebaseMessaging.instance.getToken().then((value) {
      notificationToken = value;
    });

    return Scaffold(
      backgroundColor: const Color(0xFF256D1B),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Expanded(
              flex: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  StreamBuilder(
                    stream: _serverStream,
                    builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
                      if (snapshot.hasData) {
                        var name = profileBox.get(ProfileKeys.profileName, defaultValue: null);
                        return MagicCardFlippable(
                          func: () async {
                            String cardString = snapshot.data!.get('selectionHandKnown')[name];
                            await _handleCardUpload(name, cardString);
                            //await _handleNotificationTokenUpload(name);
                          },
                          cardStr: CardWrapper(
                              cardStr: snapshot.data!.get('selectionHandKnown')[name]
                          )
                        );
                      } else {
                        return const CircularProgressIndicator();
                      }
                    })
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(
                padding: const EdgeInsets.fromLTRB(10, 5.0, 10.0, 5.0),
                margin: const EdgeInsets.fromLTRB(
                    25.0,
                    5.0,
                    25.0,
                    40.0),
                decoration: BoxDecoration(
                  color: const Color(0xFF256D1B),
                  borderRadius: BorderRadius.circular(15.0),
                  border: Border.all(
                    color: Colors.white,
                    width: 5.0,
                  )
                ),
                child: StreamBuilder(
                  stream: _serverStream,
                  builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
                    if (snapshot.hasData) {
                      var map = snapshot.data!.get('selectionHandUnknown');
                      int playerCount = snapshot.data!.get('playerCount');
                      var keyList = [];
                      if (map == null) {
                        return Container();
                      }
                      if (map.isEmpty) {
                        return Container();
                      }

                      for (var key in map.keys) {
                        keyList.add(key);
                      } /* keyList.length < playerCount ?  */
                      return Column (
                        children: [
                          Expanded(
                            flex: 3,
                            child: GridView.builder(
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio: ((MediaQuery.of(context).size.width) / (MediaQuery.of(context).size.height / 4)),
                                mainAxisSpacing: 10,
                                crossAxisSpacing: 10,
                              ),
                              scrollDirection: Axis.horizontal,
                              itemCount: snapshot.data!.get('selectionHandUnknown').length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  children: [
                                    Text(
                                      keyList[index],
                                      style: const TextStyle(color: Colors.white),
                                    ),
                                    CardWrapper(
                                        cardStr: snapshot.data!.get('selectionHandUnknown')[keyList[index]]
                                    ).getImage(),
                                  ],
                                );
                              },
                            ),
                          ),
                          keyList.length >= playerCount ? Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(5, 5.0, 5.0, 5.0),
                              child: TextButton(
                                onPressed: () async {
                                  // _notifyFirstPlayer(); TODO Put back
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => PlayScreen(serverName: widget.serverName)
                                    )
                                  );
                                },
                                child: Container(
                                  height: 70,
                                  width: 250,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                      color: const Color(0xFFFAA613),
                                      width: 5,
                                    ),
                                    borderRadius: const BorderRadius.all(Radius.circular(20))
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "Join Game",
                                      style: TextStyle(
                                      fontSize: 35,
                                      color: Color(0xFFFAA613)
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ) : const Expanded(flex: 1, child: SizedBox())
                        ],
                      );
                    } else {
                      return const CircularProgressIndicator();
                    }
                  },
                )
              ) ,
            ),
          ],
        ),
      )
    );
  }
}
