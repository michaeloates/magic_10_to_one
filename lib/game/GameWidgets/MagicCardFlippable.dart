import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:magic_10_to_one/Models/CardWrapper.dart';
import 'package:playing_cards/playing_cards.dart';

class MagicCardFlippable extends StatefulWidget {
  final void Function() func;
  CardWrapper cardStr = CardWrapper(cardStr: "");

  MagicCardFlippable({Key? key, required this.func, required this.cardStr}) : super(key: key);

  @override
  _MagicCardFlippableState createState() => _MagicCardFlippableState();
}

class _MagicCardFlippableState extends State<MagicCardFlippable> {
  Widget _buildFront() {
    return Container(
      height: 150.0,
      width: 100.0,
      child: widget.cardStr.getImage()
    );
  }

  Widget _buildBack() {
    return Container(
        height: 150.0,
        width: 100.0,
        child: Image.asset("assets/images/CardImages/back.png")
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlipCard(
        fill: Fill.fillBack,
        back: _buildFront(),
        front: _buildBack(),
        onFlip: () {
          widget.func();
        },
      )
    );
  }
}
