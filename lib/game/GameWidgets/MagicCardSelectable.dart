import 'package:flutter/material.dart';
import 'package:magic_10_to_one/Models/CardWrapper.dart';

class MagicCardSelectable extends StatefulWidget {
  final void Function() func;
  CardWrapper cardStr = CardWrapper(cardStr: "");
  String selected;

  MagicCardSelectable({Key? key, required this.func, required this.cardStr, required this.selected}) : super(key: key);

  @override
  _MagicCardSelectableState createState() => _MagicCardSelectableState();
}

class _MagicCardSelectableState extends State<MagicCardSelectable> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    widget.selected == widget.cardStr.cardStr ? selected = true : selected = false;

    return GestureDetector(
      onTap: () {
        setState(() {
          selected = !selected;
          selected ? widget.selected = widget.cardStr.cardStr : widget.selected = "";
          widget.func();
        });
      },
      child: Container(
        decoration: BoxDecoration(
          border : selected ? Border.all(
            width: 2,
            color: Colors.yellow
          ) : null
        ),
        child: widget.cardStr.getImage(),
      )
    );
  }
}
