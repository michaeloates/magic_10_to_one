import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:magic_10_to_one/Models/GameUtilities.dart';

import '../Models/Game.dart';

class FinalScreen extends StatefulWidget {
  final String serverName;

  const FinalScreen({Key? key, required this.serverName}) : super(key: key);

  @override
  _FinalScreenState createState() => _FinalScreenState();
}

class _FinalScreenState extends State<FinalScreen> {
  late Stream<DocumentSnapshot> _serverStream;
  Game game = Game();
  bool details = false;
  late double width;
  late double height;


  loadGame(AsyncSnapshot<DocumentSnapshot> snapshot) async {
    await game.load(snapshot);
    setState(() {});
    return 1;
  }

  List<TableRow> getSimpleRows(BoxConstraints constraints) {
    Map<String, List<String>> scoreboard = game.getFinalScoreboardSimple();
    List<String> players = scoreboard["names"] ?? [];
    List<String> scores = scoreboard["scores"] ?? [];
    List<String> places = scoreboard["places"] ?? [];
    List<TableRow> rows = [];
    int numRows = 1 + players.length;

    // initial name row
    rows.add(TableRow(
        children:[
          Container(
              decoration: const BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0)),
                color: Color.fromARGB(100, 170, 170, 170)
              ),
              height: (constraints.maxHeight / numRows),
              width: (constraints.maxWidth * 0.5),
              child: const Center(
                child: Text(
                  "Name: ",
                  style: TextStyle(
                    fontSize: 23.0,
                    color: Colors.black,
                    decoration: TextDecoration.underline
                  ),
                ),
              )
          ),
          Container(
              color: const Color.fromARGB(100, 170, 170, 170),
              height: (constraints.maxHeight / numRows),
              width: (constraints.maxWidth * 0.25),
              child: const Center(
                child: Text(
                  "Score: ",
                  style: TextStyle(
                    fontSize: 23.0,
                    color: Colors.black,
                    decoration: TextDecoration.underline
                  ),
                ),
              )
          ),
          Container(
              decoration: const BoxDecoration(borderRadius: BorderRadius.only(topRight: Radius.circular(15.0)),
                  color: Color.fromARGB(100, 170, 170, 170)
              ),
              height: (constraints.maxHeight / numRows),
              width: (constraints.maxWidth * 0.25),
              child: const Center(
                child: Text(
                  "Place: ",
                  style: TextStyle(
                      fontSize: 23.0,
                      color: Colors.black,
                      decoration: TextDecoration.underline
                  ),
                ),
              )
          ),
        ]
    ));

    // actual attributes
    for (var i = 0; i < players.length; i++) {
      bool best;
      places[i] == "1st" ? best = true : best = false;

      rows.add(TableRow(
          children: [
            SizedBox(
                height: (constraints.maxHeight / numRows),
                width: (constraints.maxWidth / 3),
                child: Center(
                  child: Text(
                    best ? "${players[i]} 👑" : players[i],
                    style: const TextStyle(
                        fontSize: 24.0,
                        color: Colors.black,
                    ),
                  ),
                )
            ),
            SizedBox(
                height: (constraints.maxHeight / numRows),
                width: (constraints.maxWidth / 3),
                child: Center(
                  child: Text(
                    scores[i],
                    style: const TextStyle(
                        fontSize: 24.0,
                        color: Colors.black,
                    ),
                  ),
                )
            ),
            SizedBox(
                height: (constraints.maxHeight / numRows),
                width: (constraints.maxWidth / 3),
                child: Center(
                  child: Text(
                    places[i],
                    style: const TextStyle(
                      fontSize: 24.0,
                      color: Colors.black,
                    ),
                  ),
                )
            ),
          ]
      ));
    }
    return rows;
  }

  List<TableRow> getDetailedRows(BoxConstraints constraints) {
    List<TableRow> rows = [];
    List<List<String>> scoreboard = game.getFinalScoreboard();
    int numRounds = scoreboard.length;
    int numPlayers = scoreboard[0].length;

    for(var i = 0; i < numRounds; i++) {
      List<Widget> tempRow = [];
      for(var j = 0; j < numPlayers; j++) {
        tempRow.add(
            Center(
              child: Text(
                scoreboard[i][j],
                style: const TextStyle(
                  fontSize: 15.0,
                  color: Colors.black,
                ),
              ),
            )
        );
      }
      rows.add(TableRow(
        children: tempRow
      ));
    }

    return rows;
  }

  Widget createFinalScoreSummary() {
    Widget scoreboard = Container(
        margin: const EdgeInsets.all(3.0),
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15.0))
        ),
        child: LayoutBuilder(
          builder: (context, constraints) => Table(
            children: details ? getDetailedRows(constraints) : getSimpleRows(constraints),
            columnWidths: details ? null : const {
              0: FlexColumnWidth(2),
              1: FlexColumnWidth(1),
              2: FlexColumnWidth(1),
            },
          ),
        )
    );

    return details ? SingleChildScrollView(child: scoreboard) : scoreboard;
  }

  @override
  Widget build(BuildContext context) {
    _serverStream = FirebaseFirestore.instance.collection('games').doc(widget.serverName).snapshots();
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    String serverName = game.getServerName();
    String timeEnded = timestampToString(game.timeEnded);
    String timeStarted = timestampToString(game.timeStarted);
    String gameLength = findGameLength(game.totalRounds);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xFF256D1B),
      body: StreamBuilder(
        stream: _serverStream,
        builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasData) {
            return FutureBuilder(
              future: loadGame(snapshot),
              builder: (_, sna) {
                if (sna.hasData) {
                  return Column(
                    children: [
                      const Expanded(
                        flex: 1,
                        child: SizedBox(),
                      ),
                      Expanded(
                          flex: 2,
                          child: Padding(
                              padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 30),
                              child: Container(
                                padding: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                                decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                    //border: Border.all(color: const Color(0xFFF44708), width: 4.0),
                                    color: Colors.white
                                ),
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(
                                    text: "$serverName\n",
                                    style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold),
                                    children: <TextSpan>[
                                      TextSpan(text: 'Length: $gameLength\n', style: TextStyle(fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal)),
                                      TextSpan(text: 'Started: $timeStarted\n', style: TextStyle(fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal)),
                                      TextSpan(text: 'Ended: $timeEnded\n', style: TextStyle(fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal)),
                                    ],
                                  ),
                                ),
                              ),
                          )
                      ),
                      Expanded(
                        flex: 6,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                          child: createFinalScoreSummary()
                        )
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  onPrimary: Colors.white,
                                ),
                                onPressed: () {
                                  setState(() {
                                    details = !details;
                                  });
                                },
                                child: Container(
                                  width: (width / 3),
                                  height: (height / 10),
                                  decoration: BoxDecoration(
                                      color: const Color(0xFF550527),
                                      border: Border.all(
                                        color: Colors.white,
                                        width: 4.0,
                                      ),
                                      borderRadius: const BorderRadius.all(Radius.circular(20))
                                  ),
                                  child: Center(
                                    child: Text(
                                      details ? "Summary" : "Details",
                                      style: const TextStyle(
                                          fontSize: 15,
                                          color: Colors.white
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  onPrimary: Colors.white,
                                ),
                                onPressed: () {
                                  // super messy but oh well
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  width: (width / 3),
                                  height: (height / 10),
                                  decoration: BoxDecoration(
                                      color: const Color(0xFFF44708),
                                      border: Border.all(
                                        color: Colors.white,
                                        width: 4.0,
                                      ),
                                      borderRadius: const BorderRadius.all(Radius.circular(20))
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "Back",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                } else {
                  return const CircularProgressIndicator();
                }
              }
            );
          } else {
            return const CircularProgressIndicator();
          }
        },
      )
    );
  }
}
