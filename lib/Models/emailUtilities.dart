import 'package:cloud_firestore/cloud_firestore.dart';

Future sendThoseEmails(List<String> emails, List<List<String>> finalScoreboard) async {
  for (String emailAddress in emails) {
    await sendScoreboardEmail(emailAddress, finalScoreboard);
  }
}

Future sendScoreboardEmail(String emailAddress, List<List<String>> finalScoreboard) async {
  String html = scoreboardEmailText(finalScoreboard);
  String subject = scoreboardEmailSubject();

  await sendEmail(emailAddress, html, subject);
}

Future sendFeedbackEmail(String feedback) async {
  await sendEmail("10.to.1.magic@gmail.com", feedbackEmailBody(feedback), feedbackEmailSubject());
}

sendEmail(String address, String html, String subject) async {
  await FirebaseFirestore.instance.collection('emails').doc(address).set({
    'address' : address,
    'emailSent' : false,
    'html' : html,
    'subject' : subject,
  });
}

String feedbackEmailBody(String feedback) {
  String html = "";

  html = html + "<!DOCTYPE html>";
  html = html + "<html>";

  html = html + "<head>";
  html = html + "<style>";
  html = html + "</style>";
  html = html + "</head>";

  html = html + "<body>";

  html = html + "<p>" + feedback + "</p>";

  html = html + "</body>";
  html = html + "</html>";


  return html;
}

String feedbackEmailSubject() {
  String subject = "";
  int weekday = DateTime.now().weekday;
  int month = DateTime.now().month;
  int day = DateTime.now().day;
  int hour = DateTime.now().hour;
  int minute = DateTime.now().minute;

  subject = subject + "Magic 10 to 1 Feedback - ";

  subject = subject + findWeekday(weekday) + ", ";
  subject = subject + findMonth(month) + " ";
  subject = subject + findDay(day) + ", at ";
  subject = subject + findTime(hour, minute);

  return subject;
}

String scoreboardEmailText(List<List<String>> finalScoreboard) {
  List<String> playerList = finalScoreboard[0];
  int numRounds = finalScoreboard.length - 1;

  String html = "";

  html = html + "<!DOCTYPE html>";
  html = html + "<html>";

  html = html + "<head>";
  html = html + "<style>";
  html = html + "table, th, td {";
  html = html + "border:1px solid #7E7E7E;";
  html = html + "border-collapse: collapse;";
  html = html + "text-align:center;";
  html = html + "padding:5px;";
  html = html + "}";
  html = html + "th {";
  html = html + "background-color:#D3D3D3";
  html = html + "}";
  html = html + "caption {";
  html = html + "font-weight:bold;";
  html = html + "font-size:120%;";
  html = html + "}";
  html = html + "</style>";
  html = html + "</head>";

  html = html + "<body>";

  html = html + "<h2>Thanks for playing 🪄 Magic 10 to 1! Here are the final results.</h2>";

  html = html + '<table>';
  html = html + "<caption>Final Scoreboard<caption>";

  html = html + "<tr>";
  html = html + "<th></th>";
  for (String playerName in playerList) {
    html = html + "<th>" + playerName + "</th>";
  }
  html = html + "</tr>";

  for (int i = 0; i < numRounds; i++) {
    html = html + "<tr>";
    html = html + "<td>${i+1}</td>";

    for (String score in finalScoreboard[i+1]) {
      html = html + "<td>$score</td>";
    }

    html = html + "</tr>";
  }

  html = html + "</table>";
  html = html + "</body>";
  html = html + "</html>";


  return html;
}

String scoreboardEmailSubject (){
  String subject = "";
  int weekday = DateTime.now().weekday;
  int month = DateTime.now().month;
  int day = DateTime.now().day;
  int hour = DateTime.now().hour;
  int minute = DateTime.now().minute;

  subject = subject + "🪄 Magic 10 to 1 - ";

  subject = subject + findWeekday(weekday) + ", ";
  subject = subject + findMonth(month) + " ";
  subject = subject + findDay(day) + ", at ";
  subject = subject + findTime(hour, minute);

  if (month == 8 && day == 4) {
    subject = subject + "( 🎉 HB Magician!)";
  } else if (month == 8 && day == 30) {
    subject = subject + "( 🎉 HB Dawei!)";
  }

  return subject;
}

String findWeekday(int weekday) {
  if (weekday == 1) {
    return "Monday";
  } else if (weekday == 2) {
    return "Tuesday";
  } else if (weekday == 3) {
    return "Wednesday";
  } else if (weekday == 4) {
    return "Thursday";
  } else if (weekday == 5) {
    return "Friday";
  } else if (weekday == 6) {
    return "Saturday";
  } else if (weekday == 7) {
    return "Sunday";
  } else {
    return "Error";
  }
}

String findMonth(int month) {
  if (month == 1) {
    return "January";
  } else if (month == 2) {
    return "February";
  } else if (month == 3) {
    return "March";
  } else if (month == 4) {
    return "April";
  } else if (month == 5) {
    return "May";
  } else if (month == 6) {
    return "June";
  } else if (month == 7) {
    return "July";
  } else if (month == 8) {
    return "August";
  } else if (month == 9) {
    return "September";
  } else if (month == 10) {
    return "October";
  } else if (month == 11) {
    return "November";
  } else if (month == 12) {
    return "December";
  } else {
    return "Error";
  }
}

String findDay(int day) {
  if (day == 1) {
    return "1st";
  } else if (day == 2) {
    return "2nd";
  } else if (day == 3) {
    return "3rd";
  } else if (day == 21) {
    return "21st";
  } else if (day == 22) {
    return "22nd";
  } else if (day == 23) {
    return "23rd";
  } else if (day == 31) {
    return "31st";
  } else {
    return day.toString() + "th";
  }
}

String findTime(int hour, int minute) {
  String result = "";
  String minuteString;

  if (minute < 10) {
    minuteString = "0" + minute.toString();
  } else {
    minuteString = minute.toString();
  }

  if (hour == 0) {
    result = result + "12:" + minuteString + "pm";
  } else if (hour < 13) {
    result = result + hour.toString() + ":" + minuteString + "am";
  } else {
    result = result + (hour - 12).toString() + ":" + minuteString + "pm";
  }

  return result;
}