import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:magic_10_to_one/Models/CardWrapper.dart';
import 'package:magic_10_to_one/Models/Decks/Deck.dart';
import 'package:magic_10_to_one/Models/Decks/Deck40.dart';
import 'package:magic_10_to_one/Models/Decks/Deck50.dart';
import 'package:magic_10_to_one/Models/Decks/Deck60.dart';
import 'package:magic_10_to_one/Models/Decks/Deck63.dart';
import 'package:magic_10_to_one/Models/Decks/Deck64.dart';
import 'package:magic_10_to_one/Models/Decks/Deck70.dart';
import 'Decks/Deck72.dart';
import 'Decks/Deck80.dart';
import 'package:magic_10_to_one/enums/cards.dart';

Map cardValues = {
  "02" : 2,
  "03" : 3,
  "04" : 4,
  "05" : 5,
  "06" : 6,
  "07" : 7,
  "08" : 8,
  "09" : 9,
  "10" : 10,
  "11" : 11,
  "12" : 12,
  "13" : 13,
  "14" : 14,
  "15" : 15,
  "16" : 16,
  "17" : 17,
  "Ja" : 100,
  "Qu" : 101,
  "Ki" : 102,
  "01" : 103,
  "Jo" : 999,
};

Map handSortingValues = {
  CardKeys.clubTwo : 1,
  CardKeys.clubThree : 2,
  CardKeys.clubFour : 3,
  CardKeys.clubFive : 4,
  CardKeys.clubSix : 5,
  CardKeys.clubSeven : 6,
  CardKeys.clubEight : 7,
  CardKeys.clubNine : 8,
  CardKeys.clubTen : 9,
  CardKeys.clubEleven : 10,
  CardKeys.clubTwelve : 11,
  CardKeys.clubThirteen : 12,
  CardKeys.clubFourteen : 13,
  CardKeys.clubFifteen : 14,
  CardKeys.clubSixteen : 15,
  CardKeys.clubSeventeen : 16,
  CardKeys.clubJack : 17,
  CardKeys.clubQueen : 18,
  CardKeys.clubKing : 19,
  CardKeys.clubAce : 20,

  CardKeys.spadeTwo : 30,
  CardKeys.spadeThree : 31,
  CardKeys.spadeFour : 32,
  CardKeys.spadeFive : 33,
  CardKeys.spadeSix : 34,
  CardKeys.spadeSeven : 35,
  CardKeys.spadeEight : 36,
  CardKeys.spadeNine : 37,
  CardKeys.spadeTen : 38,
  CardKeys.spadeEleven : 39,
  CardKeys.spadeTwelve : 40,
  CardKeys.spadeThirteen : 41,
  CardKeys.spadeFourteen : 42,
  CardKeys.spadeFifteen : 43,
  CardKeys.spadeSixteen : 44,
  CardKeys.spadeSeventeen : 45,
  CardKeys.spadeJack : 46,
  CardKeys.spadeQueen : 47,
  CardKeys.spadeKing : 48,
  CardKeys.spadeAce : 49,

  CardKeys.diamondTwo : 60,
  CardKeys.diamondThree : 61,
  CardKeys.diamondFour : 62,
  CardKeys.diamondFive : 63,
  CardKeys.diamondSix : 64,
  CardKeys.diamondSeven : 65,
  CardKeys.diamondEight : 66,
  CardKeys.diamondNine : 67,
  CardKeys.diamondTen : 68,
  CardKeys.diamondEleven : 69,
  CardKeys.diamondTwelve : 70,
  CardKeys.diamondThirteen : 71,
  CardKeys.diamondFourteen : 72,
  CardKeys.diamondFifteen : 73,
  CardKeys.diamondSixteen : 74,
  CardKeys.diamondSeventeen : 75,
  CardKeys.diamondJack : 76,
  CardKeys.diamondQueen : 77,
  CardKeys.diamondKing : 78,
  CardKeys.diamondAce : 79,

  CardKeys.heartTwo : 90,
  CardKeys.heartThree : 91,
  CardKeys.heartFour : 92,
  CardKeys.heartFive : 93,
  CardKeys.heartSix : 94,
  CardKeys.heartSeven : 95,
  CardKeys.heartEight : 96,
  CardKeys.heartNine : 97,
  CardKeys.heartTen : 98,
  CardKeys.heartEleven : 99,
  CardKeys.heartTwelve : 100,
  CardKeys.heartThirteen : 101,
  CardKeys.heartFourteen : 102,
  CardKeys.heartFifteen : 103,
  CardKeys.heartSixteen : 104,
  CardKeys.heartSeventeen : 105,
  CardKeys.heartJack : 106,
  CardKeys.heartQueen : 107,
  CardKeys.heartKing : 108,
  CardKeys.heartAce : 109,

  CardKeys.joker : 120,
};

Map orderSelectionValues = {

  CardKeys.clubTwo : 1,
  CardKeys.spadeTwo : 2,
  CardKeys.diamondTwo : 3,
  CardKeys.heartTwo : 4,

  CardKeys.clubThree : 5,
  CardKeys.spadeThree : 6,
  CardKeys.diamondThree : 7,
  CardKeys.heartThree : 8,

  CardKeys.clubFour : 9,
  CardKeys.spadeFour : 10,
  CardKeys.diamondFour : 11,
  CardKeys.heartFour : 12,

  CardKeys.clubFive : 13,
  CardKeys.spadeFive : 14,
  CardKeys.diamondFive : 15,
  CardKeys.heartFive : 16,

  CardKeys.clubSix : 17,
  CardKeys.spadeSix : 18,
  CardKeys.diamondSix : 19,
  CardKeys.heartSix : 20,

  CardKeys.clubSeven : 21,
  CardKeys.spadeSeven : 22,
  CardKeys.diamondSeven : 23,
  CardKeys.heartSeven : 24,

  CardKeys.clubEight : 25,
  CardKeys.spadeEight : 26,
  CardKeys.diamondEight : 27,
  CardKeys.heartEight : 28,

  CardKeys.clubNine : 29,
  CardKeys.spadeNine : 30,
  CardKeys.diamondNine : 31,
  CardKeys.heartNine : 32,

  CardKeys.clubTen : 33,
  CardKeys.spadeTen : 34,
  CardKeys.diamondTen : 35,
  CardKeys.heartTen : 36,

  CardKeys.clubEleven : 37,
  CardKeys.spadeEleven : 38,
  CardKeys.diamondEleven : 39,
  CardKeys.heartEleven : 40,

  CardKeys.clubTwelve : 41,
  CardKeys.spadeTwelve : 42,
  CardKeys.diamondTwelve : 43,
  CardKeys.heartTwelve : 44,

  CardKeys.clubThirteen : 45,
  CardKeys.spadeThirteen : 46,
  CardKeys.diamondThirteen : 47,
  CardKeys.heartThirteen : 48,

  CardKeys.clubFourteen : 49,
  CardKeys.spadeFourteen : 50,
  CardKeys.diamondFourteen : 51,
  CardKeys.heartFourteen : 52,

  CardKeys.clubFifteen : 53,
  CardKeys.spadeFifteen : 54,
  CardKeys.diamondFifteen : 55,
  CardKeys.heartFifteen : 56,

  CardKeys.clubSixteen : 57,
  CardKeys.spadeSixteen : 58,
  CardKeys.diamondSixteen : 59,
  CardKeys.heartSixteen : 60,

  CardKeys.clubSeventeen : 61,
  CardKeys.spadeSeventeen : 62,
  CardKeys.diamondSeventeen : 63,
  CardKeys.heartSixteen : 64,

  CardKeys.clubJack : 101,
  CardKeys.spadeJack : 102,
  CardKeys.diamondJack : 103,
  CardKeys.heartJack : 104,

  CardKeys.clubQueen : 105,
  CardKeys.spadeQueen : 106,
  CardKeys.diamondQueen : 107,
  CardKeys.heartQueen : 108,

  CardKeys.clubKing : 109,
  CardKeys.spadeKing : 110,
  CardKeys.diamondKing : 111,
  CardKeys.heartKing : 112,

  CardKeys.clubAce : 113,
  CardKeys.spadeAce : 114,
  CardKeys.diamondAce : 115,
  CardKeys.heartAce : 116,

  CardKeys.joker : 117,
};

List<String> allCards = [

  CardKeys.clubTwo,
  CardKeys.spadeTwo,
  CardKeys.diamondTwo,
  CardKeys.heartTwo,

  CardKeys.clubThree,
  CardKeys.spadeThree,
  CardKeys.diamondThree,
  CardKeys.heartThree,

  CardKeys.clubFour,
  CardKeys.spadeFour,
  CardKeys.diamondFour,
  CardKeys.heartFour,

  CardKeys.clubFive,
  CardKeys.spadeFive,
  CardKeys.diamondFive,
  CardKeys.heartFive,

  CardKeys.clubSix,
  CardKeys.spadeSix,
  CardKeys.diamondSix,
  CardKeys.heartSix,

  CardKeys.clubSeven,
  CardKeys.spadeSeven,
  CardKeys.diamondSeven,
  CardKeys.heartSeven,

  CardKeys.clubEight,
  CardKeys.spadeEight,
  CardKeys.diamondEight,
  CardKeys.heartEight,

  CardKeys.clubNine,
  CardKeys.spadeNine,
  CardKeys.diamondNine,
  CardKeys.heartNine,

  CardKeys.clubTen,
  CardKeys.spadeTen,
  CardKeys.diamondTen,
  CardKeys.heartTen,

  CardKeys.clubEleven,
  CardKeys.spadeEleven,
  CardKeys.diamondEleven,
  CardKeys.heartEleven,

  CardKeys.clubTwelve,
  CardKeys.spadeTwelve,
  CardKeys.diamondTwelve,
  CardKeys.heartTwelve,

  CardKeys.clubThirteen,
  CardKeys.spadeThirteen,
  CardKeys.diamondThirteen,
  CardKeys.heartThirteen,

  CardKeys.clubFourteen,
  CardKeys.spadeFourteen,
  CardKeys.diamondFourteen,
  CardKeys.heartFourteen,

  CardKeys.clubFifteen,
  CardKeys.spadeFifteen,
  CardKeys.diamondFifteen,
  CardKeys.heartFifteen,

  CardKeys.clubSixteen,
  CardKeys.spadeSixteen,
  CardKeys.diamondSixteen,
  CardKeys.heartSixteen,

  CardKeys.clubSeventeen,
  CardKeys.spadeSeventeen,
  CardKeys.diamondSeventeen,
  CardKeys.heartSeventeen,

  CardKeys.clubJack,
  CardKeys.spadeJack,
  CardKeys.diamondJack,
  CardKeys.heartJack,

  CardKeys.clubQueen,
  CardKeys.spadeQueen,
  CardKeys.diamondQueen,
  CardKeys.heartQueen,

  CardKeys.clubKing,
  CardKeys.spadeKing,
  CardKeys.diamondKing,
  CardKeys.heartKing,

  CardKeys.clubAce,
  CardKeys.spadeAce,
  CardKeys.diamondAce,
  CardKeys.heartAce,

  CardKeys.joker,
];

List<String> examplePlayers = [
  "mikePC0",
  "mikePC1",
  "mikePC2",
  "mikePC3"
];

Map<int, Color> colorCodes = {
  50: const Color.fromRGBO(147, 205, 72, .1),
  100: const Color.fromRGBO(147, 205, 72, .2),
  200: const Color.fromRGBO(147, 205, 72, .3),
  300: const Color.fromRGBO(147, 205, 72, .4),
  400: const Color.fromRGBO(147, 205, 72, .5),
  500: const Color.fromRGBO(147, 205, 72, .6),
  600: const Color.fromRGBO(147, 205, 72, .7),
  700: const Color.fromRGBO(147, 205, 72, .8),
  800: const Color.fromRGBO(147, 205, 72, .9),
  900: const Color.fromRGBO(147, 205, 72, 1),
};

// Returns an int representing the value of the card (04 = 4, Ja = 16)
int parseCardValue(CardWrapper card) {
  String cardValue = card.getValue();
  int result = cardValues[cardValue] as int;
  return result;
}

// Compares 2 cards, taking into account the cardLed & the trump card, and returns which is worth more.
CardWrapper compareCard(CardWrapper card1, CardWrapper card2, CardWrapper cardLed, CardWrapper cardTrump) {
  String card1Suit = card1.getSuit();
  String card1Value = card1.getValue();
  int card1Worth = parseCardValue(card1);
  String card2Suit = card2.getSuit();
  String card2Value = card2.getValue();
  int card2Worth = parseCardValue(card2);

  String cardLedSuit = cardLed.getSuit();
  String cardTrumpSuit = cardTrump.getSuit();

  // is the trump joker?
  if (cardTrumpSuit == "j") {
    // Check if only one of the cards matches suit of cardLed
    if (card1Suit == cardLedSuit && card2Suit != cardLedSuit){
      return card1;
    } else if (card2Suit == cardLedSuit && card1Suit != cardLedSuit){
      return card2;
    }

    // This means the cards are both cardLed. Returns the higher.
    if (card1Worth > card2Worth) {
      return card1;
    } else {
      return card2;
    }
  }

  // is either card a Joker?
  if (card1Value == "Jo") {
    return card1;
  } else if (card2Value == "Jo") {
    return card2;
  }

  // is either card right Bower?
  if (card1Value == "Ja" && card1Suit == cardTrumpSuit) {
    return card1;
  } else if (card2Value == "Ja" && card2Suit == cardTrumpSuit) {
    return card2;
  }

  // Is card1 left Bower?
  if (card1Value == "Ja") {
    if (cardTrumpSuit == "h" && card1Suit == "d") {
      return card1;
    } else if (cardTrumpSuit == "d" && card1Suit == "h") {
      return card1;
    } else if (cardTrumpSuit == "s" && card1Suit == "c") {
      return card1;
    } else if (cardTrumpSuit == "c" && card1Suit == "s") {
      return card1;
    }
  }

  // Is card2 left Bower?
  if (card2Value == "Ja") {
    if (cardTrumpSuit == "h" && card2Suit == "d") {
      return card2;
    } else if (cardTrumpSuit == "d" && card2Suit == "h") {
      return card2;
    } else if (cardTrumpSuit == "s" && card2Suit == "c") {
      return card2;
    } else if (cardTrumpSuit == "c" && card2Suit == "s") {
      return card2;
    }
  }

  // Check if only one of the cards is a trump
  if (card1Suit == cardTrumpSuit && card2Suit != cardTrumpSuit){
    return card1;
  } else if (card2Suit == cardTrumpSuit && card1Suit != cardTrumpSuit){
    return card2;
  }

  // Check if only one of the cards matches suit of cardLed
  if (card1Suit == cardLedSuit && card2Suit != cardLedSuit){
    return card1;
  } else if (card2Suit == cardLedSuit && card1Suit != cardLedSuit){
    return card2;
  }

  // If we reach here, then the cards must be the same suit and non-bower.
  // Checks which is higher and returns it.
  if (card1Worth > card2Worth) {
    return card1;
  } else {
    return card2;
  }
}

// Compares two cards without taking into account the trump or cardLed, and returns the higher.
CardWrapper basicCompare(CardWrapper card1, CardWrapper card2) {
  String card1Suit = card1.getSuit();
  int card1Worth = parseCardValue(card1);
  String card2Suit = card2.getSuit();
  int card2Worth = parseCardValue(card2);

  if (card1Worth > card2Worth) {
    return card1;
  } else if (card2Worth > card1Worth) {
    return card2;
  } else {
    // This only occurs if they are the same value (must have different suit).
    if (card1Suit == "h") {
      return card1;
    } else if (card2Suit == "h") {
      return card2;
    } else if (card1Suit == "d") {
      return card1;
    } else if (card2Suit == "d") {
      return card2;
    } else if (card1Suit == "s") {
      return card1;
    } else if (card2Suit == "s") {
      return card2;
    } else {
      // Shouldn't be possible
      return card1;
    }
  }
}

Map<String, int> sortOrder(Map<String, dynamic> playerCards) {
  Map<String, int> sorted = {};
  int playerCount = playerCards.length;
  List<CardWrapper> cards = [];
  List<String> sortedNicknames = playerCards.keys.toList(growable:false)
    ..sort((k1, k2) => orderSelectionCalc(playerCards[k1]).compareTo(orderSelectionCalc(playerCards[k2])));

  playerCards.forEach((player, cardStr) {
    CardWrapper card = CardWrapper(cardStr: cardStr);
    cards.add(card);
  });

  for (int i = 0; i < cards.length; i++) {
    int orderNum = (i-1) % playerCount;
    sorted[sortedNicknames[i]] = orderNum;
  }
  return sorted;
}

int orderSelectionCalc(String cardStr) {
  return -orderSelectionValues[cardStr];
}

Deck provideDeck(int numOfPlayers, int lengthOfGame) {
  Deck40 deck40 = Deck40();
  Deck50 deck50 = Deck50();
  Deck60 deck60 = Deck60();
  Deck63 deck63 = Deck63();
  Deck64 deck64 = Deck64();
  Deck70 deck70 = Deck70();
  Deck72 deck72 = Deck72();
  Deck80 deck80 = Deck80();
  deck40.resetDeck();
  deck50.resetDeck();
  deck60.resetDeck();
  deck63.resetDeck();
  deck64.resetDeck();
  deck70.resetDeck();
  deck72.resetDeck();
  deck80.resetDeck();

  if (numOfPlayers == 4) {
    return deck40;
  } else if (numOfPlayers == 5) {
    return deck50;
  } else if (numOfPlayers == 6) {
    return deck60;
  } else if (numOfPlayers == 7 && lengthOfGame == 18) {
    return deck63;
  } else if (numOfPlayers == 7 && lengthOfGame == 20) {
    return deck70;
  } else if (numOfPlayers == 8 && lengthOfGame == 16) {
    return deck64;
  } else if (numOfPlayers == 8 && lengthOfGame == 18) {
    return deck72;
  } else if (numOfPlayers == 8 && lengthOfGame == 20) {
    return deck80;
  } else if (numOfPlayers == 9 && lengthOfGame == 14) {
    return deck63;
  } else if (numOfPlayers == 9 && lengthOfGame == 16) {
    return deck72;
  } else if (numOfPlayers == 10 && lengthOfGame == 12) {
    return deck60;
  } else if (numOfPlayers == 10 && lengthOfGame == 14) {
    return deck70;
  }
  // Shouldn't be possible to reach this point
  return deck60;
}

int calculateSizeOfHand(int totalRounds, int currentRound) {
  int result;
  int halfRounds = (totalRounds ~/ 2);

  if (currentRound < halfRounds) {
    result = halfRounds - currentRound;
  } else {
    result = (currentRound - halfRounds) + 1;
  }

  return result;
}

int cardOrderHand(CardWrapper card, String trumpSuit) {
  // Spade --> Diamond --> Club --> Heart
  // Diamond --> Club --> Heart --> Spade
  // Club --> Heart --> Spade --> Diamond
  // Heart --> Spade --> Diamond --> Club

  int sValue, dValue, cValue, hValue;
  String cardSuit = card.getSuit();
  String cardValueStr = card.getValue();
  int cardValue = cardValues[cardValueStr];

  if (card.getString() == CardKeys.joker) {
    return 250;
  }

  // if the card is right or left bower, change the value.
  if (cardValueStr == "Ja") {
    if (trumpSuit == "s" && cardSuit == "s") {
      return 230;
    } else if (trumpSuit == "s" && cardSuit == "c") {
      return 220;
    } else if (trumpSuit == "d" && cardSuit == "d") {
      return 230;
    } else if (trumpSuit == "d" && cardSuit == "h") {
      return 220;
    } else if (trumpSuit == "c" && cardSuit == "c") {
      return 230;
    } else if (trumpSuit == "c" && cardSuit == "s") {
      return 220;
    } else if (trumpSuit == "h" && cardSuit == "h") {
      return 230;
    } else if (trumpSuit == "h" && cardSuit == "d") {
      return 220;
    }
  }

  // Set the relative values for each suit depending on trump.
  if (trumpSuit == "s") {
    sValue = 0;
    dValue = 1000;
    cValue = 2000;
    hValue = 3000;
  } else if (trumpSuit == "d") {
    sValue = 3000;
    dValue = 0;
    cValue = 1000;
    hValue = 2000;
  } else if (trumpSuit == "c") {
    sValue = 2000;
    dValue = 3000;
    cValue = 0;
    hValue = 1000;
  } else if (trumpSuit == "h") {
    sValue = 1000;
    dValue = 2000;
    cValue = 3000;
    hValue = 0;
  } else {
    sValue = 0;
    dValue = 1000;
    cValue = 2000;
    hValue = 3000;
  }

  // Assign the correct suit value.
  if (cardSuit == "s") {
    return cardValue + sValue;
  } else if (cardSuit == "d") {
    return cardValue + dValue;
  } else  if (cardSuit == "c") {
    return cardValue + cValue;
  } else if (cardSuit == "h") {
    return cardValue + hValue;
  } else {
    return -1;
  }
}

String gameRules(int numOfPlayers, int lengthOfGame) {
  if (numOfPlayers == 4) {
    return "41 Card Deck. Standard deck with all 2's, 3's and 4's removed.";
  } else if (numOfPlayers == 5) {
    return "51 Card Deck. Standard deck with black 2's removed.";
  } else if (numOfPlayers == 6) {
    return "61 Card Deck. 500 deck with black 2's removed.";
  } else if (numOfPlayers == 7 && lengthOfGame == 18) {
    return "64 Card Deck. 2 -> 13  deck, with 13 of Clubs removed.";
  } else if (numOfPlayers == 7 && lengthOfGame == 20) {
    return "71 Card Deck. 2 -> 15  deck, with black 15's removed.";
  } else if (numOfPlayers == 8 && lengthOfGame == 16) {
    return "65 Card Deck. 2 -> 13  deck.";
  } else if (numOfPlayers == 8 && lengthOfGame == 18) {
    return "73 Card Deck. 2 -> 15  deck.";
  } else if (numOfPlayers == 8 && lengthOfGame == 20) {
    return "81 Card Deck. 2 -> 17  deck.";
  } else if (numOfPlayers == 9 && lengthOfGame == 14) {
    return "64 Card Deck. 2 -> 13  deck.";
  } else if (numOfPlayers == 9 && lengthOfGame == 16) {
    return "73 Card Deck. 2 -> 15  deck.";
  } else if (numOfPlayers == 10 && lengthOfGame == 12) {
    return "61 Card Deck. 500 deck with black 2's removed.";
  } else if (numOfPlayers == 10 && lengthOfGame == 14) {
    return "71 Card Deck. 2 -> 15  deck, with black 15's removed.";
  } else {
    return "You broke it. Should probably tell Mike.";
  }
}

/// These are populateExample() functions

List<Map> exampleCalls(int totalRounds) {
  List<Map> calls = [];
  for (int i = 0; i < totalRounds; i++) {
    var round = {};
    calls.add(round);
  }
  return calls;
}

List<Map> exampleCallsWon(int totalRounds) {
  List<Map> callsWon = [];
  for (int i = 0; i < totalRounds; i++) {
    var round = {};
    callsWon.add(round);
  }
  return callsWon;
}

Map examplePileStuff() {
  Map pileStuff = {};
  Map pile = {};
  pileStuff.putIfAbsent("pile", () => pile);
  pileStuff.putIfAbsent("highestCard", () => "");
  pileStuff.putIfAbsent("cardLed", () => "");
  pileStuff.putIfAbsent("whoLedOrder", () => 0);

  return pileStuff;
}

List<Map> exampleRounds(int totalRounds) {
  List<Map> rounds = [];

  for (int i = 0; i < totalRounds; i++) {
    Map round = {};
    rounds.add(round);
  }

  return rounds;
}

Map exampleHands(int totalRounds, List<String> players) {
  Map hands = {};

  for (String player in players) {
    List<String> hand = [
      CardKeys.joker,
    ];
    hands.putIfAbsent(player, () => hand);
  }

  return hands;
}

String findPlace(int place) {
  String placeStr;

  if (place == 0) {
    placeStr = "1st";
  } else if (place == 1) {
    placeStr = "2nd";
  } else if (place == 2) {
    placeStr = "3rd";
  } else if (place == 3) {
    placeStr = "4th";
  } else if (place == 4) {
    placeStr = "5th";
  } else if (place == 5) {
    placeStr = "6th";
  } else if (place == 6) {
    placeStr = "7th";
  } else if (place == 7) {
    placeStr = "8th";
  } else if (place == 8) {
    placeStr = "9th";
  } else if (place == 9) {
    placeStr = "10th";
  } else {
    placeStr = "error";
  }

    return placeStr;
}

String getCredits() {
  String credits = "";
  credits = credits + "Thanks for playing Magic 10 to 1!\n";


  return credits;
}

Future<bool> checkLatestVersion() async {
  int localVersionNumber = GamesKeys.version;
  int minLocalVersion = 9999;

  DocumentReference ref = FirebaseFirestore.instance.collection('Keys').doc('version');
  await ref.get().then((snapshot) {
    minLocalVersion = snapshot.get('minLocalVersion');
  });

  if (localVersionNumber >= minLocalVersion) {
    return true;
  } else {
    return false;
  }
}

String timestampToString(Timestamp timestamp) {
  String result = "";
  DateTime datetime = timestamp.toDate();

  int month = datetime.month;
  int day = datetime.day;
  int hour = datetime.hour;
  int minute = datetime.minute;
  int year = datetime.year % 100;

  String minuteString = "";

  if (minute < 10) {
    minuteString = minuteString + "0";
  }

  minuteString = minuteString + minute.toString();


  if (hour == 0) {
    result = result + "12:" + minuteString + "pm";
  } else if (hour < 13) {
    result = result + hour.toString() + ":" + minuteString + "am";
  } else {
    result = result + (hour - 12).toString() + ":" + minuteString + "pm";
  }

  result = result + " - ";

  result = result + day.toString();
  result = result + "/";
  result = result + month.toString();
  result = result + "/";
  result = result + year.toString();

  return result;
}

String findGameLength(int totalRounds) {
  if (totalRounds == 20) {
    return "10 to 1";
  } else if (totalRounds == 18) {
    return "9 to 1";
  } else if (totalRounds == 16) {
    return "8 to 1";
  } else if (totalRounds == 14) {
    return "7 to 1";
  } else if (totalRounds == 12) {
    return "6 to 1";
  } else {
    return "10 to 1";
  }
}

bool checkBirthday (String nickname) {
  DateTime now = DateTime.now();
  int month = now.month;
  int day = now.day;

  if (nickname == "Magician" && month == 8 && day == 4) {
    return true;
  } else if (nickname == "Dàwèi" && month == 8 && day == 30) {
    return true;
  } else if (nickname == "Matchstick" && month == 5 && day == 25) {
    return true;
  } else if (nickname == "Tank" && month == 12 && day == 26) {
    return true;
  } else if (nickname == "Stretch" && month == 2 && day == 6) {
    return true;
  } else if (nickname == "Frog" && month == 7 && day == 2) {
    return true;
  } else if (nickname == "Liam" && month == 6 && day == 12) {
    return true;
  } else {
  return false;
  }
}

Future<void> renameAccount(String oldNickname, String newNickname) async {
  DocumentReference oldNicknameRef = FirebaseFirestore.instance.collection('players').doc(oldNickname);

  int highestScore = 0;
  Map highestScores = {};
  String iv = "";
  int losses = 0;
  int lowestScore = 0;
  Map lowestScores = {};
  String password = "fail";
  int twoHundredCount = 0;
  int wins = 0;

  // Load all the info from the old nickname
  await oldNicknameRef.get().then((snapshot) {
    highestScore = snapshot.get('highestScore');
    highestScores = Map<String, dynamic>.from(snapshot.get('highestScores'));
    iv = snapshot.get('iv');
    losses = snapshot.get('losses');
    lowestScore = snapshot.get('lowestScore');
    lowestScores = Map<String, dynamic>.from(snapshot.get('lowestScores'));
    password = snapshot.get('password');
    twoHundredCount = snapshot.get('twoHundredCount');
    wins = snapshot.get('wins');
  });

  // Create a new player
  await FirebaseFirestore.instance.collection('players').doc(newNickname).set({
    'highestScore': highestScore,
    'highestScores': highestScores,
    'iv': iv,
    'losses': losses,
    'lowestScore': lowestScore,
    'lowestScores': lowestScores,
    'password': password,
    'twoHundredCount': twoHundredCount,
    'wins': wins,
  });

  // Remove the old player (only if the load worked correctly)
  if (password != "fail") {
    await FirebaseFirestore.instance.collection('players').doc(newNickname).delete();
  } else {
    debugPrint("Load didn't work properly");
  }

  // Set the cloud function going to replace all the names in existing games
  await FirebaseFirestore.instance.collection('rename').doc(oldNickname).set({
    'oldNickname' : oldNickname,
    'newNickname' : newNickname,
    'updated' : false,
    'timeRequested' : Timestamp.fromDate(DateTime.now()),
  });
}

int calculateTimeDifference(Timestamp turnTime) {
  DateTime turnDate = turnTime.toDate();
  DateTime now = DateTime.now();

  Duration diff = now.difference(turnDate);

  return diff.inMinutes;
}

String calculateTurnTime(Timestamp turnTime) {
  int fullTime = calculateTimeDifference(turnTime);
  int minutes = fullTime % 60;
  int hours = fullTime ~/ 60;

  if (fullTime < 60) {
    return "$fullTime" + "m";
  } else {
    return "$hours" + "h $minutes" + "m";
  }
}