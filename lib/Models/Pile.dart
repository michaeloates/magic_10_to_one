import 'package:magic_10_to_one/Models/CardWrapper.dart';
import 'package:magic_10_to_one/Models/GameUtilities.dart';

class Pile {
  Map<CardWrapper, dynamic> currentPile = {};
  CardWrapper highestCard = CardWrapper(cardStr: "n00");
  CardWrapper cardLed = CardWrapper(cardStr: "n00");
  int whoLedOrder = -1;
  CardWrapper cardTrump = CardWrapper(cardStr: "n00");

  Pile();

  void addCardToPile(CardWrapper card, String playerName, int playerOrder) {
    currentPile[card] = playerName;

    if (highestCard.getString() == "n00") {
      highestCard = card;
    } else {
      highestCard = compareCard(highestCard, card, cardLed, cardTrump);
    }

    if (cardLed.getString() == "n00") {
      cardLed = card;
      whoLedOrder = playerOrder;
    }
  }

  void setPile(Map pileStrings) {
    clearPile();
    pileStrings.forEach((cardStr, name) {
      CardWrapper card = CardWrapper(cardStr: cardStr);
      currentPile[card] = name;
    });
  }

  void setCardLed(CardWrapper card) {
    cardLed = card;
  }

  void setTrump(CardWrapper card) {
    cardTrump = card;
  }

  void setHighestCard(CardWrapper card) {
    highestCard = card;
  }

  void setWhoLedOrder(int playerOrder) {
    whoLedOrder = playerOrder;
  }

  void removeCardFromPile(CardWrapper card) {
    currentPile.remove(card);
  }

  Map viewCurrentPile() {
    return currentPile;
  }

  Map<String, dynamic> getPileStrings() {
    Map<String, dynamic> pileStuff = {};

    currentPile.forEach((key, value) {
      pileStuff[key.getString()] = value;
    });

    return pileStuff;
  }

  Map exportPileStuff() {
    Map pileStuff = {};
    pileStuff["pile"] = getPileStrings();
    pileStuff["highestCard"] = highestCard.getString();
    pileStuff["cardLed"] = cardLed.getString();
    pileStuff["whoLedOrder"] = whoLedOrder;

    return pileStuff;
  }

  CardWrapper viewHighestCard() {
    return highestCard;
  }

  String viewHighestCardOwner() {
    String ownerStr = "";

    currentPile.forEach((card, owner) {
      if (card.getString() == highestCard.getString()) {
        ownerStr = owner;
      }
    });

    return ownerStr;
  }

  CardWrapper getTrump() {
    return cardTrump;
  }

  CardWrapper getCardLed() {
    return cardLed;
  }

  int getWhoLedOrder() {
    return whoLedOrder;
  }

  void clearPile() {
    currentPile = {};
    highestCard = CardWrapper(cardStr: "n00");
    cardLed = CardWrapper(cardStr: "n00");
  }

  void resetPile() {
    currentPile = {};
    highestCard = CardWrapper(cardStr: "n00");
    cardLed = CardWrapper(cardStr: "n00");
    cardTrump = CardWrapper(cardStr: "n00");
  }
}