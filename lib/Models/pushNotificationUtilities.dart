import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

Future<void> sendNotification(String fcmToken, String title, String body, String threadID, String sound, int turnCount, String serverName, int badgeNumber) async {
  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  try {
    await _firestore.collection('notifications').doc(fcmToken).set({
      'body' : body,
      'notificationSent' : false,
      'sound' : sound,
      'threadID' : threadID,
      'title' : title,
      'token' : fcmToken,
      'timeSent' : Timestamp.fromDate(DateTime.now()),
      'turnCount' : turnCount,
      'serverName' : serverName,
      'reminder1Sent' : false,
      'reminder2Sent' : false,
      'badgeNumber' : badgeNumber,
    });
  } catch (e) {
    debugPrint(e.toString());
  }
}