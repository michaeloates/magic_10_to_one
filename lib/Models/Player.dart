import 'package:magic_10_to_one/Models/Hand.dart';

import 'CardWrapper.dart';

class Player {

  Hand hand = Hand();
  int currentScore = 0;
  bool hexed = false;
  int playerOrderNumber;
  String nickname;
  bool twoHundredStatus = false;

  Player({
    required this.playerOrderNumber,
    required this.nickname
  });

  void addCardListToPlayer(List<CardWrapper> hand) {
    for (CardWrapper card in hand) {
      this.hand.addCardToHand(card);
    }
  }

  void addCardToPlayer(CardWrapper card) {
    hand.addCardToHand(card);
  }

  void removeCardFromPlayer(CardWrapper card) {
    hand.removeCardFromHand(card);
  }

  void addScore(int score){
    currentScore += score;
  }

  int getScore() {
    return currentScore;
  }

  List<String> getHandStrings(String trump) {
    return hand.getHandStrings(trump);
  }

  List<CardWrapper> getHandCards(String trumpSuit) {
    return hand.getHandCards(trumpSuit);
  }

  Hand getHand() {
    return hand;
  }

  void setHexStatus(bool hex) {
    hexed = hex;
  }

  bool getHexStatus() {
    return hexed;
  }

  void setPlayerOrderNumber(int orderNumber) {
    playerOrderNumber = orderNumber;
  }

  void setNickname(String nickname) {
    this.nickname = nickname;
  }

  String getNickname() {
    return nickname;
  }

  void setCurrentScore(int? score) {
    currentScore = score ?? 0;
  }

  bool getTwoHundredStatus() {
    return twoHundredStatus;
  }

  void setTwoHundredStatus(bool twoHundredStatus) {
    this.twoHundredStatus = twoHundredStatus;
  }

}