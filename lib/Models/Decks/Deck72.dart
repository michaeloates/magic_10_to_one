import 'package:magic_10_to_one/Models/Decks/Deck.dart';
import 'package:magic_10_to_one/enums/cards.dart';

import '../CardWrapper.dart';

class Deck72 extends Deck {

  static const deckSize = 73;

  List<String> deck72 = [
    CardKeys.joker,

    CardKeys.diamondAce,
    CardKeys.diamondTwo,
    CardKeys.diamondThree,
    CardKeys.diamondFour,
    CardKeys.diamondFive,
    CardKeys.diamondSix,
    CardKeys.diamondSeven,
    CardKeys.diamondEight,
    CardKeys.diamondNine,
    CardKeys.diamondTen,
    CardKeys.diamondEleven,
    CardKeys.diamondTwelve,
    CardKeys.diamondThirteen,
    CardKeys.diamondFourteen,
    CardKeys.diamondFifteen,
    CardKeys.diamondJack,
    CardKeys.diamondQueen,
    CardKeys.diamondKing,

    CardKeys.heartAce,
    CardKeys.heartTwo,
    CardKeys.heartThree,
    CardKeys.heartFour,
    CardKeys.heartFive,
    CardKeys.heartSix,
    CardKeys.heartSeven,
    CardKeys.heartEight,
    CardKeys.heartNine,
    CardKeys.heartTen,
    CardKeys.heartEleven,
    CardKeys.heartTwelve,
    CardKeys.heartThirteen,
    CardKeys.heartFourteen,
    CardKeys.heartFifteen,
    CardKeys.heartJack,
    CardKeys.heartQueen,
    CardKeys.heartKing,

    CardKeys.spadeAce,
    CardKeys.spadeTwo,
    CardKeys.spadeThree,
    CardKeys.spadeFour,
    CardKeys.spadeFive,
    CardKeys.spadeSix,
    CardKeys.spadeSeven,
    CardKeys.spadeEight,
    CardKeys.spadeNine,
    CardKeys.spadeTen,
    CardKeys.spadeEleven,
    CardKeys.spadeTwelve,
    CardKeys.spadeThirteen,
    CardKeys.spadeFourteen,
    CardKeys.spadeFifteen,
    CardKeys.spadeJack,
    CardKeys.spadeQueen,
    CardKeys.spadeKing,

    CardKeys.clubAce,
    CardKeys.clubTwo,
    CardKeys.clubThree,
    CardKeys.clubFour,
    CardKeys.clubFive,
    CardKeys.clubSix,
    CardKeys.clubSeven,
    CardKeys.clubEight,
    CardKeys.clubNine,
    CardKeys.clubTen,
    CardKeys.clubEleven,
    CardKeys.clubTwelve,
    CardKeys.clubThirteen,
    CardKeys.clubFourteen,
    CardKeys.clubFifteen,
    CardKeys.clubJack,
    CardKeys.clubQueen,
    CardKeys.clubKing,
  ];

  Deck72();

  @override
  void populateDeck() {
    for (int i = 0; i < deckSize; i++) {
      CardWrapper addCard = CardWrapper(cardStr: deck72[i]);
      cardsInDeck.add(addCard);
    }
  }
}