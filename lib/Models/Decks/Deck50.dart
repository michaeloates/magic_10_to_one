import 'package:magic_10_to_one/Models/Decks/Deck.dart';
import 'package:magic_10_to_one/enums/cards.dart';

import '../CardWrapper.dart';

class Deck50 extends Deck {

  static const deckSize = 51;

  List<String> deck50 = [
    CardKeys.joker,

    CardKeys.diamondAce,
    CardKeys.diamondTwo,
    CardKeys.diamondThree,
    CardKeys.diamondFour,
    CardKeys.diamondFive,
    CardKeys.diamondSix,
    CardKeys.diamondSeven,
    CardKeys.diamondEight,
    CardKeys.diamondNine,
    CardKeys.diamondTen,
    CardKeys.diamondJack,
    CardKeys.diamondQueen,
    CardKeys.diamondKing,

    CardKeys.heartAce,
    CardKeys.heartTwo,
    CardKeys.heartThree,
    CardKeys.heartFour,
    CardKeys.heartFive,
    CardKeys.heartSix,
    CardKeys.heartSeven,
    CardKeys.heartEight,
    CardKeys.heartNine,
    CardKeys.heartTen,
    CardKeys.heartJack,
    CardKeys.heartQueen,
    CardKeys.heartKing,

    CardKeys.spadeAce,
    CardKeys.spadeThree,
    CardKeys.spadeFour,
    CardKeys.spadeFive,
    CardKeys.spadeSix,
    CardKeys.spadeSeven,
    CardKeys.spadeEight,
    CardKeys.spadeNine,
    CardKeys.spadeTen,
    CardKeys.spadeJack,
    CardKeys.spadeQueen,
    CardKeys.spadeKing,

    CardKeys.clubAce,
    CardKeys.clubThree,
    CardKeys.clubFour,
    CardKeys.clubFive,
    CardKeys.clubSix,
    CardKeys.clubSeven,
    CardKeys.clubEight,
    CardKeys.clubNine,
    CardKeys.clubTen,
    CardKeys.clubJack,
    CardKeys.clubQueen,
    CardKeys.clubKing,
  ];

  Deck50();

  @override
  void populateDeck() {
    for (int i = 0; i < deckSize; i++) {
      CardWrapper addCard = CardWrapper(cardStr: deck50[i]);
      cardsInDeck.add(addCard);
    }
  }
}