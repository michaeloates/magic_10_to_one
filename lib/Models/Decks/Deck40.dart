import 'package:magic_10_to_one/Models/Decks/Deck.dart';
import 'package:magic_10_to_one/enums/cards.dart';

import '../CardWrapper.dart';

class Deck40 extends Deck {

  static const deckSize = 41;

  List<String> deck40 = [
    CardKeys.joker,

    CardKeys.diamondAce,
    CardKeys.diamondFive,
    CardKeys.diamondSix,
    CardKeys.diamondSeven,
    CardKeys.diamondEight,
    CardKeys.diamondNine,
    CardKeys.diamondTen,
    CardKeys.diamondJack,
    CardKeys.diamondQueen,
    CardKeys.diamondKing,

    CardKeys.heartAce,
    CardKeys.heartFive,
    CardKeys.heartSix,
    CardKeys.heartSeven,
    CardKeys.heartEight,
    CardKeys.heartNine,
    CardKeys.heartTen,
    CardKeys.heartJack,
    CardKeys.heartQueen,
    CardKeys.heartKing,

    CardKeys.spadeAce,
    CardKeys.spadeFive,
    CardKeys.spadeSix,
    CardKeys.spadeSeven,
    CardKeys.spadeEight,
    CardKeys.spadeNine,
    CardKeys.spadeTen,
    CardKeys.spadeJack,
    CardKeys.spadeQueen,
    CardKeys.spadeKing,

    CardKeys.clubAce,
    CardKeys.clubFive,
    CardKeys.clubSix,
    CardKeys.clubSeven,
    CardKeys.clubEight,
    CardKeys.clubNine,
    CardKeys.clubTen,
    CardKeys.clubJack,
    CardKeys.clubQueen,
    CardKeys.clubKing,
  ];

  Deck40();

  @override
  void populateDeck() {
    for (int i = 0; i < deckSize; i++) {
      CardWrapper addCard = CardWrapper(cardStr: deck40[i]);
      cardsInDeck.add(addCard);
    }
  }
}