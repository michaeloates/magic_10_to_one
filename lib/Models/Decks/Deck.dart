import '../CardWrapper.dart';

abstract class Deck {
  // var cardsInDeck;

  List<CardWrapper> cardsInDeck = [];

  /*
  Deck([this.cardsInDeck]) {
    cardsInDeck ??= [];
    if (cardsInDeck == []) {
      resetDeck();
    }
  }
   */

  Deck();

  void populateDeck();

  void setDeck(List<CardWrapper> deck) {
    cardsInDeck.clear();
    cardsInDeck.addAll(deck);
  }

  void setDeckStrings(List<String> deckStrings){
    cardsInDeck.clear();
    for (String cardStr in deckStrings) {
      CardWrapper card = CardWrapper(cardStr: cardStr);
      cardsInDeck.add(card);
    }
  }

  List<CardWrapper> getDeck() {
    return cardsInDeck;
  }
  
  List<String> getDeckStrings() {
    List<String> result = [];
    
    for (CardWrapper card in cardsInDeck) {
      result.add(card.getString());
    }

    return result;
  }

  void shuffleDeck() {
    cardsInDeck.shuffle();
  }

  CardWrapper drawCard() {
    CardWrapper card = cardsInDeck.first;
    removeCard(card);
    return card;
  }

  void removeCard(CardWrapper card) {
    cardsInDeck.remove(card);
  }

  void resetDeck() {
    cardsInDeck.clear();
    populateDeck();
    shuffleDeck();
  }
}