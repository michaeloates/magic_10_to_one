import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:magic_10_to_one/Models/Player.dart';
import 'package:magic_10_to_one/Models/pushNotificationUtilities.dart';
import 'package:magic_10_to_one/enums/cards.dart';
import 'package:magic_10_to_one/Models/GameUtilities.dart';
import 'CardWrapper.dart';
import 'Decks/Deck.dart';
import 'Pile.dart';
import 'emailUtilities.dart';
import 'package:collection/collection.dart';

class Game {

  // Fields from/to the database
  bool blindManRound = false;
  List<Map> calls = [];
  List<Map> callsWon = [];
  int currentRound = 0;
  Map currentScores = {"Magician" : 0, "Dawei" : 0, "Frog" : 0, "loading" : 0, "Tank" : 0, "Matchstick" : 0};
  String currentTrump = "n00";
  int currentTurn = 0;
  List<String> deckStrings = [];
  List<String> emailResults =[];
  bool firstCardsDealt = false;
  Map futureCalls = {};
  Map futureCards = {};
  bool gameComplete = false;
  Map hands = {};
  Map hexVotes = {};
  Map hexVoted = {};
  String hostName = "";
  Timestamp lastTurnTime = Timestamp.fromDate(DateTime.now());
  Map notificationTokens = {};
  String playerHexed = "";
  Map pileStuff = {};
  bool placeCards = false; // This represents if we're in the stage for calling or placing cards.
  int playerCount = 6;
  List<String> playerStrings = ["Magician", "Dawei", "Frog", "loading", "Tank", "Matchstick"];
  Map<String, dynamic> playersOrder = {"Magician" : 0, "Dawei" : 1, "Frog" : 2, "loading" : 3, "Tank" : 4, "Matchstick" : 5};
  Map<String, dynamic> previousPileStrings = {};
  List<String> previousPileOrderedStrings = [];
  bool queueFull = true;
  bool realGame = false;
  List<Map> rounds = [];
  bool saveLock = false;
  String serverName = "";
  String serverPassword = "";
  Timestamp timeEnded = Timestamp.fromDate(DateTime.now());
  Timestamp timeStarted = Timestamp.fromDate(DateTime.now());
  int totalRounds = 20;
  int turnCount = 0;
  int turnsPassed = 0;
  List<dynamic> turnTimes = [];
  Map<String, dynamic> twoHundredClub = {"Magician" : false, "Dawei" : false, "Frog" : false, "loading" : false, "Tank" : false, "Matchstick" : false};
  int versionStarted = 000;
  Map<String, dynamic> selectionHandUnknown = {};
  Map<String, dynamic> selectionHandKnown = {};

  // Sub-fields
  Map pileStrings = {};
  CardWrapper highestCard = CardWrapper(cardStr: "n00");
  CardWrapper cardLed = CardWrapper(cardStr: "n00");
  CardWrapper cardTrump = CardWrapper(cardStr: "n00");
  int whoLedOrder = -1;

  // Game objects
  Box profileBox = Hive.box(ProfileKeys.profile);
  Box localHexInfo = Hive.box(ProfileKeys.hexBox);

  late Deck deck;
  List<Player> players = [];
  Pile pile = Pile();
  Pile previousPile = Pile();
  List<CardWrapper> previousPileOrdered = [];
  String playerName = "loading";
  int offsetCounter = 0; // This is used to count the difference between the turnCount from local & server

  Game() {
    populateExample();
    populateGame();
  }

  /// INITIAL SETUP METHODS

  void populateExample() {
    // bool blindManRound = false;
    calls = exampleCalls(totalRounds);
    callsWon = exampleCallsWon(totalRounds);
    // int currentRound = 0;
    // Map currentScores = {};
    // int currentTurn = 0;
    deckStrings = provideDeck(playerCount, totalRounds).getDeckStrings();
    emailResults =["magic.10.to.one@gmail.com"];
    // bool gameComplete = false;
    hands = exampleHands(totalRounds, playerStrings);
    // Map hexVotes = {};
    // String playerHexed = "";
    pileStuff = examplePileStuff();
    // bool placeCards = false; // This represents if we're in the stage for calling or placing cards.
    // int playerCount = 6;
    // playerStrings;
    // Map playersOrder = {};
    // bool queueFull = true;
    // bool realGame = false;
    rounds = exampleRounds(totalRounds);
    // String serverName = "";
    // String serverPassword = "";
    // Timestamp timeEnded = Timestamp.fromDate(DateTime.now());
    // Timestamp timeStarted = Timestamp.fromDate(DateTime.now());
    // int totalRounds = 20;
    // int turnsPassed = 0;
  }

  /*
  This pre-populates all the sub-objects required in the game object.
  - Deck is set based on deckStrings
  - Players is set based on playerStrings, playersOrder, currentScores,
    personHexed, hands
  - Pile is set based on pileStrings, cardLed, cardTrump
   */
  void populateGame() {
    setOrder();
    setDeck();
    populatePlayers();
    setPileFiles();
    populatePile(pileStrings, cardLed, cardTrump, highestCard, whoLedOrder);
    populatePreviousPile(previousPileStrings);

    firstDeal(); // Also sets up callsWon
  }

  void setDeck() {
    deck = provideDeck(playerCount, totalRounds);
    deck.setDeckStrings(deckStrings);
  }

  void populatePlayers() {
    // This adds the correct number of Player objects
    // to the player list, then populates each player with
    // the relevant info from the database.

    players.clear();

    for (int i = 0; i < playerCount; i++) {
      String playerName = playerStrings[i];
      int playerOrderNumber = playersOrder[playerName];
      int playerScore = currentScores[playerName];
      bool isPlayerHexed = playerHexed == playerName;
      bool twoHundredStatus = twoHundredClub[playerName];

      List<dynamic> cardStringDyn = hands[playerName];
      List<String> cardStrings = List<String>.from(cardStringDyn);

      Player player = Player(
          playerOrderNumber: playerOrderNumber, nickname: playerName);

      player.setPlayerOrderNumber(playerOrderNumber);
      player.setCurrentScore(playerScore);
      player.setHexStatus(isPlayerHexed);
      player.setTwoHundredStatus(twoHundredStatus);
      populatePlayerHand(player, cardStrings);

      players.add(player);
    }
  }

  void populatePlayerHand(Player player, List<String> cardStrings) {
    for (String cardString in cardStrings) {
      CardWrapper card = CardWrapper(cardStr: cardString);
      player.addCardToPlayer(card);
    }
  }

  void setPileFiles() {
    pileStrings = pileStuff["pile"];
    // pileStrings = Map<String, dynamic>.from(pileStuff["pile"]);
    highestCard = CardWrapper(cardStr: pileStuff["highestCard"]);
    cardLed = CardWrapper(cardStr: pileStuff["cardLed"]);
    whoLedOrder = pileStuff["whoLedOrder"];
    cardTrump = CardWrapper(cardStr: currentTrump);

    /*
    if (rounds[currentRound].isNotEmpty) {
      cardTrump = CardWrapper(cardStr: rounds[currentRound]["trump"]);
    } else {
      cardTrump = CardWrapper(cardStr: "n00");
    }
     */
  }

  void populatePile(Map pileStrings, CardWrapper cardLed, CardWrapper cardTrump, CardWrapper highestCard, int whoLedOrder) {
    pile.setPile(pileStrings);
    pile.setTrump(cardTrump);
    pile.setCardLed(cardLed);
    pile.setWhoLedOrder(whoLedOrder);
    pile.setHighestCard(highestCard);
  }

  void populatePreviousPile(Map previousPileStrings) {
    previousPile.setPile(previousPileStrings);
    previousPile.setTrump(pile.getTrump());

    previousPileOrdered.clear();
    for (String element in previousPileOrderedStrings) {
      CardWrapper indivCard = CardWrapper(cardStr: element);
      previousPileOrdered.add(indivCard);
    }
  }

  void firstDeal() {
    // Alright. So this used to be run by the dealer, now I'm gonna make it be
    // run by the host.

    if (realGame & !firstCardsDealt & (hostName == playerName)) {
    // if (realGame & !firstCardsDealt & (dealerTurn == myOrder)) {
    // if (realGame & !firstCardsDealt & isItMyTurn()) {

      dealCards();
      firstCardsDealt = true;

      for (Map round in callsWon) {
        for (String player in playerStrings) {
          round[player] = 0;
        }
      }

      String nextPlayer = "";
      playersOrder.forEach((key, value) {
        if (value == currentTurn) {
          nextPlayer = key;
        }
      });
      String notifyToken = notificationTokens[nextPlayer];
      sendNotification(notifyToken, "Magic 10 to 1", "Come back to $serverName, it's your turn!", serverName, NotificationSounds.nextTurn, turnCount, serverName, 1);

      cleanSave(0);
    }
  }

  Future<void> load(AsyncSnapshot<DocumentSnapshot> gameStream) async {
    if (saveLock) {
      return;
    }

    try {
      playerName = profileBox.get(ProfileKeys.profileName, defaultValue: null);
      serverName = gameStream.data!.get('serverName');

      await localLoad();
    } catch(e) {
      debugPrint("$e");
    }

  }

  Future<void> localLoad() async {
    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(serverName);
    offsetCounter = 0;

    try {
      await ref.get().then((snapshot) {
        blindManRound = snapshot.get('blindManRound');
        calls = List<Map<String, dynamic>>.from(snapshot.get('calls'));
        callsWon = List<Map<String, dynamic>>.from(snapshot.get('callsWon'));
        currentRound = snapshot.get('currentRound');
        currentScores = Map.from(snapshot.get('currentScores'));
        currentTrump = snapshot.get('currentTrump');
        currentTurn = snapshot.get('currentTurn');
        deckStrings = List<String>.from(snapshot.get('deck'));
        emailResults = List<String>.from(snapshot.get('emailResults'));
        gameComplete = snapshot.get('gameComplete');
        firstCardsDealt = snapshot.get('firstCardsDealt');

        try {
          futureCalls = snapshot.get('futureCalls');
          futureCards = snapshot.get('futureCards');
        } on StateError catch(e2) {
          debugPrint("$e2");
        }

        hands = Map<String, dynamic>.from(snapshot.get('hands'));
        hexVotes = snapshot.get('hexVotes');
        hexVoted = snapshot.get('hexVoted');
        hostName = snapshot.get('hostName');

        try {
          lastTurnTime = snapshot.get('lastTurnTime');
        } on StateError catch(e2) {
          debugPrint("$e2");
        }

        notificationTokens = snapshot.get('notificationTokens');
        playerHexed = snapshot.get('personHexed');
        pileStuff = Map<String, dynamic>.from(snapshot.get('pileStuff'));
        placeCards = snapshot.get('placeCards');
        playerCount = snapshot.get('playerCount');
        playerStrings = List<String>.from(snapshot.get('players'));
        playersOrder = snapshot.get('playersOrder');
        previousPileStrings = Map<String, dynamic>.from(snapshot.get('previousPile'));
        previousPileOrderedStrings = List<String>.from(snapshot.get('previousPileOrdered'));
        queueFull = snapshot.get('queueFull');
        realGame = snapshot.get('realGame');
        rounds = List<Map<String, dynamic>>.from(snapshot.get('rounds'));
        serverName = snapshot.get('serverName');
        serverPassword = snapshot.get('serverPassword');
        timeEnded = snapshot.get('timeEnded') ?? Timestamp.fromDate(DateTime.now());
        timeStarted = snapshot.get('timeStarted');
        totalRounds = snapshot.get('totalRounds');
        turnCount = snapshot.get('turnCount');

        try {
          turnTimes = snapshot.get('turnTimes');
        } on StateError catch(e2) {
          debugPrint("$e2");
        }

        turnsPassed = snapshot.get('turnsPassed');
        twoHundredClub = snapshot.get('twoHundredClub');
        versionStarted = snapshot.get('versionStarted');
        selectionHandUnknown = snapshot.get('selectionHandUnknown');
        selectionHandKnown = snapshot.get('selectionHandKnown');
      });
    } catch(e) {
      debugPrint("$e");
    }

    populateGame();
  }

  // Returns true if it's my proper turn, false otherwise.
  Future<bool> currentTurnCheck(int offset) async {
    int serverTurn = 0;

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(serverName);
    await ref.get().then((snapshot) {
      serverTurn = snapshot.get('turnCount');
    });


    if (turnCount == (serverTurn + offset)) {
      return true;
    } else {
      debugPrint(turnCount.toString());
      debugPrint(serverTurn.toString());
      debugPrint(offset as String?);
      return false;
    }
  }

  Future<void> save(int offset) async {
    saveLock = true;

    await cleanSave(offset);
    saveLock = false;
  }

  Future<void> cleanSave(int offset) async {
    saveLock = true;
    if (!realGame) {
      return;
    }

    pileStuff = pile.exportPileStuff();
    previousPileStrings = previousPile.getPileStrings();

    // Transfers the ordered cards back into strings.
    previousPileOrderedStrings.clear();
    for (CardWrapper element in previousPileOrdered) {
      previousPileOrderedStrings.add(element.getString());
    }

    List<String> deckSave = deck.getDeckStrings();
    currentTrump = pile.getTrump().getString();

    // deconstruct players, update
    for (Player player in players) {
      String nickname = player.getNickname();
      int score = player.getScore();
      List<String> handStrings = player.getHandStrings(pile.getTrump().getSuit());

      currentScores[nickname] = score;
      hands[nickname] = handStrings;
    }

    // Update the FCM token
    String? notificationToken = "notReady";
    await FirebaseMessaging.instance.getToken().then((value) {
      notificationToken = value;
    });
    notificationTokens[playerName] = notificationToken;

    bool turnCheck = await currentTurnCheck(offset);
    if (!turnCheck) {
      debugPrint("turnCheck Save Failed!");
      await localLoad();
      return;

    } else {
      await FirebaseFirestore.instance.collection('games').doc(serverName).update({
        // 'blindManRound': blindManRound, // Shouldn't change
        'calls': calls,
        'callsWon': callsWon, // Done
        'currentRound': currentRound, // Done
        'currentScores': currentScores, // Done
        'currentTrump': currentTrump,
        'currentTurn': currentTurn, // Done
        'deck': deckSave, // Done
        // emailResults shouldn't change
        'firstCardsDealt' : firstCardsDealt,
        'futureCalls' : futureCalls,
        'futureCards' : futureCards,
        'gameComplete': gameComplete, // Done
        'hands': hands, // Done
        'hexVotes': hexVotes, // Later
        'hexVoted': hexVoted,
        'lastTurnTime' : lastTurnTime,
        'notificationTokens' : notificationTokens,
        'personHexed': playerHexed, // Later
        'pileStuff': pileStuff, // Done
        'placeCards': placeCards,
        // 'playerCount': playerCount, // Shouldn't change?
        // 'players': players, // Shouldn't change?
        'playersOrder': playersOrder, // Only changes at the start for first deal
        'previousPile' : previousPileStrings,
        'previousPileOrdered' : previousPileOrderedStrings,
        // 'queueFull': queueFull, // Shouldn't change?
        // No reason to save realGame.
        'rounds': rounds, // Done
        // No reason to save serverName.
        // 'serverPassword': serverPassword, // Shouldn't change
        'timeEnded': timeEnded, // Done
        // 'timeStarted': timeStarted, // Shouldn't change
        // 'totalRounds': totalRounds, // Shouldn't change
        'turnCount': turnCount,
        'turnTimes' : turnTimes,
        'turnsPassed': turnsPassed, // Done
      });
    }

    saveLock = false;
  }

  Future<void> callSave() async {
    saveLock = true;
    bool turnCheck = await currentTurnCheck(0);
    if (!turnCheck) {
      debugPrint("Call Save Failed!");
      await localLoad();
      return;
    }

    await FirebaseFirestore.instance.collection('games').doc(serverName).update({
      'calls': calls,
    });
    saveLock = false;
  }

  Future<void> cardSave() async {
    saveLock = true;
    bool turnCheck = await currentTurnCheck(0);
    if (!turnCheck) {
      debugPrint("Card Save Failed!");
      await localLoad();
      return;
    }

    pileStuff = pile.exportPileStuff();

    for (Player player in players) {
      String nickname = player.getNickname();
      List<String> handStrings = player.getHandStrings(pile.getTrump().getSuit());
      hands[nickname] = handStrings;
    }

    await FirebaseFirestore.instance.collection('games').doc(serverName).update({
      'pileStuff': pileStuff,
      'hands' : hands,
    });

    saveLock = false;
  }

  Future<void> sendEmailResults() async {
    await sendThoseEmails(emailResults, getFinalScoreboard());
  }

  Future<void> saveScores() async {
    // Go through each player and update their scores.
    List<int> tempScores = [];
    for (Player player in players) {
      tempScores.add(player.getScore());
    }

    for (Player player in players) {
      String playerName = player.getNickname();
      int highestScore = 0;
      int lowestScore = 0;
      int wins = 0;
      int losses = 0;
      int twoHundredCount = 0;
      Map<String, dynamic> highestScores = {};
      Map<String, dynamic> lowestScores = {};
      int localScore = player.getScore();
      int localHigh = tempScores.reduce((curr, next) => curr > next? curr: next);
      bool didPlayerWin = (localScore == localHigh);

      DocumentReference ref = FirebaseFirestore.instance.collection('players').doc(playerName);
      try {
        await ref.get().then((snapshot) {
          losses = snapshot.get('losses');
          lowestScore = snapshot.get('lowestScore');
          wins = snapshot.get('wins');
          highestScore = snapshot.get('highestScore');
          highestScores = snapshot.get('highestScores');
          lowestScores = snapshot.get('lowestScores');
          twoHundredCount = snapshot.get('twoHundredCount');
        });
      } catch (e) {
        debugPrint(e.toString());
      }

      int subLowestScore = lowestScores[totalRounds.toString()];
      int subHighestScore = highestScores[totalRounds.toString()];
      if (didPlayerWin) {
        wins += 1;
      } else {
        losses += 1;
      }

      if (localScore > highestScore) {
        highestScore = localScore;
      }
      if ((localScore < lowestScore) || (lowestScore == 0)) {
        lowestScore = localScore;
      }
      if (localScore > subHighestScore) {
        subHighestScore = localScore;
      }
      if (localScore < subLowestScore || (subLowestScore == 0)) {
        subLowestScore = localScore;
      }

      // Calculates if twoHundredClub should be applied
      if (localScore >= 200) {
        twoHundredCount += 1;
      } else if (totalRounds == 18 && localScore >= 180) {
        twoHundredCount += 1;
      } else if (totalRounds == 16 && localScore >= 160) {
        twoHundredCount += 1;
      }

      lowestScores[totalRounds.toString()] = subLowestScore;
      highestScores[totalRounds.toString()] = subHighestScore;

      await ref.update({
        "losses" : losses,
        "lowestScore" : lowestScore,
        "wins" : wins,
        "highestScore" : highestScore,
        "lowestScores" : lowestScores,
        "highestScores" : highestScores,
        "twoHundredCount" : twoHundredCount,
      });
    }
  }

  /// GAME LOGIC METHODS

  /*
  This one is gonna be fun. We need to increment turnsPassed (unless
  turnsPassed == playerCount, in which case we call endPile). We also need to
  pass the currentTurn to the next player (assuming it's their go).

  over.

  If the round is over, then the turnToDeal should be incremented
  (modulo playerCount). After this, the currentTurn should be set to that player.

  recursionCheck allows us to check if we're in the top-level instance of this function.
   */
  Future<void> nextTurn(bool recursionCheck) async {
    if (!isItMyTurn() && !recursionCheck) {
      return;
    }

    turnsPassed++;
    turnCount++;


    // If it's just the next player's turn.
    if (turnsPassed < playerCount) {
      currentTurn = (currentTurn + 1) % playerCount;
    }

    // Is it just the end of calling?
    else if (!placeCards) {
      placeCards = true;
      turnsPassed = 0;
      currentTurn = (currentTurn + 1) % playerCount;
      futureCalls = {};
    }

    // Is the round over?
    else if (isRoundOver()) {
      handWinner();
      roundEndPoints();

      transferPile();
      resetPile();
      resetDeck();

      futureCalls = {};
      futureCards = {};

      placeCards = false;
      turnsPassed = 0;

      // is the game over?
      if (currentRound == (totalRounds - 1)) {
        timeEnded = Timestamp.fromDate(DateTime.now());
        await sendEmailResults();
        await saveScores();
        gameComplete = true;
        await save(offsetCounter);
        return;
      }

      currentRound++;

      dealCards();
      currentTurn = currentRound % playerCount;
    }

    // or is just the hand over?
    else if (turnsPassed == playerCount && !isRoundOver()) {
      handWinner();
      endPile();
      turnsPassed = 0;
      futureCards = {};
    }

    // Here we check & play the futureTurns
    playFutureCalls(findCurrentPlayerName());
    playFutureCards(findCurrentPlayerName());

    if (!recursionCheck && !gameComplete) {
      notifyCurrentPlayer(findCurrentPlayerName());
      lastTurnTime = Timestamp.fromDate(DateTime.now());
      turnTimes.add(lastTurnTime);
      await save(offsetCounter + 1);
      offsetCounter = 0;
    }
  }

  void playFutureCalls (String nextPlayer) {
    // Checks if we're in the make calls phase
    if (placeCards) {
      return;
    }

    bool removeCall = false;
    bool callMade = false;
    String keyName = "";

    futureCalls.forEach((key, value) {
      if (nextPlayer == key) {
        removeCall = true;
        keyName = key;

        // Check if call can be made
        if (value == cantGo()) {
          notifyCurrentPlayer(nextPlayer);
          return;
        }

        // Make their call
        callNum(value);
        callMade = true;
        notifyQueuedCallPlayer(nextPlayer);
      }
    });

    if (removeCall) {
      // remove the call from futureCalls
      futureCalls.remove(keyName);
    }

    if (callMade) {
      // do all the nextTurn() stuff, recursively
      offsetCounter += 1;
      nextTurn(true);
    }

  }

  void playFutureCards (String nextPlayer) {
    // Checks if we're in the place cards phase
    if (!placeCards) {
      return;
    }

    bool removeCard = false;
    bool cardPlaced = false;
    String keyName = "";

    futureCards.forEach((key, value) {
      if (nextPlayer == key) {
        removeCard = true;
        keyName = key;
        CardWrapper futureCard = CardWrapper(cardStr: value);

        // If the card can be played TODO Check if card is in hand
        if (canIPlace(futureCard)) {
          cardPlaced = true;
          offsetCounter += 1;
          placeCard(futureCard);
          notifyQueuedCardPlayer(nextPlayer);
        } else {
          // Do nothing
        }
      }
    });

    if (removeCard) {
      // remove the card from futureCards
      futureCards.remove(keyName);
    }

    if (cardPlaced) {
      // do all the nextTurn() stuff, recursively
      nextTurn(true);
    }

  }

  void notifyCurrentPlayer(String nextPlayer) {
    String notifyToken = notificationTokens[nextPlayer];
    sendNotification(notifyToken, "Magic 10 to 1", "Come back to $serverName, it's your turn!", serverName, NotificationSounds.nextTurn, turnCount, serverName, 1);
  }

  void notifyQueuedCardPlayer(String nextPlayer) {
    String notifyToken = notificationTokens[nextPlayer];
    sendNotification(notifyToken, "Magic 10 to 1", "Your queued card was played", serverName, NotificationSounds.nextTurn, turnCount, serverName, 0);
  }

  void notifyQueuedCallPlayer(String nextPlayer) {
    String notifyToken = notificationTokens[nextPlayer];
    sendNotification(notifyToken, "Magic 10 to 1", "Your queued call was made", serverName, NotificationSounds.nextTurn, turnCount, serverName, 0);
  }

  String findCurrentPlayerName() {
    String nextPlayer = "";
    playersOrder.forEach((key, value) {
      if (value == currentTurn) {
        nextPlayer = key;
      }
    });

    return nextPlayer;
  }

  bool isRoundOver() {
    bool answer = true;

    for (Player player in players) {
      if (player.getHand().getHandCards("c").isNotEmpty) {
        answer = false;
      }
    }

    return answer;
  }

  void setOrder() {
    // if it's a real game, and the first dealt has not occurred yet, then set the player order
    // note: all players will actually do this until the first cards are dealt, this is fine as it's going to be
    // the same order no matter what anyway
    if (realGame & !firstCardsDealt) {
      playersOrder = sortOrder(selectionHandKnown);
    }
  }

  /*
  This takes the deck, and deals out the correct number of cards
  to each player.
   */
  void dealCards() {
    int handSize = calculateSizeOfHand(totalRounds, currentRound);

    for (Player player in players) {
      if (player.getHand().getHandStrings("c").isNotEmpty) {
        return;
      }

      for (int i = 0; i < handSize; i++) {
        CardWrapper card = deck.drawCard();
        player.hand.addCardToHand(card);
      }
    }

    dealTrump();
  }

  // Finds and sets the trump from the deck
  void dealTrump() {
    CardWrapper trump = deck.drawCard();
    // rounds[currentRound]["trump"] = trump.getString(); // No more.
    pile.setTrump(trump);
    currentTrump = trump.getString();
  }

  CardWrapper getCardLed() {
    return pile.getCardLed();
  }

  // Current player makes their call
  void callNum(int call) {
    calls[currentRound][findCurrentPlayerName()] = call;
    // Then we should call nextTurn()
  }

  // If you are the final call, returns what you can't call. If you can call
  // anything or you aren't the final call, returns -1.
  int cantGo() {
    if ((turnsPassed == (playerCount - 1))) {
      int totalCalls = 0;
      int maxCall = calculateSizeOfHand(totalRounds, currentRound);

      for (int call in calls[currentRound].values) {
        totalCalls += call;
      }

      if (totalCalls > maxCall) {
        return -1;
      } else {
        return (maxCall - totalCalls);
      }

    } else {
      return -1;
    }
  }

  // Current player places a card from their hand. returns true if successful
  bool placeCard(CardWrapper card) {
    for (Player player in players) {
      // if the player exists in firebase
      if (player.nickname == findCurrentPlayerName()) {
        // if their hand on actually contains the card
        for(CardWrapper hCard in player.hand.getHandCards("asdf")) {
          if (hCard.cardStr == card.cardStr) {
            player.hand.removeCardFromHand(card);
            pile.addCardToPile(card, player.nickname, currentTurn);
            return true;
          }
        }
      }
    }
    return false;
  }

  /*
  This function finds the winner of the pile, clears the pile, changes the
  order accordingly.
   */
  void endPile() {
    transferPile();

    String winner = findHighestCardOwner();
    int winnerNum = playersOrder[winner];
    pile.clearPile();
    turnsPassed = 0;
    currentTurn = winnerNum;
  }

  // This function transfers the current pile to the previous pile
  void transferPile() {
    previousPileOrdered.clear();
    previousPileOrdered = getPile();

    previousPile.clearPile();
    previousPile.setPile(pile.getPileStrings());
    previousPile.setTrump(pile.getTrump());
  }

  String findHighestCardOwner() {
    return pile.viewHighestCardOwner();
  }

  void resetPile() {
    pile.resetPile();
  }

  void resetPreviousPile() {
    previousPileOrdered.clear();
    previousPile.resetPile();
  }

  Map<String, dynamic> getCardToPlayerPile() {
    return pile.getPileStrings();
  }

  Map<String, dynamic> getPreviousCardToPlayerPile() {
    return previousPile.getPileStrings();
  }

  void resetDeck() {
    deck.resetDeck();
  }

  // Assigns the score to
  void handWinner() {
    String winner = findHighestCardOwner();
    int handsWon;
    handsWon = callsWon[currentRound][winner];
    handsWon++;
    callsWon[currentRound][winner] = handsWon;
  }

  void roundEndPoints() {
    for (Player player in players) {
      String nickname = player.getNickname();
      int call = calls[currentRound][nickname];
      int callWon = callsWon[currentRound][nickname];

      if (call == callWon) {
        player.addScore(10 + call);
      }

      rounds[currentRound][nickname] = player.getScore();
    }
  }

  void hexVote(String playerName) {
    hexVotes[this.playerName] = playerName;
    int hexVoteCount = 0;
    int minVotes = playerCount / 2 as int;
    Player hexingPlayer = players[0]; // temporarily set to first player.

    for (Player player in players) {
      if (player.nickname == playerName) {
        hexingPlayer = player;
      }
    }

    for (String value in hexVotes.values) {
      if (value == playerName) {
        hexVoteCount++;
      }
    }

    if (hexVoteCount > minVotes) {
      hexPlayer(hexingPlayer);
      playerHexed =
          playerName;
    }
  }

  void removeHexVote() {
    hexVotes[playerName] = null;
  }

  void hexPlayer(Player player) {
    player.setHexStatus(true);
  }

  // maybe make this just return a list of scores?...
  Player findWinner() {
    Player highestPlayer = Player(playerOrderNumber: -1, nickname: "null");
    // Finds the highest score
    for (Player player in players) {
      if (player.getScore() > highestPlayer.getScore()) {
        highestPlayer = player;
      }
    }

    for (Player player in players) {
      if (highestPlayer.getScore() == player.getScore() && highestPlayer.getNickname() != player.getNickname()) {
        // do something if there's a tie...
      }
    }

    return highestPlayer;
  }

  Future<void> sendEveryoneNotification(String title, String body, String threadID, String sound) async {
    for (Player player in players) {
      String fcmToken = "";
      fcmToken = notificationTokens[player.getNickname()];
      sendNotification(fcmToken, title, body, threadID, sound, 0, serverName, 1);
    }
  }

  void sendCurrentNotification() {
    sendNotification(getCurrentTurnNotificationToken(), "Magic 10 to 1", "Come back to $serverName, it's your turn!", serverName, NotificationSounds.nextTurn, turnCount, serverName, 1);
  }

  Future<void> addCallToFuture(int call) async {
    futureCalls[playerName] = call;

    await FirebaseFirestore.instance.collection('games').doc(serverName).update({
      'futureCalls': futureCalls,
    });
  }

  Future<void> clearFutureCall() async {
    futureCalls.remove(playerName);

    await FirebaseFirestore.instance.collection('games').doc(serverName).update({
      'futureCalls': futureCalls,
    });
  }

  Future<void> addCardToFuture(CardWrapper card) async {
    futureCards[playerName] = card.getString();

    await FirebaseFirestore.instance.collection('games').doc(serverName).update({
      'futureCards': futureCards,
    });
  }

  Future<void> clearFutureCard() async {
    futureCards.remove(playerName);

    await FirebaseFirestore.instance.collection('games').doc(serverName).update({
      'futureCards': futureCards,
    });
  }

  /// GETTER/SETTER METHODS

  // Returns a Boolean of whether the current round is a BlindMan Round
  bool getBlindManRound() => blindManRound;

  int getTurnCount() => turnCount;

  // Returns the index integer of the current round
  int getCurrentRound() => currentRound;

  int getCurrentTurn() => currentTurn;

  String getCurrentPlayerTurn() {
    String result = "error";
    playersOrder.forEach((key, value) {
      if (value == currentTurn) {
        result = key;
      }
    });
    return result;
  }

  int getHighestScore() {
    int highestScore = 0;

    currentScores.forEach((key, value) {
      int intValue = value as int;
      if (intValue > highestScore) {
        highestScore = intValue;
      }
    });

    if (highestScore == 0) {
      return -1;
    } else {
      return highestScore;
    }
  }

  String getTotalCallsString() {
    int result = 0;

    if (calls[currentRound].isEmpty) {
      return "-";
    }

    calls[currentRound].forEach((key, value) {
      int intValue = value as int;
      result = result + intValue;
    });

    return result.toString();
  }

  String getCurrentTurnNotificationToken() {
    return notificationTokens[getCurrentPlayerTurn()];
  }

  bool getGameComplete() => gameComplete;

  // Returns a list of all the cards in the device player's hand.
  List<CardWrapper> getMyHand() => getHand(playerName);

  // Returns the hexed player. Returns null if N/A.
  String getHexedPlayer() => playerHexed;

  // Returns hex votes
  Map getHexVotes() => hexVotes;

  // Returns hex voted
  Map getHexVoted() => hexVoted;

  // Returns a list of the playerNames.
  List<String> getPlayerNames() => playerStrings;

  List<Player> getPlayers() => players;

  CardWrapper getTrump() => pile.getTrump();

  Map<String, dynamic> getPlayersOrder() => playersOrder;

  // Returns the current score for the player name supplied
  int getScore(String playerName) {
    for (Player player in players) {
      if (player.nickname == playerName) {
        return player.getScore();
      }
    }
    return -1; // Shouldn't occur.
  }

  // Returns the total scoreboard as a map, with the playerName as the key, and
  // the score as the value.
  Map<String, Map<String, int>> getScoreBoard() {
    Map<String, Map<String, int>> scoreboard = {};

    for (Player player in players) {
      Map<String, int> individualScoreBoard = {};
      String nickname = player.getNickname();

      if (currentRound >= totalRounds) {
        individualScoreBoard["call"] = 0;
        individualScoreBoard["score"] = 0;
        individualScoreBoard["callWon"] = 0;
      } else {
        individualScoreBoard["call"] = calls[currentRound][nickname] ?? -1;
        individualScoreBoard["score"] = player.currentScore;
        individualScoreBoard["callWon"] = callsWon[currentRound][nickname] ?? 0;
      }
      scoreboard[nickname] = individualScoreBoard;
    }

    return scoreboard;
  }

  // Creates a list of lists. First list is List<String> of player names, in
  // order of winning. Subsequent lists are for each round, scores in the
  // same order.
  List<List<String>> getFinalScoreboard() {
    List<List<String>> result = [];

    // Create and add the playerNames list to the result.
    List<String> playerNames = [];
    currentScores.forEach((name, score) {
      playerNames.add(name);
    });

    playerNames.sort((a, b) => currentScores[a].compareTo(currentScores[b]));
    List<String> playersReversed = List.from(playerNames.reversed);
    result.add(playersReversed);

    // Go through each round and add each one.
    for (int i = 0; i < totalRounds; i++) {
      List<String> roundScores = [];
      for (String playerName in playersReversed) {
        roundScores.add(rounds[i][playerName].toString());
      }

      result.add(roundScores);
    }

    return result;
  }

  Map<String, List<String>> getFinalScoreboardSimple() {
    Map<String, List<String>> result = {};

    List<String> names = [];
    List<String> scores = [];
    List<String> places = [];

    currentScores.forEach((name, score) {
      names.add(name);
      scores.add(score.toString());
    });

    scores.sort((a, b) => int.parse(a).compareTo(int.parse(b)));
    names.sort((a, b) => currentScores[a].compareTo(currentScores[b]));

    List<String> sortedNames = List.from(names.reversed);
    List<String> sortedScores = List.from(scores.reversed);

    for (int i = 0; i < playerCount; i++) {
      String placeStr = findPlace(i);

      if (i == 0) {
        places.add(placeStr);
      } else if (sortedScores[i] == sortedScores[i-1]) {
        places.add(places[i-1]);
      } else {
        places.add(placeStr);
      }
    }

    result["names"] = sortedNames;
    result["scores"] = sortedScores;
    result["places"] = places;
    return result;
  }

  List<List<String>> getPopupScoreboard() {
    List<List<String>> result = [];

    // Create and add the playerNames list to the result.
    List<String> playerNames = [];
    currentScores.forEach((name, score) {
      playerNames.add(name);
    });

    playerNames.sort((a, b) => currentScores[a].compareTo(currentScores[b]));

    List<String> playersReversed = List.from(playerNames.reversed);
    List<String> playersReversedFour = List.from(playerNames.reversed);

    for (int i = 0; i < playerNames.length; i++) {
      playersReversedFour[i] = playersReversedFour[i].substring(0,4);
    }

    result.add(playersReversedFour);

    // Go through each round and add each one.
    for (int i = 0; i < currentRound; i++) {
      List<String> roundScores = [];
      for (String playerName in playersReversed) {
        roundScores.add(rounds[i][playerName].toString());
      }

      result.add(roundScores);
    }

    return result;
  }

  // Returns a list of all the cards in the supplied playerName's hand.
  // Organised by suit (with Joker at the front).
  List<CardWrapper> getHand(String playerName) {
    String trumpSuit = pile.getTrump().getSuit();
    for (Player player in players) {
      if (player.nickname == playerName) {
        return player.getHandCards(trumpSuit);
      }
    }
    List<CardWrapper> emptyList = [];
    return emptyList;
  }

  // Returns a List<CardWrapper> of the current pile, with the highest card last.
  List<CardWrapper> getPile() {
    List<CardWrapper> pileCards = [];
    int currentPileLength = pile.viewCurrentPile().keys.toList().length;

    if (pile.viewCurrentPile().isEmpty) {
      return pileCards;
    }

    // Sorts the nicknames into order played
    List<String> sortedNicknames = playersOrder.keys.toList(growable:false)
      ..sort((k1, k2) => (playersOrder[k1]).compareTo(playersOrder[k2]));

    // Finds which index number led this hand.
    int whoLed = pile.getWhoLedOrder();
    for (int i = 0; i < currentPileLength; i++) {
      String playerStr = sortedNicknames[whoLed];
      pile.viewCurrentPile().forEach((card, name) {
        if (name == playerStr) {
          pileCards.add(card);
        }
      });

      whoLed = (whoLed + 1) % playerCount;
    }

    // Takes the highest card and places it at the end of the list
    CardWrapper highestCard = pile.viewHighestCard();
    pileCards.removeWhere((item) => item.getString() == highestCard.getString());
    pileCards.add(highestCard);

    return pileCards;
  }

  // Same as the last function, but for the previous hand
  List<CardWrapper> getPreviousPile() {
    return previousPileOrdered;
  }

  // Returns an int of whether it is the devicePlayer's turn.
  // 0: Not player's Turn
  // 1: Player's turn (to call)
  // 2: Player's turn (to place a card)
  // 3: Game over.
  int turnTypeCheck() {
    int playerOrderNum = playersOrder[playerName];

    if (!(playerOrderNum == currentTurn)) {
      return 0;
    } else if (gameComplete) {
      return 3;
    } else if (!placeCards) {
      return 1;
    } else {
      return 2;
    }
  }

  // Returns a bool as to whether or not we are in a placeCards state
  bool placeCardsCheck() {
    return placeCards;
  }

  bool isItMyTurn() {
    if (turnTypeCheck() > 0) {
      return true;
    } else {
      return false;
    }
  }

  // Returns a string of the current player.
  String getCurrentPlayer() {
    return playerName;
  }

  int getServerVersion() {
    return versionStarted;
  }

  String getServerName() {
    return serverName;
  }

  int getFutureCall() {
    if (futureCalls[playerName] == null) {
      return -1;
    } else {
      return futureCalls[playerName];
    }
  }

  String getFutureCardString() {
    if (futureCards[playerName] == null) {
      return "n00";
    } else {
      return futureCards[playerName];
    }

  }

  // Returns the current suit of the cardLed, or empty if there is no cardLed.
  String currentPlaceSuit() {
    try {
      pile.getCardLed().getSuit();
    } catch (e) {
      return "-";
    }

    String suit = pile.getCardLed().getSuit();
    String cardLedStr = pile.getCardLed().getString();

    if (cardLedStr == "cJa" && pile.getTrump().getSuit() == "s") {
      return "♠️";
    } else if (cardLedStr == "sJa" && pile.getTrump().getSuit() == "c") {
      return "♣️";
    } else if (cardLedStr == "dJa" && pile.getTrump().getSuit() == "h") {
      return "♥️";
    } else if (cardLedStr == "hJa" && pile.getTrump().getSuit() == "d") {
      return "♦️";
    }

    if (suit == "j") {
      suit = pile.getTrump().getSuit();
    }

    if (suit == "n") {
      return "-";
    } else if (suit == "h") {
      return "♥️";
    } else if (suit == "d") {
      return "♦️";
    } else if (suit == "c") {
      return "♣️";
    } else if (suit == "s") {
      return "♠️";
    } else {
      return "error";
    }
  }

  String getWhoStarted() {
    int orderNum = currentRound % playerCount;
    String result = "";

    playersOrder.forEach((key, value) {
      if (value == orderNum) {
        result = key;
      }
    });

    return result;
  }

  // Returns a bool of if you can place the card you are trying to place.
  bool canIPlace(CardWrapper card) {
    String cardLedStr = pile.getCardLed().getString();
    String cardLedSuit = pile.getCardLed().getSuit();
    String trumpSuit = pile.getTrump().getSuit();
    String cardStr = card.getString();

    /// First off, work out what the cardLed suit really is.
    // if the cardLed is joker, updates the suit
    if (cardLedSuit == "j") {
      cardLedSuit = trumpSuit;
    }

    if (cardLedStr == "cJa" && trumpSuit == "s") {
      cardLedSuit = "s";
    } else if (cardLedStr == "sJa" && trumpSuit == "c") {
      cardLedSuit = "c";
    } else if (cardLedStr == "dJa" && trumpSuit == "h") {
      cardLedSuit = "h";
    } else if (cardLedStr == "hJa" && trumpSuit == "d") {
      cardLedSuit = "d";
    }

    /// Now, work out what suit the card we're looking to place down really is
    // check if the card is a joker, and if so, make it the trump suit.
    String cardSuit = card.getSuit();
    if (cardSuit == "j") {
      cardSuit = trumpSuit;
    }

    // Check if the card is the left bower, and if so, make it the trump suit.
    if (cardStr == "cJa" && trumpSuit == "s") {
      cardSuit = "s";
    } else if (cardStr == "sJa" && trumpSuit == "c") {
      cardSuit = "c";
    } else if (cardStr == "dJa" && trumpSuit == "h") {
      cardSuit = "h";
    } else if (cardStr == "hJa" && trumpSuit == "d") {
      cardSuit = "d";
    }

    /// Now the cardSuit and cardLedSuit are correct

    // Creates a list of all the cards in the player's hand.
    List<CardWrapper> playerHand = [];
    for (Player player in players) {
      if (player.nickname == findCurrentPlayerName()) {
        playerHand.addAll(player.getHandCards(trumpSuit));
      }
    }

    // Checks whether you have any of the cardLed suit in your hand.
    bool hasAny = false;
    for (CardWrapper handCard in playerHand) {
      String handCardStr = handCard.getString();
      String handCardSuit = handCard.getSuit();

      // Checks if the handCard is a joker
      if (handCardSuit == "j") {
        handCardSuit = trumpSuit;
      }

      // Checks if the handCard is a left bower
      if (handCardStr == "cJa" && trumpSuit == "s") {
        handCardSuit = "s";
      } else if (handCardStr == "sJa" && trumpSuit == "c") {
        handCardSuit = "c";
      } else if (handCardStr == "dJa" && trumpSuit == "h") {
        handCardSuit = "h";
      } else if (handCardStr == "hJa" && trumpSuit == "d") {
        handCardSuit = "d";
      }

      if (handCardSuit == cardLedSuit) {
        hasAny = true;
      }
    }

    // If you don't have any of the cardLed in your hand, you can place it.
    if (!hasAny) {
      return true;
    } else if (cardLedSuit == "n") { // or if there is no cardLed
      return true;
    } else if (cardSuit == cardLedSuit) { // or if the cardSuit is the same as the cardLedSuit
      return true;
    } else {
      return false;
    }
  }

  String timeSinceLastTurn() {
    return calculateTurnTime(lastTurnTime);
  }

  double calculateAvgTurnTime (String playerName) {
    double avgTime = 0.0;
    int playerNum = playersOrder[playerName];
    List<double> indivTurnTimes = [];


    for (int i = playerNum; i < turnCount; i = i + players.length) {
      if (i == 0) {
        break;
      }

      Timestamp playTimeStamp = turnTimes[i];
      Timestamp previousPlayTimeStamp = turnTimes[i-1];

      DateTime playTime = playTimeStamp.toDate();
      DateTime previousPlayTime = previousPlayTimeStamp.toDate();
      double playTimeDiff = playTime.difference(previousPlayTime).inSeconds.toDouble();

      indivTurnTimes.add(playTimeDiff);
    }

    avgTime = indivTurnTimes.average;
    return avgTime;
  }

  bool isCardQueued () {
    if (futureCards[playerName] == null) {
      return false;
    } else {
      return true;
    }
  }

  bool isCallQueued () {
    if (futureCalls[playerName] == null) {
      return false;
    } else {
      return true;
    }
  }

  void updateLocalHexInfo() {
    localHexInfo.put("hexMap_$serverName", hexVoted);
  }

  bool isHexDifferent() {
    Map localHexVotes = localHexInfo.get("hexMap_$serverName") ?? {};

    if (const MapEquality().equals(localHexVotes, hexVoted)) {
      return false;
    } else {
      return true;
    }
  }

}