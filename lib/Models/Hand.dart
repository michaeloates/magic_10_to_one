import 'dart:core';
import 'package:magic_10_to_one/Models/GameUtilities.dart';
import 'CardWrapper.dart';

class Hand {

  List<CardWrapper> cardsInHand = [];

  Hand();

  void addCardToHand(CardWrapper card){
    cardsInHand.add(card);
  }

  void removeCardFromHand(CardWrapper card) {
    cardsInHand.removeWhere((item) => item.getString() == card.getString());
  }

  List<CardWrapper> getHandCards(String trumpSuit) {
    sortCards(trumpSuit);
    return cardsInHand;
  }

  List<String> getHandStrings(String trump) {
    List<String> handStrings = [];
    sortCards(trump);

    for (CardWrapper card in cardsInHand) {
      handStrings.add(card.getString());
    }

    return handStrings;
  }

  // https://stackoverflow.com/questions/12888206/how-can-i-sort-a-list-of-strings-in-dart
  void sortCards(String trumpSuit) {
    cardsInHand.sort((a, b) => cardOrderHand(a, trumpSuit).compareTo(cardOrderHand(b, trumpSuit)));
  }
}