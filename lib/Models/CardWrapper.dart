import 'package:flutter/material.dart';

class CardWrapper {
  String cardStr;

  CardWrapper({
    required this.cardStr,
  });

  Image getImage() {
    return Image.asset('assets/images/CardImages/$cardStr.png');
  }

  Image getSpecImage(double height, double width) {
    return Image.asset(
        'assets/images/CardImages/$cardStr.png',
      fit: BoxFit.contain,
      height: height,
      width: width,
    );
  }

  String getSuit() {
    return cardStr.substring(0,1);
  }

  String getFullName() {
    String suit = "";
    String number = "";

    if (getSuit() == "s") {
      suit = "Spades";
    } else if (getSuit() == "c") {
      suit = "Clubs";
    } else if (getSuit() == "d") {
      suit = "Diamonds";
    } else if (getSuit() == "h") {
      suit = "Hearts";
    } else if (getSuit() == "j") {
      return "Joker";
    } else {
      // an error
      return "n00";
    }

    if (getValue() == "01") {
      number = "Ace";
    } else if (getValue() == "02") {
      number = "Two";
    } else if (getValue() == "03") {
      number = "Three";
    } else if (getValue() == "04") {
      number = "Four";
    } else if (getValue() == "05") {
      number = "Five";
    } else if (getValue() == "06") {
      number = "Six";
    } else if (getValue() == "07") {
      number = "Seven";
    } else if (getValue() == "08") {
      number = "Eight";
    } else if (getValue() == "09") {
      number = "Nine";
    } else if (getValue() == "10") {
      number = "Ten";
    } else if (getValue() == "11") {
      number = "Eleven";
    } else if (getValue() == "12") {
      number = "Twelve";
    } else if (getValue() == "13") {
      number = "Thirteen";
    } else if (getValue() == "14") {
      number = "Fourteen";
    } else if (getValue() == "15") {
      number = "Fifteen";
    } else if (getValue() == "16") {
      number = "Sixteen";
    } else if (getValue() == "17") {
      number = "Seventeen";
    } else if (getValue() == "Ja") {
      number = "Jack";
    } else if (getValue() == "Qu") {
      number = "Queen";
    } else if (getValue() == "Ki") {
      number = "King";
    }

    return "$number of $suit";
  }

  String getString() {
    return cardStr;
  }

  String getValue() {
    return cardStr.substring(1);
  }

  int parseValue() {
    return int.parse(getValue());
  }
}