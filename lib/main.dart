import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:overlay_support/overlay_support.dart';
import 'Matchmaking/ProfileMenu.dart';
import 'Matchmaking/RejoinMenu.dart';
import 'Matchmaking/ServerMenu.dart';
import 'Matchmaking/HostMenu.dart';
import 'Models/GameUtilities.dart';
import 'Drawer/NavDrawer.dart';
import 'enums/cards.dart';

Future<void> main() async {
  await Hive.initFlutter();
  await Hive.openBox(ProfileKeys.profile);
  await Hive.openBox(ProfileKeys.hexBox);

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OverlaySupport(
        child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: MaterialColor(0xFF2D682D, colorCodes),
        ),
        home: const MyHomePage(title: 'Magic 10 to 1'),
      )
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final FirebaseMessaging _messaging = FirebaseMessaging.instance;



  Future<void> setupNotifications() async {
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      debugPrint('User granted permission');
    } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
      debugPrint('User granted provisional permission');
    } else {
      debugPrint('User declined or has not accepted permission');
    }
  }

  @override
  void initState() {
    super.initState();
    setupNotifications();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: const Color(0xFF256D1B),
      resizeToAvoidBottomInset: false,
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 450,
              child: GridView.count(
                crossAxisCount: 2,
                children: [
                  Option(
                    backgroundColor: const Color(0xFFA10702),
                    icon: Icons.storage,
                    active: true,
                    text: "Host",
                    nextPage: HostMenu(),
                  ),
                  Option(
                      backgroundColor: const Color(0xFFF44708),
                      icon: Icons.play_arrow,
                      active: true,
                      text: "Join",
                      nextPage: const ServerMenu(),
                  ),
                  Option(
                      backgroundColor: const Color(0xFFFAA613),
                      icon: Icons.person_outline_outlined,
                      active: true,
                      text: "Profile",
                      nextPage: const ProfileMenu()
                  ),
                  Option(
                      backgroundColor: const Color(0xFF550527),
                      icon: Icons.login,
                      active: true,
                      text: "Rejoin",
                      nextPage: const RejoinMenu()
                  ),
                ]
              )
            )
          ],
        ),
      ),
    );
  }


  Widget Option(
      {Color backgroundColor = const Color(0xFF5BC0EB), IconData icon = Icons.disabled_by_default, bool active = false, String text = "default", required Widget nextPage}) {
    var iconColor;
    var textColor;
    active ? iconColor = Colors.white : iconColor = Colors.grey;
    active ? textColor = Colors.white : textColor = Colors.grey;

    return Builder(builder: (context) {
      return Padding(
        padding: const EdgeInsets.only(
            left: 10.0, right: 10, bottom: 5, top: 5),
        child: Card(
          color: backgroundColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            side: BorderSide(
                color: Colors.white,
              width: 5,
            ),
          ),
          child: InkWell(
            onTap: () {
              active ?
              Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => nextPage
                  )
              ) :
              debugPrint("no page route found");
            },
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(icon, size: 40,
                    color: iconColor,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(text, style: TextStyle(
                      color: textColor,
                      fontWeight: FontWeight.w700,
                      fontSize: 16.0,
                    ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
    );
  }
}
