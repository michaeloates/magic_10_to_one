enum Cards {
  d01,
  d02,
  d03,
  d04,
  d05,
  d06,
  d07,
  d08,
  d09,
  d10,
  d11,
  d12,
  d13,
  d14,
  d15,
  dJa,
  dQu,
  dKi,

  h01,
  h02,
  h03,
  h04,
  h05,
  h06,
  h07,
  h08,
  h09,
  h10,
  h11,
  h12,
  h13,
  h14,
  h15,
  hJa,
  hQu,
  hKi,

  s01,
  s02,
  s03,
  s04,
  s05,
  s06,
  s07,
  s08,
  s09,
  s10,
  s11,
  s12,
  s13,
  s14,
  sJa,
  sQu,
  sKi,

  c01,
  c02,
  c03,
  c04,
  c05,
  c06,
  c07,
  c08,
  c09,
  c10,
  c11,
  c12,
  c13,
  c14,
  cJa,
  cQu,
  cKi,

  jJo,

  n00,
}

class GamesKeys {
  static String games = "games";
  static String players = "player";
  static int version = 1302;
}

class NotificationSounds {
  static String nextTurn = "nextTurn.wav";
  static String smallBad = "smallBad.wav";
  static String bigFail = "bigFail.wav";
}

class ProfileKeys {
  static String profile = "profile";
  static String profileName = "profile-name";
  static String profilePassword = "profile-password";
  static String profileCreated = "profile-created";
  static String profileHighestScore = "profile-highest-score";
  static String profileLowestScore = "profile-lowest-score";
  static String profileLosses = "profile-losses";
  static String profileWins = "profile-wins";
  static String hexBox = "localHexInfo";
}

class CardKeys {
  static String suitDiamonds = "diamonds";
  static String suitHearts = "hearts";
  static String suitSpades = "spades";
  static String suitClubs = "clubs";
  static String suitJoker = "joker";

  static String diamondAce = "d01";
  static String diamondTwo = "d02";
  static String diamondThree = "d03";
  static String diamondFour = "d04";
  static String diamondFive = "d05";
  static String diamondSix = "d06";
  static String diamondSeven = "d07";
  static String diamondEight = "d08";
  static String diamondNine = "d09";
  static String diamondTen = "d10";
  static String diamondEleven = "d11";
  static String diamondTwelve = "d12";
  static String diamondThirteen = "d13";
  static String diamondFourteen = "d14";
  static String diamondFifteen = "d15";
  static String diamondSixteen = "d16";
  static String diamondSeventeen = "d17";
  static String diamondJack = "dJa";
  static String diamondQueen = "dQu";
  static String diamondKing = "dKi";

  static String heartAce = "h01";
  static String heartTwo = "h02";
  static String heartThree = "h03";
  static String heartFour = "h04";
  static String heartFive = "h05";
  static String heartSix = "h06";
  static String heartSeven = "h07";
  static String heartEight = "h08";
  static String heartNine = "h09";
  static String heartTen = "h10";
  static String heartEleven = "h11";
  static String heartTwelve = "h12";
  static String heartThirteen = "h13";
  static String heartFourteen = "h14";
  static String heartFifteen = "h15";
  static String heartSixteen = "h16";
  static String heartSeventeen = "h17";
  static String heartJack = "hJa";
  static String heartQueen = "hQu";
  static String heartKing = "hKi";

  static String spadeAce = "s01";
  static String spadeTwo = "s02";
  static String spadeThree = "s03";
  static String spadeFour = "s04";
  static String spadeFive = "s05";
  static String spadeSix = "s06";
  static String spadeSeven = "s07";
  static String spadeEight = "s08";
  static String spadeNine = "s09";
  static String spadeTen = "s10";
  static String spadeEleven = "s11";
  static String spadeTwelve = "s12";
  static String spadeThirteen = "s13";
  static String spadeFourteen = "s14";
  static String spadeFifteen = "s15";
  static String spadeSixteen = "s16";
  static String spadeSeventeen = "s17";
  static String spadeJack = "sJa";
  static String spadeQueen = "sQu";
  static String spadeKing = "sKi";

  static String clubAce = "c01";
  static String clubTwo = "c02";
  static String clubThree = "c03";
  static String clubFour = "c04";
  static String clubFive = "c05";
  static String clubSix = "c06";
  static String clubSeven = "c07";
  static String clubEight = "c08";
  static String clubNine = "c09";
  static String clubTen = "c10";
  static String clubEleven = "c11";
  static String clubTwelve = "c12";
  static String clubThirteen = "c13";
  static String clubFourteen = "c14";
  static String clubFifteen = "c15";
  static String clubSixteen = "c16";
  static String clubSeventeen = "c17";
  static String clubJack = "cJa";
  static String clubQueen = "cQu";
  static String clubKing = "cKi";

  static String joker = "jJo";
}