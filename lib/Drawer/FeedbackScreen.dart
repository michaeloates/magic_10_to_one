import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:magic_10_to_one/Models/emailUtilities.dart';

import '../enums/cards.dart';

class FeedbackScreen extends StatefulWidget {
  const FeedbackScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FeedbackScreenState();
}

class _FeedbackScreenState extends State<FeedbackScreen> {
  final _feedbackKey = GlobalKey<FormState>();
  Box profileBox = Hive.box(ProfileKeys.profile);
  String playerName = "Not Loaded Yet";

  @override
  Widget build(BuildContext context) {
    playerName = profileBox.get(ProfileKeys.profileName, defaultValue: "null");

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text("Feedback"),
        // backgroundColor: const Color(0xFF44435E),
      ),
      backgroundColor: const Color(0xFF256D1B),
      body: Container(
        alignment: Alignment.topCenter,
        color:  const Color(0xFF4b4a67),
        padding: const EdgeInsets.fromLTRB(10, 50, 10, 10),
        child: Form(
          key: _feedbackKey,
          child: Column (
            children: [
              RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                text: "Send any questions, comments or feedback below!\n",
                style: TextStyle(fontSize: 18, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
              ),
            ),
              TextFormField(
                maxLines: 10,
                decoration: const InputDecoration(
                    fillColor: Color(0x11FFFFFF),
                    filled: true,
                    hintText: 'Enter feedback',
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        color: Color(0xFFf2af29),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 2,
                        color: Color(0xFFf2af29),
                      ),
                    ),
                    labelText: 'Feedback',
                    labelStyle: TextStyle(
                      color: Color(0xFFf2af29),
                    )
                ),
                style: const TextStyle(color: Color(0xFFf2af29),),
                cursorColor: const Color(0xFFf2af29),
                onSaved: (String? value) {
                  String? body = value;
                  body = (body! + "\n\n - Sent by $playerName");

                  sendFeedbackEmail(body);
                },
                validator: (String? value) {
                  return null;

                  // return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
                },
              ),
              ElevatedButton(
                onPressed: () {
                  _feedbackKey.currentState!.save();
                  showDialog(context: context, builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Feedback Sent'),
                      content: SingleChildScrollView(
                        child: ListBody(
                          children: const <Widget>[
                            Text('Thanks for sending your feedback!'),
                          ],
                        ),
                      ),
                      actions: <Widget>[
                        TextButton(
                          child: const Text('Dismiss'),
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },);
                  },
                child: const Text("Send"),
                style: ElevatedButton.styleFrom(
                    primary: const Color(0x88f2af29),
                ),
              ),
            ],
          )
        )
      ),
    );
  }
}