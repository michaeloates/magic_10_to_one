import 'package:flutter/material.dart';
import 'package:magic_10_to_one/Drawer/Changelist.dart';
import 'package:magic_10_to_one/Drawer/CreditsScreen.dart';
import 'package:magic_10_to_one/Drawer/FeedbackScreen.dart';
import 'package:magic_10_to_one/Drawer/Tutorial.dart';
import 'package:url_launcher/url_launcher.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key? key}) : super(key: key);

  _launchURL() async {
    final Uri privacyPolicy = Uri.parse('https://www.michaeloates.cc/Privacy-Policy.html');

    try {
      await launchUrl(privacyPolicy);
    } catch (e) {
      print (e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const DrawerHeader(
            child: Text(
              'Menu',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
            decoration: BoxDecoration(
                color:  Color(0xFF2D682D),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Privacy Policy'),
            onTap: _launchURL,
          ),
          ListTile(
            leading: const Icon(Icons.border_color),
            title: const Text('Feedback'),
            onTap: () => {Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const FeedbackScreen())
            )},
          ),
          ListTile(
            leading: const Icon(Icons.format_align_center),
            title: const Text('Credits'),
            onTap: () => {Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => const CreditsScreen())
            )},
          ),
          ListTile(
            leading: const Icon(Icons.book),
            title: const Text('Tutorial'),
            onTap: () => {Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const TutorialScreen())
            ),},
          ),
          ListTile(
            leading: const Icon(Icons.published_with_changes),
            title: const Text('Changelist'),
            onTap: () => {Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const Changelist())
            )},
          ),
        ],
      ),
    );
  }
}