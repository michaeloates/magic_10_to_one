import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Changelist extends StatefulWidget {
  const Changelist({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChangelistState();
}

class _ChangelistState extends State<Changelist> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Changelist"),
        // backgroundColor: const Color(0xFF44435E),
      ),
      backgroundColor: const Color(0xFF4b4a67),
      body: ListView (
        children: [

          // Title
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 30, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: 'Magic 10 to 1 Changelist:\n', style: TextStyle(fontSize: 18,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),

          // 1.3.2
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.3.2:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Added a Hex alert border around the hex button, for when new hex votes are made\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Small bug fixes & quality of life improvements\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.3.1
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.3.1:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Fixed a issue with loading games\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.3.0
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.3.0:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Added the ability to queue cards and calls.\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Reduced notifications at the start of a game\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.2.15
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.15:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Added a timer since the last turn.\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Archived Games now loads correctly\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Optimisation in the top row of the Play Screen\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],

              ),
            ),
          ),

          // 1.2.14
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.14:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Playing a card, or calling will clear notifications.\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- iOS now has a badge icon.\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],

              ),
            ),
          ),

          // 1.2.13
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.13:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- The app will now skip the card flip page.\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Some other bugs & speed improvements.\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],

              ),
            ),
          ),

          // 1.2.12
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.12:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Secret magic function\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Total calls made are now shown\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- The player currently winning has their name and score shown with gold highlight\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],

              ),
            ),
          ),

          // 1.2.11
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.11:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- A mini scoreboard can be shown by clicking the scoreboard in-game\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- You cannot be hexed on your birthday (supported players only)\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],

              ),
            ),
          ),

          // 1.2.10
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.10:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- 16 of clubs shows the correct image now\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.2.9
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.9:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Fixed a hand sorting issue\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.2.8
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.8:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Removed Hex Notifications due to spam\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Added the ability for 9-to-1 and 10-to-1 games with 8 players, and 8-to-1 with 9 players\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.2.7
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.7:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Hexing players now requires over 50% (previously 66%)\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Added notifications when hexes are made\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- New warning texts\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Fixed an issue with "Outdated Sever" being displayed\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.2.6
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.6:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- The "Rejoin" menu now only shows currently active games\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Completed games can be viewed in the "Profile" menu, under "Archived Games"\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: "- People who haven't called now show '-' for their call, instead of '0'\n", style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- The suit of the card led now shows the correct colour on iOS\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: "- Fixed an issue where notifications weren't updating correctly\n", style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.2.5
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.5:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Fixed an issue with the Scores Breakdown resetting\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Added the Changelist\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: '- Updated the design of the credits screen\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),

          // 1.2.4
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.4:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Added a crown for players that score over 200\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Emails now send correctly\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: '- Added a custom sound for push notifications\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),

          // 1.2.3
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.3:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Notifications are now time-sensitive (for iOS)\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Profile menu design update\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),

          // 1.2.2
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.2:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- The scoreboard now shows extra info about the game\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Added a "Score Breakdown" section of the scores screen\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: '- Added a "200 Count" to the scores screen\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: '- Added more notifications\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),

          // 1.2.1
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.1:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Removed sounds from the play screen\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Fixed scoring issues\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: '- Only inactive games show on the "Join" menu\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: '- Only games you are a part of show in the "Rejoin" menu\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: '- Fixed a couple of other small bugs\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),

          // 1.2.0
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.2.0:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- Added push notifications!\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),

          // 1.1.0
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '1.1.0:\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal)),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
            child: RichText(
              textAlign: TextAlign.left,
              text: const TextSpan(
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: '- The previous hand is now viewable by clicking on the deck\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- The current round shows on the top of play screen\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Added some sounds and vibrations\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Fixed an issue with the trump not updating\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                  TextSpan(text: '- Fixed some other small bugs\n', style: TextStyle(fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,)),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}