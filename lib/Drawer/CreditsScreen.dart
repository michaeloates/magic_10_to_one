import 'package:flutter/material.dart';

class CreditsScreen extends StatefulWidget {
  const CreditsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CreditsScreenState();
}

class _CreditsScreenState extends State<CreditsScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Credits"),
        // backgroundColor: const Color(0xFF44435E),
      ),
      backgroundColor: const Color(0xFF44435E),
      body: ListView (
        children: <Widget> [
          Container(
            padding: const EdgeInsets.fromLTRB(30, 20, 30, 10),
            child: Image.asset("assets/images/Logos/clean.png"),
          ),
          Container(
            alignment: Alignment.topCenter,
            //color:  const Color(0xFF4b4a67),
            padding: const EdgeInsets.fromLTRB(10, 50, 10, 10),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                text: "Thanks for playing Magic 10 to 1!\n\n\n",
                style: TextStyle(fontSize: 20, color: Color(0xFFf2af29), fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: 'Created by\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: 'Michael Oates     David Oates\nMajikku Studios\n\n\n', style: TextStyle(fontSize: 18,
                      fontWeight: FontWeight.bold)),
                  TextSpan(text: 'Additional Thanks to\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal)),
                  TextSpan(text: "Frog, LOC, Stretch, Monkey, Tank, Matchstick, Red Dog, Mr. T, Roach, Pooh, and the rest of the Wood players\n",
                      style: TextStyle(fontSize: 16,
                          fontWeight: FontWeight.w600)),
                  TextSpan(text: "\n\n\n Version 1.3.2 \n",
                      style: TextStyle(fontSize: 16,
                          fontWeight: FontWeight.w300)),
                ],
              ),
            ),
          ),



        ],
      )
    );
  }
}