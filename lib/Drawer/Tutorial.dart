import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:magic_10_to_one/Models/CardWrapper.dart';

class TutorialScreen extends StatefulWidget {
  const TutorialScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TutorialScreenState();
}

class _TutorialScreenState extends State<TutorialScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tutorial"),
        // backgroundColor: const Color(0xFF44435E),
      ),
      backgroundColor: const Color(0xFF256D1B),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.topCenter,
          color:  const Color(0xFF4b4a67),
          padding: const EdgeInsets.fromLTRB(10, 50, 10, 10),
          child: Column(
            children: [
              RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text: "10 to 1 tutorial\n\n\n",
                  style: TextStyle(fontSize: 20, color: Color(0xFFf2af29)),
                  children: <TextSpan>[
                    TextSpan(text: 'General\n', style: TextStyle(fontSize: 15,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                        fontStyle: FontStyle.normal,)),
                    TextSpan(text: '"10 to 1" ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                    TextSpan(text: 'is a multiplayer card game that incorporates a series of sets and rounds that must be won cumulatively in order to win.\n '
                        '\nPlayers must intuitively predict how many sets they will win prior to the start of a round in order to earn points. \n\n', style: TextStyle(fontSize: 15)),
                    TextSpan(text: 'Calling\n', style: TextStyle(fontSize: 15,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal,
                        decoration: TextDecoration.underline,)),
                    TextSpan(text: "At the beginning of each round, each player is required to first call how many sets they expect to win. A ",
                        style: TextStyle(fontSize: 15)),
                    TextSpan(text: "set ",
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold,)),
                    TextSpan(text: "in this case is one full rotation of the total players. The last player to call must choose a number that does not equal the amount "
                        "of total sets for the round in order to ensure at least one person fails.\n",
                        style: TextStyle(fontSize: 15)),
                    TextSpan(text: '\nPlaying a round\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                      decoration: TextDecoration.underline,)),
                    TextSpan(text: "Once everyone has called, the first set of the round is played. Players must ",
                        style: TextStyle(fontSize: 15)),
                    TextSpan(text: "in order ",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                    TextSpan(text: "place a card from their hand and attempt to win and lose sets to meet their call. \n\nThe first card placed in a set "
                        "determines what suit is led for that set. Players must follow the suit each set and place a card that corresponds to the suit. If the player does not have a card corresponding to the suit in their hand, they must place another card of a different suit. "
                        "In most cases, this means discarding a different card in their hand with the exception of trump cards. \n",
                        style: TextStyle(fontSize: 15)),
                    TextSpan(text: '\nTrumps\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                      decoration: TextDecoration.underline,)),
                    TextSpan(text: "Each round, a specific suit is selected as the 'trump' suit of that round. Cards corresponding to this suit will automatically be valued higher than any other suit regardless of the value they present.\n\n"
                        "For example, if the card led for a set is the Ace of Spades and the next player places the two of Diamonds (with Diamonds being the trump suit for this round), then the second player is currently winning the set. \n",
                        style: TextStyle(fontSize: 15)),
                  ],
                ),
              ),
              Row (
                children: [
                  Expanded(flex: 1, child: Column(
                      children: [
                        CardWrapper(cardStr: "s01").getSpecImage(150, 100),
                        const Text(
                          "player1",
                          style: TextStyle(color: Color(0xFFf2af29)),
                        ),
                      ]
                  ),
                  ),
                  Expanded(flex: 1, child: Column(
                      children: [
                        CardWrapper(cardStr: "d02").getSpecImage(150, 100),
                        const Text(
                          "player2",
                          style: TextStyle(color: Color(0xFFf2af29)),
                        ),
                      ]
                  ),
                  ),
                ],
              ),
              RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text: "",
                  style: TextStyle(fontSize: 20, color: Color(0xFFf2af29)),
                  children: <TextSpan>[
                    TextSpan(text: '\nBowers and the Joker\n', style: TextStyle(fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                      decoration: TextDecoration.underline,)),
                    TextSpan(text: "The final exception to this game includes the Bowers and the Joker. In 10 to 1, the Joker is the highest valued card, winning any set automatically. It should be noted however that"
                        " the Joker is still limited by being the same suit of the trump, and therefore, can only be played like a trump card. \n\n The second and third highest valued cards are the right and left Bower respectively. "
                        "The right Bower is always the Jack of the trump suit, and the left the Jack of the suit presenting the same colour. Both bowers, regardless of what suit they present on the card, are of the trump suit. For example, if the trump suit is Hearts, then the right Bower will be the Jack of Hearts, and the left"
                        " the Jack of Diamonds - with both being considered from the suit of Hearts\n",
                        style: TextStyle(fontSize: 15)),
                  ],
                ),
              ),
              Row (
                children: [
                  Expanded(flex: 1, child: Column(
                    children: [
                      const Text(
                          "3rd",
                          style: TextStyle(color: Color(0xFFf2af29)),
                      ),
                      CardWrapper(cardStr: "dJa").getSpecImage(150, 100)
                    ]
                  ),
                  ),
                  Expanded(flex: 1, child: Column(
                      children: [
                        const Text(
                          "1st",
                          style: TextStyle(color: Color(0xFFf2af29)),
                        ),
                        CardWrapper(cardStr: "jJo").getSpecImage(150, 100)
                      ]
                  ),
                  ),
                  Expanded(flex: 1, child: Column(
                      children: [
                        const Text(
                          "2nd",
                          style: TextStyle(color: Color(0xFFf2af29)),
                        ),
                        CardWrapper(cardStr: "hJa").getSpecImage(150, 100)
                      ]
                  ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}