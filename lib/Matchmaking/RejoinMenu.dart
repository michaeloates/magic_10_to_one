import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';

import '../Models/GameUtilities.dart';
import '../enums/cards.dart';
import '../game/OrderSelection.dart';
import '../game/PlayScreen.dart';

class RejoinMenu extends StatefulWidget {
  const RejoinMenu({Key? key}) : super(key: key);

  @override
  _RejoinMenuState createState() => _RejoinMenuState();
}

class _RejoinMenuState extends State<RejoinMenu> {
  List<QueryDocumentSnapshot<Object?>> docs = [];
  Box profileBox = Hive.box(ProfileKeys.profile);
  String playerName = "";
  List<dynamic> players = [];
  bool gameContainsPlayer = false;
  bool gameComplete = true;
  int minServerVersion = 9999;


  void retrieveDocs(AsyncSnapshot<QuerySnapshot> snap) {
    // do same thing in queue. Set game started once order selection started and then head there immediately.
    docs.clear();
    if (snap.data?.docs != null) {
      for (QueryDocumentSnapshot<Object?> doc in snap.data!.docs) {
        players = doc.get('players');
        gameContainsPlayer = players.contains(playerName);
        gameComplete = doc.get('gameComplete');

        try {
          if (doc.get('gameStarted') && gameContainsPlayer && !gameComplete) {
            docs.add(doc);
          }
        } catch (e) {
          //debugPrint("This server does not have the attribute: gameStarted");
        }
      }
    }
  }

  void setName() async {
    playerName = await profileBox.get(ProfileKeys.profileName, defaultValue: null);
  }

  Future findMinVersion() async {
    DocumentReference ref = FirebaseFirestore.instance.collection('Keys').doc('version');

    await ref.get().then((snapshot) {
      minServerVersion = snapshot.get('minServerVersion');
    });
  }

  @override
  void initState()  {
    super.initState();
    setName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Server List")
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('games').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            retrieveDocs(snapshot);
            return ListView.builder(
                shrinkWrap: true,
                itemCount: docs.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return Card (
                      elevation: 0,
                      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                      child: GestureDetector(
                        onTap: () async {
                          List<dynamic> players = docs[index].get('players');
                          int serverVersion = await docs[index].get('versionStarted');
                          await findMinVersion();
                          bool serverVersionCheck = serverVersion >= minServerVersion;
                          bool versionCheck = await checkLatestVersion();

                          if (players.contains(playerName)) {
                            if (!versionCheck) {
                              Fluttertoast.showToast(
                                msg: "App needs to be updated to continue!",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                fontSize: 16.0,
                              );
                            } else if (!serverVersionCheck) {
                              Fluttertoast.showToast(
                                msg: "Server is outdated. Please start a new game.",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                fontSize: 16.0,
                              );
                            } else {
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (context) => PlayScreen(serverName: docs[index].id)
                                  )
                              );
                            }
                          } else {
                            Fluttertoast.showToast(
                              msg: "Sorry, you aren't part of this running game",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              fontSize: 16.0,
                            );
                            setState(() {});
                          }
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xFF550527),
                                border: Border.all(
                                  color: Colors.white,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(20))
                            ),
                            child: ListTile(
                              leading: const Icon(Icons.dns, color: Colors.white,),
                              title: Text(docs[index].id, style: const TextStyle(color: Colors.white, fontSize: 20),),
                              trailing: Text(docs[index].get('players').length.toString(), style: const TextStyle(
                                color: Colors.white,
                              ),),
                            ),
                        ),
                      )
                  );
                }
            );
          } else {
            return const Text("No servers available");
          }
        }
      ),
    );
  }
}

