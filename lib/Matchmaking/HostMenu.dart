import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:magic_10_to_one/Models/GameUtilities.dart';
import 'dart:math';
import '../enums/cards.dart';
import 'Queue.dart';
import 'package:hive/hive.dart';

class Model {
  bool blindManRound;
  var calls = [];
  var callsWon = [];
  int currentRound;
  var currentScores = {};
  String currentTrump;
  int currentTurn;
  var deckStrings = [];
  var emailResults = [];
  bool firstCardsDealt = false;
  bool firstPlayerNotified = false;
  Map futureCards = {};
  Map futureCalls = {};
  bool gameComplete;
  var hands = {};
  var hexVotes = {};
  var hexVoted = {};
  String hostName = "";
  var notificationTokens = {};
  String personHexed = "";
  var pileStuff = {};
  bool placeCards = false;
  int playerCount;
  var players = [];
  var playersOrder = {};
  var previousPile = {};
  var previousPileOrdered = [];
  bool queueFull;
  bool realGame = true;
  var rounds = [];
  var selectionHandKnown = {};
  var selectionHandUnknown = {};
  String serverName;
  String? serverPassword;
  int totalRounds;
  int turnCount;
  var turnTimes = [];
  var twoHundredClub = {};
  int turnsPassed;
  int versionStarted;

  bool fillExampleData;

  Box profileBox = Hive.box(ProfileKeys.profile);
  Random rng = Random();
  String gameLengthString = "10-to-1";


  // test input stuff

  var fourPlayersMap = const [
    {"display" : "10-to-1",
      "value" : 20,},
  ];

  var fivePlayersMap = const [
    {"display" : "10-to-1",
      "value" : 20,},
  ];

  var sixPlayersMap = const [{
      "display" : "10-to-1",
      "value" : 20,},
  ];

  var sevenPlayersMap = const [
    {"display" : "10-to-1",
      "value" : 20,},
    {"display" : "9-to-1",
      "value" : 18,},
  ];

  var eightPlayersMap = const [
    {"display" : "8-to-1",
      "value" : 16,},
    {"display" : "9-to-1",
      "value" : 18,},
    {"display" : "10-to-1",
      "value" : 20,},
  ];

  var ninePlayersMap = const [
    {"display" : "7-to-1",
      "value" : 14,},
    {"display" : "8-to-1",
      "value" : 16,},
  ];

  var tenPlayersMap = const [
    {"display" : "7-to-1",
      "value" : 14,},
    {"display" : "6-to-1",
      "value" : 12,},
  ];

  List<String> fourPlayersOptions = const [
    "10-to-1"
  ];

  List<String> fivePlayersOptions = const [
    "10-to-1"
  ];

  List<String> sixPlayersOptions = const [
    "10-to-1"
  ];

  List<String> sevenPlayersOptions = const [
    "10-to-1", "9-to-1"
  ];

  List<String> eightPlayersOptions = const [
    "10-to-1", "9-to-1", "8-to-1"
  ];

  List<String> ninePlayersOptions = const [
    "7-to-1", "8-to-1"
  ];

  List<String> tenPlayersOptions = const [
    "7-to-1", "6-to-1"
  ];

  List<int> numPlayerOptions = [4, 5, 6, 7, 8, 9, 10];

  Model({
    this.serverName = "game",
    this.serverPassword,
    this.blindManRound = false,
    this.currentRound = 0,
    this.currentTrump = "n00",
    this.currentTurn = 0,
    this.gameComplete = false,
    this.personHexed = "",
    this.playerCount = 6,
    this.queueFull = false,
    this.totalRounds = 20,
    this.turnCount = 0,
    this.turnsPassed = 0,
    this.versionStarted = 000,

    this.fillExampleData = false,
  }) {
    assignServerNameNumber();
  }

  void populateFields() {
    populateCalls();
    populateCallsWon();
    populateRounds();
    populateDeck();
    populateEmail();
    populatePileStuff();

    versionStarted = GamesKeys.version;

    if (fillExampleData) {
      populateExampleData();
    }
  }

  Future<bool> accountCreated() async {
    String playerName = await profileBox.get(ProfileKeys.profileName, defaultValue: "accountNotCreated");

    if (playerName == "accountNotCreated") {
      return false;
    } else {
      return true;
    }

  }

  void populateCalls() {
    calls.clear();
    for (int i = 0; i < totalRounds; i++) {
      var round = {};
      calls.add(round);
    }
  }

  void populateCallsWon() {
    callsWon.clear();
    for (int i = 0; i < totalRounds; i++) {
      var round = {};
      callsWon.add(round);
    }
  }

  void populateRounds() {
    rounds.clear();
    for (int i = 0; i < totalRounds; i++) {
      Map round = {};
      rounds.add(round);
    }
  }

  void populateDeck() {
    deckStrings = provideDeck(playerCount, totalRounds).getDeckStrings();
  }

  void populateEmail() {
    emailResults.add("10.to.1.magic@gmail.com");
  }

  void populatePileStuff() {
    Map pile = {};
    pileStuff.putIfAbsent("pile", () => pile);
    pileStuff.putIfAbsent("highestCard", () => "n00");
    pileStuff.putIfAbsent("cardLed", () => "n00");
    pileStuff["whoLedOrder"] = -1;
  }

  void populateExampleData() {
    populateCallsExample();
    populateCallsWonExample();
    int numPlayers = examplePlayers.length;

    hostName = examplePlayers[0];

    selectionHandKnown = {};
    selectionHandUnknown = {};
    players = [];
    currentScores = {};
    playersOrder = {};
    placeCards = true;
    firstCardsDealt = true;

    deckStrings.clear();
    deckStrings.add("cKi");
    deckStrings.add("sJa");
    deckStrings.add("cJa");
    deckStrings.add("s01");

    playersOrder = {
      "mikePC0" : 1,
      "mikePC1" : 3,
      "mikePC2" : 0,
      "mikePC3" : 2,
    };

    for (int i = 0; i < numPlayers; i++) {
      String examplePlayer = examplePlayers[i];
      selectionHandKnown[examplePlayer] = allCards[rng.nextInt(allCards.length)];
      selectionHandUnknown[examplePlayer] = selectionHandKnown[examplePlayer];
      players.add(examplePlayer);
    }

    currentRound = totalRounds - 2;
    currentTurn = 0;
    turnsPassed = numPlayers - 1;


    for (String player in examplePlayers) {
      List<String> hand = [];
      for (int i = 0; i < 1; i++) {
        // hand.add(allCards[rng.nextInt(allCards.length)]);
      }
      hands[player] = hand;
    }

    List<String> hand = ["h01"];
    hands["mikePC2"] = hand;


    Map pile = {
      "c08" : "mikePC0",
      "hQu" : "mikePC1",
      "hKi" : "mikePC3",
    };

    pileStuff["pile"] = pile;
    pileStuff["highestCard"] = "c08";
    pileStuff["cardLed"] = "c08";
    pileStuff["whoLedOrder"] = 1;

    queueFull = true;

    for (int i = 0; i < (totalRounds - 1); i++) {
      Map round = {
        "trump" : allCards[rng.nextInt(allCards.length)],
      };
      rounds[i] = (round);
    }

    for (String examplePlayer in examplePlayers) {
      int scoreKeep = 0;
      for (int i = 0; i < (totalRounds - 2); i++) {
        if (rng.nextInt(100) < 73) {
          scoreKeep += (10 + rng.nextInt(4));
        }

        rounds[i][examplePlayer] = scoreKeep;
        currentScores[examplePlayer] = scoreKeep;
      }
    }

  }

  void populateCallsExample() {
    calls.clear();
    for (int i = 0; i < totalRounds; i++) {
      var round = {};

      for (String examplePlayer in examplePlayers) {
        if (i < totalRounds - 1) {
          round[examplePlayer] = rng.nextInt((calculateSizeOfHand(totalRounds, i)/2).round());
        } else {
          round[examplePlayer] = 0;
        }
      }
      calls.add(round);
    }
  }

  void populateCallsWonExample() {
    callsWon.clear();
    for (int i = 0; i < totalRounds; i++) {
      var round = {};

      for (String examplePlayer in examplePlayers) {
        if (i < totalRounds - 1) {
          round[examplePlayer] = rng.nextInt((calculateSizeOfHand(totalRounds, i)/2).round());
        } else {
          round[examplePlayer] = 0;
        }
      }
      callsWon.add(round);
    }
  }

  void assignServerNameNumber() async {
    var randomNum = Random();
    int potentialServerNumber = randomNum.nextInt(999);
    String potentialServerName = serverName += potentialServerNumber.toString();

    List<String> serverList = [];
    await FirebaseFirestore.instance.collection('games').get()
        .then((QuerySnapshot querySnapshot) {
      for (var doc in querySnapshot.docs) {
        serverList.add(doc.id);
      }
    });

    while (true) {
      if (serverList.contains(potentialServerName)) {
        potentialServerNumber = randomNum.nextInt(999);
        potentialServerName = serverName += potentialServerNumber.toString();
      } else {
        break;
      }
    }
    serverName = potentialServerName;
  }

  Future<bool> checkServerNameDuplicate() async {
    List<String> serverList = [];
    await FirebaseFirestore.instance.collection('games').get()
        .then((QuerySnapshot querySnapshot) {
      for (var doc in querySnapshot.docs) {
        serverList.add(doc.id);
      }
    });

    if (serverList.contains(serverName)) {
      return true;
    } else {
      return false;
    }
  }

  // Deprecated. Old function.
  List lengthOfGame() {
    if (playerCount == 4) {
      return fourPlayersMap;
    } else if (playerCount == 5) {
      return fivePlayersMap;
    } else if (playerCount == 6) {
      return sixPlayersMap;
    } else if (playerCount == 7) {
      return sevenPlayersMap;
    } else if (playerCount == 8) {
      return eightPlayersMap;
    } else if (playerCount == 9) {
      return ninePlayersMap;
    } else if (playerCount == 10) {
      return tenPlayersMap;
    }
    return sixPlayersMap;
  }

  List<String> lengthOfGame2() {
    if (playerCount == 4) {
      return fourPlayersOptions;
    } else if (playerCount == 5) {
      return fivePlayersOptions;
    } else if (playerCount == 6) {
      return sixPlayersOptions;
    } else if (playerCount == 7) {
      return sevenPlayersOptions;
    } else if (playerCount == 8) {
      return eightPlayersOptions;
    } else if (playerCount == 9) {
      return ninePlayersOptions;
    } else if (playerCount == 10) {
      return tenPlayersOptions;
    }
    return sixPlayersOptions;
  }

  updateTotalRounds2(String gameLength)  {
    if (playerCount == 4) {
      totalRounds = 20;
    } else if (playerCount == 5) {
      totalRounds = 20;
    } else if (playerCount == 6) {
      totalRounds = 20;
    } else if (playerCount == 7) {
      if (gameLength == "9-to-1") {
        totalRounds = 18;
      } else {
        totalRounds = 20;
      }
    } else if (playerCount == 8) {
      if (gameLength == "8-to-1") {
        totalRounds = 16;
      } else if (gameLength == "9-to-1") {
        totalRounds = 18;
      } else {
        totalRounds = 20;
      }
    } else if (playerCount == 9) {
      if (gameLength == "7-to-1") {
        totalRounds = 14;
      } else {
        totalRounds = 16;
      }
    } else if (playerCount == 10) {
      if (gameLength == "6-to-1") {
        totalRounds = 12;
      } else {
        totalRounds = 14;
      }
    }
  }

  // This just sets the default number of rounds.
  updateDefaultTotalRounds3() {
    if (playerCount == 4) {
      gameLengthString = "10-to-1";
      totalRounds = 20;
    } else if (playerCount == 5) {
      gameLengthString = "10-to-1";
      totalRounds = 20;
    } else if (playerCount == 6) {
      gameLengthString = "10-to-1";
      totalRounds = 20;
    } else if (playerCount == 7) {
      gameLengthString = "9-to-1";
      totalRounds = 18;
    } else if (playerCount == 8) {
      gameLengthString = "8-to-1";
      totalRounds = 16;
    } else if (playerCount == 9) {
      gameLengthString = "7-to-1";
      totalRounds = 14;
    } else if (playerCount == 10) {
      gameLengthString = "6-to-1";
      totalRounds = 12;
    }
  }

  void addEmail(String email) {
    emailResults.add(email);
  }

  Future<void> save() async {
    await FirebaseFirestore.instance.collection('games').doc(serverName).set({
      'blindManRound' : blindManRound,
      'calls' : calls,
      'callsWon' : callsWon,
      'currentRound' : currentRound,
      'currentScores' : currentScores,
      'currentTrump' : currentTrump,
      'currentTurn' : currentTurn,
      'deck' : deckStrings,
      'emailResults' : emailResults,
      'firstCardsDealt' : firstCardsDealt,
      'firstPlayerNotified' : firstPlayerNotified,
      'futureCalls' : futureCalls,
      'futureCards' : futureCards,
      'gameComplete' : gameComplete,
      'hands' : hands,
      'hexVotes' : hexVotes,
      'hexVoted' : hexVoted,
      'hostName' : hostName,
      'lastTurnTime' : Timestamp.fromDate(DateTime.now()),
      'notificationTokens' : notificationTokens,
      'personHexed' : personHexed,
      'pileStuff' : pileStuff,
      'placeCards' : placeCards,
      'playerCount' : playerCount,
      'players' : players,
      'playersOrder' : playersOrder,
      'previousPile': previousPile,
      'previousPileOrdered': previousPileOrdered,
      'queueFull' : queueFull,
      'realGame' : realGame,
      'rounds' : rounds,
      'selectionHandKnown' : selectionHandKnown,
      'selectionHandUnknown' : selectionHandUnknown,
      'serverName' : serverName,
      'serverPassword' : serverPassword,
      'timeEnded' : null,
      'gameStarted' : false,
      'hostJoined' : false,
      'timeStarted' : Timestamp.fromDate(DateTime.now()),
      'totalRounds' : totalRounds,
      'turnCount' : turnCount,
      'turnTimes' : turnTimes,
      'turnsPassed' : turnsPassed,
      'twoHundredClub' : twoHundredClub,
      'versionStarted' : versionStarted,
    });
  }
}

class HostMenu extends StatefulWidget {
  HostMenu({Key? key}) : super(key: key);

  @override
  _HostMenuState createState() => _HostMenuState();
}

class _HostMenuState extends State<HostMenu> {
  final Model _model = Model();
  final _formKey = GlobalKey<FormState>();
  var duplicate = false;
  bool versionCheck = false;
  Box profileBox = Hive.box(ProfileKeys.profile);

  _HostMenuState();

  Model get model {
    return _model;
  }

  Future<void> updateHostName() async {
    model.hostName = await profileBox.get(ProfileKeys.profileName, defaultValue: null);
  }

  @override
  Widget build(BuildContext context) {
    int numPlayers = 6;
    updateHostName();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Create Game"),
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget> [
              Container(height: 12),
              TextFormField(
                initialValue: model.serverName,
                textCapitalization: TextCapitalization.words,
                maxLines: 1,
                minLines: 1,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Server Name",
                ),
                validator: (String? value) {
                  if (duplicate) {
                    return 'A server already exists with that name. Please enter another';
                  }
                  if (value == null || value.isEmpty) {
                    return 'please enter an option';
                  }
                  return null;
                },
                onChanged: (String? value) async {
                  model.serverName = value!;
                  final check = await _model.checkServerNameDuplicate();
                  setState(() {
                    duplicate = check;
                  });
                },
                onSaved: (String? value) {
                  model.serverName = value!;
                },
              ),
              Container(height: 12),
              TextFormField(
                initialValue: model.serverPassword,
                maxLines: 1,
                minLines: 1,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Server Password (optional)",
                ),
                validator: (String? value) {
                  // if (value == null || value.isEmpty) {
                  //   return 'please enter an option';
                  // }
                  return null;
                },
                onSaved: (String? value) {
                  model.serverPassword = value!;
                },
              ),
              Container(height: 12),
              Row(
                children: <Widget>[
                  Expanded(child:
                    // put the new one here
                  DropdownButtonFormField(
                    value: numPlayers,
                    decoration: const InputDecoration(
                        labelText: "Number of Players",
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey, width: 1.0)
                        ),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey, width: 1.0)
                        )
                    ),
                    onChanged: (int? newNumPlayers) {
                      setState(() {
                        model.playerCount = newNumPlayers!;
                        model.updateDefaultTotalRounds3();
                      });
                    },
                    items: model.numPlayerOptions
                        .map<DropdownMenuItem<int>>((int value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Text(value.toString()),
                      );
                    }).toList(),
                  )
                  ),
                  Container(width: 12),
                  Expanded(child:
                      DropdownButtonFormField(
                        value: model.gameLengthString,
                        decoration: const InputDecoration(
                          labelText: "Length of Game",
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey, width: 1.0)
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey, width: 1.0)
                          )
                        ),
                        onChanged: (String? newGameLength) {
                          setState(() {
                            model.updateTotalRounds2(newGameLength!);
                          });
                        },
                        items: model.lengthOfGame2()
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      )
                  )
                ],
              ),

              Container(height: 12),

              Text(
                gameRules(model.playerCount, model.totalRounds),
                textAlign: TextAlign.center,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),

              // Blind man's game setting. Add in future update.
              /*
              Container(height: 12),

              CheckboxListTile(
                  value: model.blindManRound,
                  title: const Text("Blind Man's Game"),
                  subtitle: const Text("Only applies to second 1-deal"),
                  secondary: const Icon(Icons.visibility_off),
                  activeColor: const Color(0xFF58A939),
                  checkColor: Colors.white,
                  selected: model.blindManRound,
                  onChanged: (bool? value) {
                    setState(() {
                      model.blindManRound = value!;
                    });
                  }
                  ),

               */

              Container(height: 50),

              TextFormField(
                maxLines: 1,
                minLines: 1,
                autocorrect: false,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "E-mail Results (optional)",
                ),
                keyboardType: TextInputType.emailAddress,
                validator: (String? value) {
                  // if (value == null || value.isEmpty) {
                  //   return 'please enter an option';
                  // }
                  return null;
                },
                onSaved: (String? value) {
                  if (value != "") {
                    model.addEmail(value!);
                  }
                },
              ),

              Container(height: 20),

              /*
              // Fill example data option.
              CheckboxListTile(
                  value: model.fillExampleData,
                  title: const Text("Fill example data"),
                  subtitle: const Text("(Debug use only)"),
                  secondary: const Icon(Icons.adb),
                  activeColor: Colors.green,
                  checkColor: Colors.white,
                  selected: model.fillExampleData,
                  onChanged: (bool? value) {
                    setState(() {
                      model.fillExampleData = value!;
                    });
                  }
              ),
               */

              /*
              // Fill example data option.
              CheckboxListTile(
                  value: model.fillExampleData,
                  title: const Text("Fill example data"),
                  subtitle: const Text("(Debug use only)"),
                  secondary: const Icon(Icons.adb),
                  activeColor: Colors.green,
                  checkColor: Colors.white,
                  selected: model.fillExampleData,
                  onChanged: (bool? value) {
                    setState(() {
                      model.fillExampleData = value!;
                    });
                  }
              ),
               */

            ],
          )
        )
      ),

      floatingActionButton: FloatingActionButton(
        backgroundColor:  const Color(0xDDA10702),
        onPressed: () async {
          versionCheck = await checkLatestVersion();
          if (!versionCheck) {
            Fluttertoast.showToast(
              msg: "App needs to be updated to continue!",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              fontSize: 16.0,
            );
          } else if (!await model.accountCreated()) {
            Fluttertoast.showToast(
              msg: "You aren't logged in.",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              fontSize: 16.0,
            );
          } else {
            if (_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              _model.populateFields();
              _model.save();
              Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => QueueMenu(serverName: _model.serverName)
                  )
              );
            }
          }
        },
        child: const Icon(Icons.done,
        color: Colors.white,),
      ),

    );
  }
}
