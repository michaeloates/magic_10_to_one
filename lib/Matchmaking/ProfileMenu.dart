import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:magic_10_to_one/Matchmaking/ArchivedGames.dart';
import 'package:magic_10_to_one/Matchmaking/ScoresBreakdown.dart';
import 'package:magic_10_to_one/enums/cards.dart';
import 'package:crypt/crypt.dart';
import 'package:encrypt/encrypt.dart' as encrypt;


Future<bool> removeAccountFirebase(String name) async {
  try {
    DocumentReference ref = FirebaseFirestore.instance.collection('players').doc(name);
    try {
      await ref.delete();
      return true;
    } catch (e) {
      debugPrint("Couldn't find password for the player");
      return false;
    }
  } catch (e) {
    debugPrint("Couldn't find username in player list");
    return false;
  }
}

Future<bool> checkProfilePasswordCombo(String password, String name) async {
  String firePassword = "";
  String iv = "";
  String salt = await retreiveSalt();
  String hashedPassword = await Crypt.sha256(password, rounds: 956, salt: salt).toString();
  try {
    DocumentReference ref = FirebaseFirestore.instance.collection('players').doc(name);
    try {
      await ref.get().then((snapshot) {
        firePassword = snapshot.get('password');
        iv = snapshot.get('iv');
      });
      String fireKey = await retreiveKey();
      final key = encrypt.Key.fromUtf8(fireKey);
      final encrypter = encrypt.Encrypter(encrypt.AES(key));
      final decryptedPassword = encrypter.decrypt(encrypt.Encrypted.fromBase64(firePassword), iv: encrypt.IV.fromBase64(iv));

      if (decryptedPassword == hashedPassword && firePassword.isNotEmpty) {
        return true;
      } else {
        debugPrint("Password: $password does not match firebase password");
        return false;
      }
    } catch (e) {
      debugPrint("Couldn't find password for the player");
      return false;
    }
  } catch (e) {
    debugPrint("Couldn't find username in player list");
    return false;
  }
}

Future<String> retreiveKey() async {
  String key = "";
  try  {
    DocumentReference ref = FirebaseFirestore.instance.collection('Keys').doc('symmetric1');
    await ref.get().then((snapshot) {
      key = snapshot.get('key');
    });
  } catch (e) {
    debugPrint("Error in loading key from firebase. possible incorrect format or doesn't exist");
  }
  return key;
}

Future<String> retreiveSalt() async {
  String key = "";
  try  {
    DocumentReference ref = FirebaseFirestore.instance.collection('Keys').doc('symmetric1');
    await ref.get().then((snapshot) {
      key = snapshot.get('salt');
    });
  } catch (e) {
    debugPrint("Error in loading key from firebase. possible incorrect format or doesn't exist");
  }
  return key;
}

class Profile {
  String name;
  String password;
  int highestScore = 0;
  int lowestScore = 0;
  int losses = 0;
  int wins = 0;
  int twoHundredCount = 0;

  Map<String, int> highestScores = {
    "12" : 0,
    "14" : 0,
    "16" : 0,
    "18" : 0,
    "20" : 0,
  };
  Map<String, int> lowestScores = {
    "12" : 0,
    "14" : 0,
    "16" : 0,
    "18" : 0,
    "20" : 0,
  };

  Profile({this.name = "", this.password = ""});

  Future<bool> checkProfileNameDuplicate() async {
    List<String> playerList = [];
    await FirebaseFirestore.instance.collection('players').get()
        .then((QuerySnapshot querySnapshot) {
      for (var doc in querySnapshot.docs) {
        playerList.add(doc.id);
      }
    });

    // TODO - Update this to ignore capitalisation.
    if (playerList.contains(name)) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> save() async {
    String fireKey = await retreiveKey();
    String salt = await retreiveSalt();
    String hashedPassword = await Crypt.sha256(password, rounds: 956, salt: salt).toString();

    final key = encrypt.Key.fromUtf8(fireKey);
    final encrypter = encrypt.Encrypter(encrypt.AES(key));
    final encrypt.IV iv = encrypt.IV.fromLength(16);
    final encrypt.Encrypted encryptedPassword = encrypter.encrypt(hashedPassword, iv: iv);
    String uploadPassword = encryptedPassword.base64;
    String uploadIv = iv.base64;

    var profileBox = Hive.box(ProfileKeys.profile);
    profileBox.put(ProfileKeys.profileName, name);
    profileBox.put(ProfileKeys.profilePassword, uploadPassword);
    profileBox.put("iv", uploadIv);
    profileBox.put(ProfileKeys.profileHighestScore, highestScore);
    profileBox.put(ProfileKeys.profileLowestScore, lowestScore);
    profileBox.put(ProfileKeys.profileLosses, losses);
    profileBox.put(ProfileKeys.profileWins, wins);
    await FirebaseFirestore.instance.collection('players').doc(name).set({
      "password" : uploadPassword,
      "wins" : wins,
      "losses" : losses,
      "highestScore" : highestScore,
      "lowestScore" : lowestScore,
      "iv" : uploadIv,
      "highestScores" : highestScores,
      "lowestScores" : lowestScores,
      "twoHundredCount" : twoHundredCount,
    });
  }

  Future<void> load() async {
    try  {
      DocumentReference ref = FirebaseFirestore.instance.collection('players').doc(name);
      await ref.get().then((snapshot) {
        highestScore = snapshot.get('highestScore');
        lowestScore = snapshot.get('lowestScore');
        losses = snapshot.get('losses');
        wins = snapshot.get('wins');
        highestScores = Map<String, int>.from(snapshot.get('highestScores'));
        lowestScores = Map<String, int>.from(snapshot.get('lowestScores'));
        twoHundredCount = snapshot.get('twoHundredCount');
      });
    } catch (e) {
      debugPrint("Error in loading profile data from firebase. possible incorrect format or doesn't exist");
    }
  }
}

class ProfileMenu extends StatefulWidget {
  const ProfileMenu({Key? key}) : super(key: key);

  @override
  _ProfileMenuState createState() => _ProfileMenuState();
}

class _ProfileMenuState extends State<ProfileMenu> {
  final profileBox = Hive.box(ProfileKeys.profile);
  final _formKey = GlobalKey<FormState>();
  final _formKeyLogin = GlobalKey<FormState>();
  final Profile _profile = Profile();
  bool duplicate = false;
  final titles = ['Name', 'Highest Score', 'Lowest Score', 'Losses', 'Wins', '200 Counter'];
  final icons = [Icons.person, Icons.sports_score, Icons.sports_score, Icons.sports_score, Icons.sports_score, Icons.sports_score,];
  List<dynamic> scoreValues = ["", "", "", "", "", ""];
  String tempPassword = "";
  final TextEditingController _textFieldController = TextEditingController();
  bool stateSet = false;

  Future<bool> _deleteAccountHandle(BuildContext context, String name) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Password required'),
            content: Container(
              height: MediaQuery.of(context).size.height / 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RichText(
                    text: const TextSpan(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(text: 'Are you sure you want to delete '),
                        TextSpan(text: 'all ', style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(text: 'of your data? Please note that once destroyed, all user data will '),
                        TextSpan(text: 'not be recoverable.', style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  TextField(
                    obscureText: true,
                    autocorrect: false,
                    obscuringCharacter: '*',
                    onChanged: (value) {
                      setState(() {
                        tempPassword = value;
                      });
                    },
                    controller: _textFieldController,
                    decoration: const InputDecoration(hintText: "Enter password"),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: const Color(0xFF256D1B),
                    ),
                    onPressed: () {
                      Navigator.pop(context, false);
                      },
                    child: const Text('No'),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: const Color(0xFFA10702),
                    ),
                    onPressed: () async {
                      bool correct = await checkProfilePasswordCombo(tempPassword, name);
                      if (correct) {
                        bool deleted = await removeAccountFirebase(name);
                        if (deleted) {
                          Navigator.pop(context, true);
                        } else {
                          Navigator.pop(context, false);
                        }
                      } else {
                        Navigator.pop(context, false);
                      }
                    },
                    child: const Text('Yes'),
                  ),
                ],
              ),

            ],
          );
        }) ?? false;
  }

  void scoreValuesUpdater() async {
    int highScore = 0;
    int lowScore = 0;
    int losses = 0;
    int wins = 0;
    int twoHundredCount = 0;

    try  {
      DocumentReference ref = FirebaseFirestore.instance.collection('players').doc(profileBox.get(ProfileKeys.profileName));
      await ref.get().then((snapshot) {
        highScore = snapshot.get('highestScore');
        lowScore = snapshot.get('lowestScore');
        losses = snapshot.get('losses');
        wins = snapshot.get('wins');
        twoHundredCount = snapshot.get('twoHundredCount');
      });
    } catch (e) {
      debugPrint("Error in loading profile data from firebase. possible incorrect format or doesn't exist");
    }

    scoreValues.clear();
    scoreValues.add(profileBox.get(ProfileKeys.profileName, defaultValue: ""));

    scoreValues.add(highScore.toString());
    scoreValues.add(lowScore.toString());
    scoreValues.add(losses.toString());
    scoreValues.add(wins.toString());
    scoreValues.add(twoHundredCount.toString());

    if (!stateSet) {
      setState(() {});
      stateSet = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    bool created = profileBox.get(ProfileKeys.profileCreated, defaultValue: false);
    if (created) {
      scoreValuesUpdater();
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile"),
      ),
      backgroundColor: const Color(0xFF256D1B),
      body: !created ? Container(
        padding: const EdgeInsets.all(10),
        child: ListView (
          children: <Widget> [
            Form(
                key: _formKey,
                child: Column(
                  children: <Widget> [
                    Container(height: 12),
                    Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.white,
                              width: 3.0,
                            ),
                            color: const Color(0xFFFAA613),
                            borderRadius: BorderRadius.all(Radius.circular(20))
                        ),
                        height: 60,
                        child:
                        const Text(
                          "Create Profile", style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w500,
                          color: Colors.white,
                        ),
                        )
                    ),
                    Container(height: 20),
                    TextFormField(
                      initialValue: _profile.name,
                      maxLines: 1,
                      minLines: 1,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        filled: true,
                        errorStyle: TextStyle(
                          color: const Color(0xFFA10702),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 3,
                            color: Color(0xFFA10702),
                          ),
                        ),
                        fillColor: Colors.white,
                        labelText: "Profile Name",
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        labelStyle: TextStyle(
                          color: Color(0xFF4D4D4D),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 3,
                            color: Color(0xFFFAA613),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 2,
                            color: Color(0xFFFAA613),
                          ),
                        ),
                      ),
                      validator: (String? value) {
                        if (value == "default") {
                          return 'please change the name from the default';
                        }
                        if (duplicate) {
                          return 'A user already exists with that name. Please enter another';
                        }
                        if (value == null || value.isEmpty) {
                          return 'please enter an option';
                        }
                        if (value.contains(' ')) {
                          return 'please enter one word';
                        }
                        if (value.length >= 14) {
                          return 'please enter a username shorter than 14 characters';
                        }
                        return null;
                      },
                      onChanged: (String? value) async {
                        _profile.name = value!;
                        final check = await _profile.checkProfileNameDuplicate();
                        setState(() {
                          duplicate = check;
                        });
                      },
                      onSaved: (String? value) {
                        _profile.name = value!;
                      },
                    ),
                    Container(
                      height: 15,
                    ),
                    TextFormField(
                      initialValue: _profile.password,
                      maxLines: 1,
                      minLines: 1,
                      obscureText: true,
                      autocorrect: false,
                      obscuringCharacter: '*',
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        errorStyle: TextStyle(
                          color: Color(0xFFA10702),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 3,
                            color: Color(0xFFA10702),
                          ),
                        ),
                        filled: true,
                        fillColor: Colors.white,
                        labelText: "Profile Password",
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        labelStyle: TextStyle(
                          color: Color(0xFF4D4D4D),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 3,
                            color: Color(0xFFFAA613),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 2,
                            color: Color(0xFFFAA613),
                          ),
                        ),
                      ),
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'please enter an option';
                        }
                        if (value.contains(' ')) {
                          return 'please enter one word';
                        }
                        return null;
                      },
                      onChanged: (String? value) async {
                        _profile.password = value!;
                      },
                      onSaved: (String? value) {
                        _profile.password = value!;
                      },
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      height: 50.0,
                      width: 150.0,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: const Color(0xFFFAA613),
                            onPrimary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                                side: const BorderSide(
                                  color: Colors.white,
                                  width: 2.0,
                                )
                            )
                        ),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            profileBox.put(ProfileKeys.profileName, _profile.name);
                            profileBox.put(ProfileKeys.profileCreated, true);
                            _formKey.currentState!.save();
                            _profile.save();
                            Fluttertoast.showToast(
                              msg: "Profile Created Successfully",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              fontSize: 16.0,
                            );
                            Navigator.of(context).pop();
                          }
                        },
                        child: const Text("Continue",
                            style: TextStyle(fontSize: 15)
                        ),
                      )
                    ),
                  ]),
            ),
            Container(
              height: 10,
            ),
            Form(
                key: _formKeyLogin,
                child: Column(
                    children: <Widget> [
                      Container(height: 12),
                      Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.white,
                                width: 3.0,
                              ),
                              color: const Color(0xFF550527),
                              borderRadius: const BorderRadius.all(Radius.circular(20))
                          ),
                          height: 60,
                          child:
                          const Text(
                            "Login", style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                          )
                      ),
                      Container(
                        height: 20,
                      ),
                      TextFormField(
                        initialValue: _profile.name,
                        maxLines: 1,
                        minLines: 1,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          filled: true,
                          fillColor: Colors.white,
                          labelText: "Profile Name",
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          errorStyle: TextStyle(
                            color: Color(0xFFA10702),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 3,
                              color: Color(0xFFA10702),
                            ),
                          ),
                          labelStyle: TextStyle(
                            color: Color(0xFF4D4D4D),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 3,
                              //color: Colors.grey,
                              color: Color(0xFF550527),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 2,
                              color: Color(0xFF550527),
                            ),
                          ),
                        ),
                        validator: (String? value) {
                          if (value == "default") {
                            return 'please change the name from the default';
                          }
                          if (value == null || value.isEmpty) {
                            return 'please enter an option';
                          }
                          if (value.contains(' ')) {
                            return 'please enter one word';
                          }
                          return null;
                        },
                        onChanged: (String? value) async {
                          _profile.name = value!;
                        },
                        onSaved: (String? value) {
                          _profile.name = value!;
                        },
                      ),
                      Container(
                        height: 15,
                      ),
                      TextFormField(
                        initialValue: _profile.password,
                        maxLines: 1,
                        minLines: 1,
                        obscureText: true,
                        obscuringCharacter: '*',
                        autocorrect: false,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          fillColor: Colors.white,
                          filled: true,
                          labelText: "Profile Password",
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          labelStyle: TextStyle(
                            color: Color(0xFF4D4D4D),
                          ),
                          errorStyle: TextStyle(
                            color: Color(0xFFA10702),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 3,
                              color: Color(0xFFA10702),
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 3,
                              color: Color(0xFF550527),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 2,
                              color: Color(0xFF550527),
                            ),
                          ),
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'please enter an option';
                          }
                          if (value.contains(' ')) {
                            return 'please enter one word';
                          }
                          return null;
                        },
                        onChanged: (String? value) async {
                          _profile.password = value!;
                        },
                        onSaved: (String? value) {
                          _profile.password = value!;
                        },
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        height: 50.0,
                        width: 150.0,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: const Color(0xFF550527),
                            onPrimary: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                              side: const BorderSide(
                                color: Colors.white,
                                width: 2.0,
                              )
                            )
                          ),
                          onPressed: () async {
                            if (_formKeyLogin.currentState!.validate()) {
                              final check = await checkProfilePasswordCombo(_profile.password, _profile.name);
                              if (check) {
                                profileBox.put(ProfileKeys.profileName, _profile.name);
                                profileBox.put(ProfileKeys.profileCreated, true);
                                _formKeyLogin.currentState!.save();
                                await _profile.load();
                                await _profile.save();
                                Fluttertoast.showToast(
                                  msg: "Login Successfully",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  fontSize: 16.0,
                                );
                                Navigator.of(context).pop();
                              } else {
                                Fluttertoast.showToast(
                                  msg: "Login Failed: Incorrect username or password",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  fontSize: 16.0,
                                );
                              }
                            }
                          },
                          child: const Text("Continue",
                              style: TextStyle(fontSize: 15)
                          ),
                        )
                      ),
                    ]
                )
            )
          ]
        )
      ) :
      Column(
        children: [
          Container(height: 12),
          Expanded(
            flex: 2,
            child: ListView.builder(
              itemCount: titles.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    child: ListTile(
                      leading: Icon(icons[index]),
                      title: Text(
                        titles[index],
                        style: const TextStyle(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      trailing: Text(scoreValues[index]),
                    )
                );
              },
            ),
          ),

          Container(height: 5),

          Expanded(
              flex: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 0,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(00, 0, 20, 0),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.white,
                              width: 3.0,
                            ),
                            color: const Color(0xFFFAA613),
                            borderRadius: const BorderRadius.all(Radius.circular(10))
                        ),
                        width: MediaQuery.of(context).size.width / 3,
                        child: TextButton(
                          onPressed: () async {
                            Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) => const ScoresBreakdown()
                                )
                            );
                          },
                          child: const Text(
                            "Scores Breakdown",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),

                  Expanded(
                    flex: 0,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.white,
                              width: 3.0,
                            ),
                            color: const Color(0xFF550527),
                            borderRadius: const BorderRadius.all(Radius.circular(10))
                        ),
                        width: MediaQuery.of(context).size.width / 3,
                        child: TextButton(
                          onPressed: () async {
                            Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) => const ArchivedGames()
                                )
                            );
                          },
                          child: const Text(
                            "Archived Games",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
          ),


          Container(height: 20),


          Expanded(
            flex: 0,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 3.0,
                    ),
                    color: const Color(0xFFA10702),
                    borderRadius: const BorderRadius.all(Radius.circular(10))
                ),
                width: MediaQuery.of(context).size.width / 3,
                child: TextButton(
                  onPressed: () async {
                    bool userWantsDelete = await _deleteAccountHandle(context, profileBox.get(ProfileKeys.profileName, defaultValue: ""));
                    if (userWantsDelete) {
                      profileBox.clear();
                      Navigator.of(context).pop();
                    } else {
                      Fluttertoast.showToast(
                        msg: "Account not deleted",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        fontSize: 16.0,
                      );
                    }
                    tempPassword = "";
                    _textFieldController.clear();
                  },
                  child: const Text(
                    "Delete Account",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ),



          const Expanded(
            flex: 1,
            child: SizedBox(),
          )
        ],
      ),

      floatingActionButton: !created ? FloatingActionButton(
        heroTag: "btn2",
        backgroundColor: const Color(0xDD550527),
        shape: const StadiumBorder(
          side: BorderSide(
            color: Colors.white,
            width: 2.0,
          ),
        ),
        onPressed: () async {
          if (_formKeyLogin.currentState!.validate()) {
            final check = await checkProfilePasswordCombo(_profile.password, _profile.name);
            if (check) {
              profileBox.put(ProfileKeys.profileName, _profile.name);
              profileBox.put(ProfileKeys.profileCreated, true);
              _formKeyLogin.currentState!.save();
              await _profile.load();
              await _profile.save();
              Fluttertoast.showToast(
                msg: "Login Successfully",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                fontSize: 16.0,
              );
              Navigator.of(context).pop();
            } else {
              Fluttertoast.showToast(
                msg: "Login Failed: Incorrect username or password",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                fontSize: 16.0,
              );
            }
          }
        },
        child: const Icon(
          Icons.check,
          color: Colors.white,
        ),
      ) : Column (
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget> [
          Container(
            height: 25,
          ),
          FloatingActionButton(
            heroTag: "btn1",
            backgroundColor: const Color(0xFFF44708),
            shape: const StadiumBorder(
              side: BorderSide(
                color: Colors.white,
                width: 2.0,
              ),
            ),
            onPressed: () {
              profileBox.clear();
              Navigator.of(context).pop();
            },
            child: const Icon(Icons.logout, color: Colors.white,),
          ),
        ],
      )
    );
  }
}
