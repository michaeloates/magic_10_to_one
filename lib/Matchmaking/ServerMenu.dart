import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:magic_10_to_one/Models/GameUtilities.dart';
import 'package:magic_10_to_one/enums/cards.dart';

import 'Queue.dart';

class ServerMenu extends StatefulWidget {
  const ServerMenu({Key? key}) : super(key: key);

  @override
  _ServerMenuState createState() => _ServerMenuState();
}



class _ServerMenuState extends State<ServerMenu> {
  final TextEditingController _textFieldController = TextEditingController();
  String tempPassword = "";
  List<QueryDocumentSnapshot<Object?>> docs = [];
  Box profileBox = Hive.box(ProfileKeys.profile);
  String playerName = "";
  int minServerVersion = 9999;

  Future<bool> _displayPasswordInput(BuildContext context, String password) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('Password required'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  tempPassword = value;
                });
              },
              controller: _textFieldController,
              decoration: const InputDecoration(hintText: "Enter password"),
            ),
            actions: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: const Color(0xFF47A025),
                    onPrimary: Colors.white,
                ),
                onPressed: () {
                  if (tempPassword == password) {
                    tempPassword = "";
                    Navigator.pop(context, true);
                  } else {
                    tempPassword = "";
                    Navigator.pop(context, false);
                  }
                },
                child: const Text("Continue",
                ),
              )
            ],
          );
        });
  }

  void setName() async {
    playerName = await profileBox.get(ProfileKeys.profileName, defaultValue: null);
  }

  Future findMinVersion() async {
    DocumentReference ref = FirebaseFirestore.instance.collection('Keys').doc('version');

    await ref.get().then((snapshot) {
      minServerVersion = snapshot.get('minServerVersion');
    });
  }

  void retrieveDocs(AsyncSnapshot<QuerySnapshot> snap) {
    // do same thing in queue. Set game started once order selection started and then head there immediately.
    docs.clear();
    if (snap.data?.docs != null) {
      for (QueryDocumentSnapshot<Object?> doc in snap.data!.docs) {

        try {
          if (!doc.get('gameStarted')) {
            docs.add(doc);
          }
        } catch (e) {
          //debugPrint("This server does not have the attribute: gameStarted");
        }
      }
    }
  }

  @override
  void initState()  {
    super.initState();
    setName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Server List")
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('games').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            retrieveDocs(snapshot);
            return ListView.builder(
                shrinkWrap: true,
                itemCount: docs.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return Card (
                      elevation: 0,
                      margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                      child: GestureDetector(
                        onTap: () async {
                          List<dynamic> players = await docs[index].get('players');
                          int currPlayers = await docs[index].get('players').length;
                          int properPlayers = await docs[index].get('playerCount');
                          bool started = await docs[index].get('gameStarted');
                          String serverPassword = await docs[index].get('serverPassword');
                          bool serverFull = currPlayers >= properPlayers;
                          int serverVersion = await docs[index].get('versionStarted');
                          await findMinVersion();
                          bool serverVersionLoading = minServerVersion == 9999;
                          bool serverVersionCheck = serverVersion >= minServerVersion;
                          bool versionCheck = await checkLatestVersion();
                          bool serverContainsPlayer = players.contains(playerName);

                          if (!versionCheck) {
                            Fluttertoast.showToast(
                              msg: "App needs to be updated to continue!",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              fontSize: 16.0,
                            );
                          } else if (serverVersionLoading) {
                            Fluttertoast.showToast(
                              msg: "Loading, please try again.",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              fontSize: 16.0,
                            );
                          } else if (!serverVersionCheck) {
                            Fluttertoast.showToast(
                              msg: "Server is outdated. Please start a new game.",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              fontSize: 16.0,
                            );
                          } else if (!serverFull || serverContainsPlayer) {
                            if (!started) {
                              if (serverPassword.isEmpty) {
                                Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => QueueMenu(serverName: docs[index].id)
                                    )
                                );
                              } else {
                                bool correct = await _displayPasswordInput(context, serverPassword);
                                if (correct) {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) => QueueMenu(serverName: docs[index].id)
                                      )
                                  );
                                } else {
                                  Fluttertoast.showToast(
                                    msg: "Wrong password",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1,
                                    fontSize: 16.0,
                                  );
                                  setState(() {});
                                }
                              }
                            } else {
                              Fluttertoast.showToast(
                                msg: "Server already started, go to rejoin to join back",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                fontSize: 16.0,
                              );
                              setState(() {});
                            }
                          } else {
                            Fluttertoast.showToast(
                              msg: "Server is full. Sorry!",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              fontSize: 16.0,
                            );
                            setState(() {});
                          }
                        },
                        child: Container(
                            decoration: BoxDecoration(
                                color: const Color(0xFFF44708),
                                border: Border.all(
                                  color: Colors.white,
                                ),
                                borderRadius: const BorderRadius.all(Radius.circular(20))
                            ),
                            child: ListTile(
                              leading: const Icon(Icons.dns, color: Colors.white,),
                              title: Text(docs[index].id, style: const TextStyle(color: Colors.white, fontSize: 20),),
                              trailing: Text(docs[index].get('players').length.toString(),style: const TextStyle(
                                color: Colors.white,
                              ),),
                            ),
                        ),
                      )
                  );
                }
            );
          } else {
            return const Text("No servers available");
          }
        }
      ),
    );
  }
}

