

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import '../enums/cards.dart';

class ScoresBreakdown extends StatefulWidget {
  const ScoresBreakdown({Key? key}) : super(key: key);


  @override
  _ScoresBreakdownState createState() => _ScoresBreakdownState();
}

class _ScoresBreakdownState extends State<ScoresBreakdown>{
  final profileBox = Hive.box(ProfileKeys.profile);
  final titles = [
    'High Scores:', '10 to 1', '9 to 1', '8 to 1', '7 to 1', '6 to 1',
    'Low Scores:', '10 to 1', '9 to 1', '8 to 1', '7 to 1', '6 to 1',
  ];
  List<dynamic> ScoreValues = ["", "", "", "", "", "", "", "", "", "", "", "",];
  List<dynamic> tileColors = [
    const Color(0xFFFAA613), Colors.white, Colors.white, Colors.white, Colors.white, Colors.white,
    const Color(0xFFA10702), Colors.white, Colors.white, Colors.white, Colors.white, Colors.white,
  ];
  List<dynamic> textColors = [
    Colors.white, Colors.black, Colors.black, Colors.black, Colors.black, Colors.black,
    Colors.white, Colors.black, Colors.black, Colors.black, Colors.black, Colors.black,
  ];
  bool stateSet = false;

  void scoreValuesUpdater() async {
    Map<String, dynamic> highestScores = {};
    Map<String, dynamic> lowestScores = {};
    int highScore20 = 0;
    int highScore18 = 0;
    int highScore16 = 0;
    int highScore14 = 0;
    int highScore12 = 0;
    int lowScore20 = 0;
    int lowScore18 = 0;
    int lowScore16 = 0;
    int lowScore14 = 0;
    int lowScore12 = 0;

    try  {
      DocumentReference ref = FirebaseFirestore.instance.collection('players').doc(profileBox.get(ProfileKeys.profileName));
      await ref.get().then((snapshot) {
        highestScores = snapshot.get('highestScores');
        lowestScores = snapshot.get('lowestScores');
      });
    } catch (e) {
      debugPrint("Error in loading profile data from firebase. possible incorrect format or doesn't exist");
    }

    highScore20 = highestScores["20"];
    highScore18 = highestScores["18"];
    highScore16 = highestScores["16"];
    highScore14 = highestScores["14"];
    highScore12 = highestScores["12"];
    lowScore20 = lowestScores["20"];
    lowScore18 = lowestScores["18"];
    lowScore16 = lowestScores["16"];
    lowScore14 = lowestScores["14"];
    lowScore12 = lowestScores["12"];

    ScoreValues.clear();

    ScoreValues.add("");
    ScoreValues.add(highScore20.toString());
    ScoreValues.add(highScore18.toString());
    ScoreValues.add(highScore16.toString());
    ScoreValues.add(highScore14.toString());
    ScoreValues.add(highScore12.toString());
    ScoreValues.add("");
    ScoreValues.add(lowScore20.toString());
    ScoreValues.add(lowScore18.toString());
    ScoreValues.add(lowScore16.toString());
    ScoreValues.add(lowScore14.toString());
    ScoreValues.add(lowScore12.toString());

    if (!stateSet) {
      setState(() {});
      stateSet = true;
    }
  }


  @override
  Widget build(BuildContext context) {
    scoreValuesUpdater();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Scores Breakdown"),
      ),
      backgroundColor: const Color(0xFF256D1B),
      body: Column(
        children: [
          Expanded(
            flex: 3,
            child: ListView.builder(
              itemCount: titles.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    child: ListTile(
                      tileColor: tileColors[index],
                      textColor: textColors[index],
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                      ),
                      leading: const Icon(Icons.sports_score),
                      title: Text(
                        titles[index],
                        style: const TextStyle(
                        fontWeight: FontWeight.w500,
                      ),
                      ),
                      trailing: Text(ScoreValues[index]),
                    )
                );
              },
            ),
          ),
        ],
      ),


    );
  }
}