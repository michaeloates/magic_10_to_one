import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:magic_10_to_one/Models/Decks/Deck.dart';
import 'package:magic_10_to_one/Models/pushNotificationUtilities.dart';
import 'package:magic_10_to_one/enums/cards.dart';
import 'package:magic_10_to_one/Models/GameUtilities.dart';

import '../game/PlayScreen.dart';

class QueueMenu extends StatefulWidget {
  final String serverName;
  const QueueMenu({Key? key, required this.serverName}) : super(key: key);


  @override
  _QueueMenuState createState() => _QueueMenuState();
}

class _QueueMenuState extends State<QueueMenu> {
  StreamController<DocumentSnapshot> streamController = new StreamController.broadcast();
  late Stream<DocumentSnapshot> _serverStream;
  bool ready = false;
  bool hostJoined = false;
  List<String> players = [];
  Box profileBox = Hive.box(ProfileKeys.profile);
  late String? notificationToken;
  bool sentHostNotification = false;

  @override
  void initState() {
    super.initState();
    // here it should try and add the player to the queue
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _playerUpdateHandler();
    });
  }

  // Also in this, I'm adding 200 status upload...
  // Might also add the currentScores thingo...
  Future<void> _handleNotificationTokenUpload(String name) async {
    Map<String, dynamic> notificationTokens = {};

    Map<String, dynamic> currentScores = {};

    Map<String, dynamic> twoHundredClub = {};
    bool twoHundredStatus = false;
    int twoHundredCount = -1;

    await FirebaseMessaging.instance.getToken().then((value) {
      notificationToken = value;
    });

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    DocumentReference playerRef = FirebaseFirestore.instance.collection('players').doc(name);
    await ref.get().then((snapshot) {
      currentScores = snapshot.get('currentScores');
      notificationTokens = snapshot.get('notificationTokens');
      twoHundredClub = snapshot.get('twoHundredClub');
    });

    await playerRef.get().then((snapshot) {
      twoHundredCount = snapshot.get('twoHundredCount');
    });

    currentScores[name] = 0;

    while(twoHundredCount == -1) {
      // wait
    }

    if (twoHundredCount > 0) {
      twoHundredStatus = true;
    }

    twoHundredClub[name] = twoHundredStatus;

    notificationTokens[name] = notificationToken;

    await ref.update({
      "currentScores" : currentScores,
      "notificationTokens" : notificationTokens,
      "twoHundredClub" : twoHundredClub,
    });
  }

  Future<bool> amIHost() async {
    var hostName;
    var playerName;

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    await ref.get().then((snapshot) {
      hostName = snapshot.get('hostName');
    });

    playerName = await profileBox.get(ProfileKeys.profileName, defaultValue: null);

    if (hostName == playerName) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> _sendHostNotification(String hostName) async {
    if (sentHostNotification) {
      return;
    }

    Map<String, dynamic> notificationTokens = {};
    String hostNotificationToken = "";
    String serverName = widget.serverName;

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    await ref.get().then((snapshot) {
      notificationTokens = snapshot.get('notificationTokens');
    });

    notificationTokens.forEach((key, value) {
      if (key == hostName) {
        hostNotificationToken = value;
      }
    });

    sendNotification(hostNotificationToken, "Magic 10 to 1", "Please rejoin $serverName, it's time to start!", widget.serverName, NotificationSounds.nextTurn, 0, serverName, 1);
    sentHostNotification = true;
  }

  _playerRemoveHandler() async {
    var tempPlayers = [];
    bool started = false;
    var playerName = await profileBox.get(ProfileKeys.profileName, defaultValue: null);

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    // handle not removing player if the game is started
    await ref.get().then((snapshot) {
      started = snapshot.get('gameStarted');
      tempPlayers = snapshot.get('players');
    });

    if (started) {
      return;
    }

    // handle removing the player and giving host to someone else if present
    playerName != null ? tempPlayers.remove(playerName) : null;

    // I'm getting rid of this host change stuff. Host will always stay the
    // same, whoever is starts the game is host.
    /*
    if (hostName != null) {
      if (playerName == hostName) {
        if (tempPlayers.isNotEmpty) {
          var random = Random();
          var ran = random.nextInt(tempPlayers.length);
          await ref.update(
              {'hostName' : tempPlayers[ran]}
          );
          Fluttertoast.showToast(
            msg: "Host changed to ${tempPlayers[ran]}",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            fontSize: 16.0,
          );
        }
      } else {
        await ref.update(
            {'hostName' : ""}
        );
      }
    }
     */

    await ref.update(
        {'players' : tempPlayers}
    );
  }

  _playerUpdateHandler() async {
    List<dynamic> tempPlayers = [];

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    await ref.get().then((snapshot) {
      tempPlayers = snapshot.get('players');
    });

    var playerName = await profileBox.get(ProfileKeys.profileName, defaultValue: null);
    if (playerName == null) {
      Fluttertoast.showToast(
        msg: "Profile not created yet!",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        fontSize: 16.0,
      );
      Navigator.of(context).pop();
    } else {
      if (!tempPlayers.contains(playerName)) {
        tempPlayers.add(playerName);
      }
    }
    await ref.update(
        {'players' : tempPlayers}
        );
  }

  Future<void> _hostHandleCards() async {
    var playerList;
    Deck deck;
    Map<String, String> selectionHandKnown = {};
    int playerCount = 6;
    int totalRounds = 20;
    Map<String, List<String>> hands = {};
    Map<String, int> order = {};

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    await ref.get().then((snapshot) {
      playerCount = snapshot.get('playerCount');
      totalRounds = snapshot.get('totalRounds');
      playerList = snapshot.get('players');
    });

    for (String player in playerList) {
      hands[player] = [];
    }

    if (true) {
      deck = provideDeck(playerCount, totalRounds);
      for(var i = 0; i < playerList.length ; i++) {
        selectionHandKnown[playerList[i]] = deck.drawCard().getString();
        order[playerList[i]] = i;
      }
      await ref.update({
        "selectionHandKnown" : selectionHandKnown,
        "selectionHandUnknown" : {},
        "gameStarted" : true,
        "hands" : hands,
        "playersOrder" : order,
        "hostJoined" : true
      });
    }
  }

  Future<bool> _hostJoinHandler() async {
    var hostName;
    bool joined = false;
    var playerName = await profileBox.get(ProfileKeys.profileName, defaultValue: null);

    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);

    await ref.get().then((snapshot) {
      hostName = snapshot.get('hostName');
      joined = snapshot.get('hostJoined');
    });

    if (joined || hostName == playerName) {
      return true;
    } else {
      return false;
    }
  }

  Future<String> retreiveHost() async {
    var hostName;
    DocumentReference ref = FirebaseFirestore.instance.collection('games').doc(widget.serverName);
    await ref.get().then((snapshot) {
      hostName = snapshot.get('hostName');
    });

    if (hostName != null) {
      return hostName;
    } else {
      return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    _serverStream = FirebaseFirestore.instance.collection('games').doc(widget.serverName).snapshots();

    var name = profileBox.get(ProfileKeys.profileName, defaultValue: null);
    _handleNotificationTokenUpload(name);

    return Scaffold(
      appBar: AppBar(
        title: Text("Waiting for: " + widget.serverName),
        actions: <Widget> [
          Padding(
            padding: const EdgeInsets.only(right:10.0),
            child: GestureDetector(
              onTap: () async {
                if (await amIHost()) {
                  confirmServerDelete(context, widget.serverName);
                } else {
                  Fluttertoast.showToast(
                    msg: "Only the host can delete",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    fontSize: 16.0,
                  );
                }
              },
              child: const Icon(
                Icons.delete,
              )
            )
          )
        ],
      ),

      body: StreamBuilder(
          stream: _serverStream,
          builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.get('players').length,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (BuildContext context, int index) {
                    snapshot.data!.get('players').length == snapshot.data!.get('playerCount') ? ready = true : ready = false;
                    return Card (
                        elevation: 0,
                        margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                        child: GestureDetector(
                          onTap: () {
                            // maybe list details on the user if they would like?
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey, // change depending on whether they're in or not
                                  border: Border.all(
                                    color: Colors.white,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                              ),
                              child: Container (
                                alignment: Alignment.center,
                                child: Text(snapshot.data?.get('players')[index] ?? "ERROR", style: TextStyle(color: Colors.white, fontSize: 20),),
                                height: 50,
                              )
                          ),
                        )
                    );
                  }
              );
            } else {
              return const Text("Server not available");
            }
          }
      ),
      floatingActionButton: Column (
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget> [
          FloatingActionButton(
            heroTag: "btn0",
            backgroundColor: Colors.red,
            onPressed: () async {
              if (await amIHost()) {
                confirmServerDelete(context, widget.serverName);
              } else {
                _playerRemoveHandler();
                Navigator.of(context).pop();
              }
            },
            child: const Icon(Icons.exit_to_app),
          ),
          Container(
            height: 25,
          ),
          FloatingActionButton(
            heroTag: "btn1",
            onPressed: () {
              setState(() {});
            },
            child: const Icon(Icons.refresh),
          ),
          Container(
            height: 25,
          ),
          FloatingActionButton(
            heroTag: "btn2",
            backgroundColor: ready ? Colors.yellow : Colors.grey,
            onPressed: () async {
              // start game here
              if (ready) {
                hostJoined = await _hostJoinHandler();
                String hostname = await retreiveHost();
                String playerName = await profileBox.get(ProfileKeys.profileName, defaultValue: null);
                if (hostJoined) {

                  if (hostname == playerName) {
                    await _hostHandleCards();
                  }

                  Navigator.of(context).push(
                      MaterialPageRoute(
                          // builder: (context) => OrderSelection(serverName: widget.serverName)
                          builder: (context) => PlayScreen(serverName: widget.serverName)
                      )
                  );
                } else {
                  _sendHostNotification(hostname);
                  Fluttertoast.showToast(
                    msg: "Host has not joined yet, please wait for $hostname to join first (Notification sent)",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    fontSize: 16.0,
                  );
                }
              } else {
                Fluttertoast.showToast(
                  msg: "Not enough players yet",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 1,
                  fontSize: 16.0,
                );
              }
            },
            child: const Icon(Icons.play_arrow_outlined),
          ),
        ],
      )
    );
  }
}

void confirmServerDelete(BuildContext context, String serverName) {
  Widget cancel = TextButton(
    child: const Text("Cancel"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  Widget accept = TextButton(
    child: const Text("Accept"),
    onPressed: () async {
      var nav = Navigator.of(context);
      nav.pop();
      nav.pop();
      await FirebaseFirestore.instance.collection('games').doc(serverName).delete().then((_) => debugPrint("success"));
    },
  );

  AlertDialog alert = AlertDialog(
    title: const Text("Delete Server"),
    content: const Text(
      "Are you sure you want to delete?"
    ),
    actions: [
      cancel,
      accept,
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    }
  );
}
