import 'package:flutter/material.dart';

class GameHistory extends StatefulWidget {
  const GameHistory({Key? key}) : super(key: key);


  @override
  _GameHistoryState createState() => _GameHistoryState();
}

class _GameHistoryState extends State<GameHistory>{
  List<dynamic> previousGames = ["Game Title",];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Game History"),
      ),
      backgroundColor: const Color(0xFF256D1B),
      body: Column(
        children: [
          Expanded(
            //flex: 3,
            child: ListView.builder(
              itemCount: previousGames.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    child: ListTile(
                      tileColor: Colors.black,
                      textColor: Colors.white,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                      ),
                      //leading: const Text("18"),
                      title: const Text("18/10/22"),
                      trailing: Wrap(
                        spacing: 12, // space between two icons
                        children: <Widget>[
                          Text(
                            previousGames[index],
                            style: const TextStyle(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Container(width: 30,),
                          const Text("10 to 1"),
                          Container(width: 30,),
                          const Text("180"),
                        ],
                      ),
                    )
                );
              },
            ),
          ),
        ],
      ),


    );
  }

}